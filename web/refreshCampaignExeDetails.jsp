<%@page import="org.json.JSONArray"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.CampaignManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmCampaigndetails"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmAiTrackingCampaignDetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.AITrackingManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmBrandownerdetails"%>
<script src="scripts/billingReport.js" type="text/javascript"></script>

<%
    RmBrandownerdetails parObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails"); 
   
    String startDate = request.getParameter("_sdate");
    String endDate = request.getParameter("_edate");
    String stime = request.getParameter("_stime");
    String etime = request.getParameter("_etime");
    String campExeName = request.getParameter("campExeName");
    String campId = request.getParameter("campId");
    SimpleDateFormat tmDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
    int iCampId = 0;
    if(campId != null){
        iCampId = Integer.parseInt(campId);
    }
    
    RmAiTrackingCampaignDetails[] aiTracking = new AITrackingManagement().getTxDetails(parObj.getOwnerId(), startDate, endDate, stime, etime,campExeName, iCampId);
    int count = 0;
    session.setAttribute("aiTrackingDetails", aiTracking);
    session.setAttribute("campExeNameDetail", campExeName);
%>
<!--<div class="col-lg-12 col-md-12 col-sm-12">    -->

    <div class="row">
        <button style="margin-left: 59%" class="btn btn-success btn-xs ladda-button refreshCampaignReport" data-style="zoom-in" id="refreshButton"  onclick="refreshCampaignTracking('<%=startDate%>','<%=endDate%>','<%=campExeName%>','<%=iCampId%>')"> <i class="fa fa-refresh"></i> Refresh  </button>
            <br><br>               
        <div class="col-lg-12">
<!--            <a style="margin-left: 59%" class="btn btn-success btn-xs ladda-button" id="refreshButton"  data-style="zoom-in" onclick="refreshCampaignTracking('<%=startDate%>','<%=endDate%>','<%=campExeName%>','<%=iCampId%>')"> <i class="fa fa-refresh"></i> Refresh  </a>
            <br><br>-->
            <table class="table table-striped table-bordered table-hover" id="dataTables-example1">
                <thead id="apiUsageHeader">
                    <tr>
                        <th style="text-align: center">Sr.No.</th>
                        <th style="text-align: center">Date</th>
                        <th style="text-align: center">Campaign</th>
                        <th style="text-align: center">Execution Name</th>
                        <th style="text-align: center">User Name</th>
                        <th style="text-align: center">Screen Name</th>
                        <th style="text-align: center">Follower Count</th>                                        
                        <th style="text-align: center">Location</th>
<!--                        <th>Post On</th>-->
                        <th style="text-align: center">Post URL</th>
                    </tr>
                </thead>
                <tbody>
                    <%  
                        if (aiTracking != null) {
                            for (RmAiTrackingCampaignDetails transcationdetailse : aiTracking) {
                                try {
                                        String campName = "NA"; String user = "NA"; String followerCount = "NA"; String date = null;
                                        String location = "NA"; String twitterPostURL = null; String screenName = "NA";
                                        String campaignExeName = "NA";
                                        if(transcationdetailse.getTwitterReturnData() != null){
                                            JSONObject data = new JSONObject(transcationdetailse.getTwitterReturnData());
                                            RmCampaigndetails campDetails = new CampaignManagement().getCampaignByIdDetails(transcationdetailse.getCampaignId());
                                            if(campDetails != null){
                                                campName = campDetails.getRewardTitle();
                                            }
                                            if(data.has("user")){
                                                JSONObject userJson = data.getJSONObject("user");
                                                if(userJson.has("followers_count")){
                                                    followerCount = userJson.getString("followers_count");
                                                }
                                                if(userJson.has("name")){
                                                    user = userJson.getString("name");
                                                }
                                                if(userJson.has("location")){
                                                    location = userJson.getString("location");
                                                    if(location != null && !location.equalsIgnoreCase("null")){                                                        
                                                    }else
                                                        location="NA";
                                                }
                                                if(userJson.has("screen_name")){
                                                    screenName = userJson.getString("screen_name");
                                                }
                                            }
                                            if(data.has("created_at")){
                                                date = data.getString("created_at");
                                            }
                                            if(data.has("extended_entities")){
                                                JSONObject mediaJSON = data.getJSONObject("extended_entities");
                                                JSONArray mediaArr = mediaJSON.getJSONArray("media");                                                
                                                String postURL = mediaArr.getString(0);                                                
                                                //System.out.println("mediaJson >> "+postURL);
                                                JSONObject media = new JSONObject(postURL);
                                                if(media.has("expanded_url")){
                                                    twitterPostURL = media.getString("expanded_url");
                                                }
                                            }
                                            if(transcationdetailse.getCampaignExecutedId() != null){
                                                campaignExeName = transcationdetailse.getCampaignExecutedId();
                                            }
                                            
                                        
//                                        if(date != null){
//                                            Date d = tmDateFormat.parse(date);
//                                            date = tmDateFormat.format(d);
//                                        }else{
//                                            date = "NA";
//                                        }
                                       count ++; 
                                    
                                
                    %>
                    <tr>
                        <td ><font><%=count%></font></td>
                        <td ><font><%=tmDateFormat.format(transcationdetailse.getCreationDate())%></font>
                        <td ><font ><%=campName%></font></td>
                        <td ><font ><%=campaignExeName%></font></td>
                        <td ><font><%=user%> </font></td>     
                        <td ><font ><%=screenName%></font></td> 
                        <td ><font ><%=followerCount%></font></td>                        
                        <td ><font><%=location%></font></td>
                        <td >
                            <%if(twitterPostURL != null){%>
                            <a href="<%=twitterPostURL%>" target="_blank"> View Post </a>
                            <%}else{%>
                            <font>NA</font>
                            <%}%>
                        </td>
                    </tr>
                    <%
                        }
                        }catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                        }
                        }if (count < 1){
                        
                    %>
                    <img src="images/no_record_found.png" alt="No record found" width="400px" height="300px"/>
                <script>
                    document.getElementById("apiUsageHeader").style.display = 'none';
                </script>
                    <%}%>
                </tbody>
                </table>            
        </div>
                </div>
        <%if (count != 0) {%>
        <a  style="float: left"class="btn btn-info btn-xs"  onclick="downloadCSV('<%=startDate%>', '<%=endDate%>', '<%=stime%>', '<%=etime%>', '<%=campId%>')"> Download CSV <i class="fa fa-file-excel-o"></i> </a>
        <%}%>
        <br><br>
        <a style="float: left"class="btn btn-success btn-xs ladda-button" data-style="zoom-in" id="backButton"  onclick="backToCampaignTracking()"> <i class="fa fa-backward"></i> Back  </a>
    


<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });

</script>

