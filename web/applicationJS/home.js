/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var dateData = ["Data e Ora", "2014-05-20 00:00:00", "2014-05-20 00:00:00", "2014-05-20 00:00:00", "2014-05-20 00:00:00", "2014-05-20 04:00:00", "2014-05-20 05:00:00", "2014-05-20 06:00:00", "2014-05-20 07:00:00", "2014-05-20 08:00:00", "2014-05-20 09:00:00", "2014-05-20 10:00:00", "2014-05-20 11:00:00", "2014-05-20 12:00:00", "2014-05-20 13:00:00", "2014-05-20 14:00:00", "2014-05-20 15:00:00", "2014-05-20 16:00:00", "2014-05-20 17:00:00", "2014-05-20 18:00:00", "2014-05-20 19:00:00", "2014-05-20 20:00:00", "2014-05-20 21:00:00", "2014-05-20 22:00:00", "2014-05-20 23:00:00", "2014-05-20 24:00:00"]; // SORTED
var data1 = ['Call Count', 30, 200, 100, 30, 150, 250, 50, 20, 10, 40, 15, 25, 30, 200, 100, 40, 40, 15, 50, 20, 10, 40, 15, 25, 50];
var data2 = ['TranscationAmount', 50, 300, 150, 40, 230, 290, 90, 200, 100, 400, 150, 250, 50, 120, 70, 150, 150, 250, 30, 200, 100, 400, 150, 250, 40];
var data3 = ['Call Count', 90, 120, 230, 450, 540, 240];
var data4 = ['TranscationAmount', 300, 400, 500, 700, 900, 600];

var monthData = ["Data e Ora", "2014-05-01 00:00:00", "2014-05-02 00:00:00", "2014-05-03 00:00:00", "2014-05-04 00:00:00", "2014-05-05 00:00:00", "2014-05-06 00:00:00", "2014-05-07 00:00:00", "2014-05-08 08:00:00", "2014-05-09 09:00:00", "2014-05-10 10:00:00", "2014-05-11 11:00:00", "2014-05-12 12:00:00", "2014-05-13 13:00:00", "2014-05-14 14:00:00", "2014-05-15 15:00:00", "2014-05-16 16:00:00", "2014-05-17 17:00:00", "2014-05-18 18:00:00", "2014-05-19 19:00:00", "2014-05-20 20:00:00", "2014-05-21 21:00:00", "2014-05-22 22:00:00", "2014-05-23 23:00:00", "2014-05-24 24:00:00", "2014-05-25 24:00:00", "2014-05-26 24:00:00", "2014-05-27 24:00:00", "2014-05-28 24:00:00", "2014-05-29 24:00:00", "2014-05-30 24:00:00"]; // SORTED
var data5 = ['Call Count', 30, 200, 100, 30, 150, 250, 50, 20, 10, 40, 15, 25, 30, 200, 100, 40, 40, 15, 50, 20, 10, 40, 15, 25, 50, 30, 200, 100, 30, 150];
var data6 = ['TranscationAmount', 50, 300, 150, 40, 230, 290, 90, 200, 100, 400, 150, 250, 50, 120, 70, 150, 150, 250, 30, 200, 100, 400, 150, 250, 40, 50, 300, 150, 40, 230];
function hourlyRevenueTXDetails() {
    c3.generate({
        bindto: '#stocked',
        data: {
            x: 'Data e Ora',
            xFormat: '%Y-%m-%d %H:%M:%S',
            columns: [
                dateData,
                data2,
                data1
            ],
            colors: {
                data1: '#62cb31',
                TranscationAmount: '#FF7F50'
            },
            types: {
                TranscationAmount: 'bar'
            },
            groups: [
                ['Transcation Amount', 'Call Count']
            ]
        },
        axis: {
            x: {
                type: 'timeseries',
                // if true, treat x value as localtime (Default)
                // if false, convert to UTC internally                        
                tick: {
                    format: '%H:%M'
                            //fomat:function (x) { return x.getFullYear(); }
                }
            }
        }
    });
    document.getElementById("homeFirstReportLabel").innerHTML = "Hourly Details";
}
function lastSevenDayRevenueTXDetails() {
    c3.generate({
        bindto: '#stocked',
        data: {
            x: 'Data e Ora',
            xFormat: '%Y-%m-%d %H:%M:%S',
            columns: [
                dateData,
                data4,
                data3
            ],
            colors: {
                data1: '#62cb31',
                TranscationAmount: '#FF7F50'
            },
            types: {
                TranscationAmount: 'bar'
            },
            groups: [
                ['Transcation Amount', 'Call Count']
            ]
        },
        axis: {
            x: {
                type: 'timeseries',
                // if true, treat x value as localtime (Default)
                // if false, convert to UTC internally                        
                tick: {
                    format: '%d-%m-%Y'
                            //fomat:function (x) { return x.getFullYear(); }
                }
            }
        }
    });
    document.getElementById("homeFirstReportLabel").innerHTML = "Last 7 days";
}
function lastThirtyDayRevenueTXDetails() {
    c3.generate({
        bindto: '#stocked',
        data: {
            x: 'Data e Ora',
            xFormat: '%Y-%m-%d %H:%M:%S',
            columns: [
                monthData,
                data6,
                data5
            ],
            colors: {
                data1: '#62cb31',
                TranscationAmount: '#FF7F50'
            },
            types: {
                TranscationAmount: 'bar'
            },
            groups: [
                ['Transcation Amount', 'Call Count']
            ]
        },
        subchart: {
            show: true
        },
        axis: {
            x: {
                type: 'timeseries',
                // if true, treat x value as localtime (Default)
                // if false, convert to UTC internally                        
                tick: {
                    format: '%d-%m-%Y'
                            //fomat:function (x) { return x.getFullYear(); }
                }
            }
        }
    });
    document.getElementById("homeFirstReportLabel").innerHTML = "Last 30 days";
}
hourlyRevenueTXDetails();
setTimeout(function () {
    lastSevenDayRevenueTXDetails();
    document.getElementById("homeFirstReportLabel").innerHTML = "Last 7 days";
}, 4000);
setTimeout(function () {
    lastThirtyDayRevenueTXDetails();
    document.getElementById("homeFirstReportLabel").innerHTML = "Last 30 days";
}, 8000);
setTimeout(function () {
    hourlyRevenueTXDetails();
    document.getElementById("homeFirstReportLabel").innerHTML = "Hourly Details";
}, 12000);

// Merchant Report
var monthMerchantData = ["Merchant", "Amazon", "MyTrip", "BookMyShow", "Flipkart", "PayTM"]; // SORTED
var revenueData = ['Call Count', 30, 200, 100, 30, 150, 250, 50, 20, 10, 40, 15, 25, 30, 200, 100, 40, 40, 15, 50, 20, 10, 40, 15, 25, 50, 30, 200, 100, 30, 150];
var txData      = ['TranscationAmount', 50, 300, 150, 40, 230, 290, 90, 200, 100, 400, 150, 250, 50, 120, 70, 150, 150, 250, 30, 200, 100, 400, 150, 250, 40, 50, 300, 150, 40, 230];
function hourlyMerchantTXDetails() {
    c3.generate({
        bindto: '#merchantReport',
        data: {
            x: 'Merchant',
            //xFormat: '%Y-%m-%d %H:%M:%S',
            columns: [
                monthMerchantData,                
                txData,
                revenueData
            ],
            colors: {
                data1: '#62cb31',
                TranscationAmount: '#FF7F50'
            },
            type: 'bar',
            groups: [
                ['Transcation Amount', 'Call Count']
            ]
        },
        axis: {
            x: {
                type: 'category',
                // if true, treat x value as localtime (Default)
                // if false, convert to UTC internally                        
                categories:monthMerchantData
            }
        }
    });    
}
hourlyMerchantTXDetails();

