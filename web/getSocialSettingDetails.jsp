<%@page import="com.mollatech.rewardme.nucleus.db.RmSocialmediadetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.SocialMediaManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.SubscriptionManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmSubscriptionpackagedetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.CampaignManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmCampaigndetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmBrandownerdetails"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.rewardme.nucleus.commons.GlobalStatus"%>
<%@page import="org.json.JSONObject"%>
<script src="scripts/profile.js" type="text/javascript"></script>
<style>
    td {
  text-align: center;
  vertical-align: middle;
}
p.test1 {
    width: 280px;    
    word-break: break-all;
}
</style>
<%
    String SessionId = (String) request.getSession().getAttribute("_brandownerSessionId");
    if (SessionId == null) {
        response.sendRedirect("logout.jsp");
        return;
    }  
%>
<div id="wrapper">
        
</div>
<div class="content">
    <div class="row">
        <div class="col-md-12 tour-13">
            <div class="hpanel">
                <div class="panel-body">
                    <input type="text" class="form-control input-sm m-b-md" id="filter" placeholder="Search in table">                                        
                    <div class="table-responsive">
                        <table id="api" class="footable table table-stripped table-responsive" valign="middle" data-page-size="25" data-filter=#filter>    
                            <thead>
                                <tr>
                                    <th></th>
                                    <th style="text-align: center" class="tour-15">Social Media</th>
                                    <th style="text-align: center" class="tour-15">Handler Name</th>
                                    <th style="text-align: center" class="tour-15">Access Token</th>
                                    <th data-hide="phone,tablet" style="text-align: center" class="tour-15">Access Token Secret</th>                                                                                                            
<!--                                    <th data-hide="phone,tablet" style="text-align: center">Consumer Key</th>                                    -->
<!--                                    <th data-hide="phone,tablet" style="text-align: center">Consumer Secret</th>                                    -->
                                    <th style="text-align: center" class="tour-17">Update</th>
                                </tr>
                            </thead>
                            <tbody style="text-align: center">
                                <%
                                    RmBrandownerdetails usrObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
                                    SocialMediaManagement socialObj = new SocialMediaManagement();
                                    RmSocialmediadetails[] socialMediaDetails = socialObj.getSocialMediaDetailsByOwnerId(usrObj.getOwnerId());                                    
                                    String twitterHandlerName="NA"; String twitterAppId="NA"; String twitterAppKey = "NA"; String twitterClientSecret = "NA"; String twitterConsumerSecret = "NA";
                                    String facebookAppId="NA"; String facebookAppKey = "NA"; String facebookClientSecret = "NA";
                                    String instagramAppId="NA"; String instagramAppKey = "NA"; String instagramClientSecret = "NA";
                                    if(socialMediaDetails != null){
                                        for(int j=0; j < socialMediaDetails.length; j++){
                                        for (int i = 0; i < 3; i++) {                                                                                                                                    
                                                if( socialMediaDetails[j].getTwitterHandlerName()!= null && !socialMediaDetails[j].getTwitterHandlerName().isEmpty()){
                                                    twitterHandlerName = socialMediaDetails[j].getTwitterHandlerName();
                                                }
                                                if( socialMediaDetails[j].getTwitterAccessToken() != null && !socialMediaDetails[j].getTwitterAccessToken().isEmpty()){
                                                    twitterAppId = socialMediaDetails[j].getTwitterAccessToken();
                                                }
                                                if( socialMediaDetails[j].getTwitterAccessTokenSecret() != null && !socialMediaDetails[j].getTwitterAccessTokenSecret().isEmpty()){
                                                    twitterAppKey = socialMediaDetails[j].getTwitterAccessTokenSecret();
                                                }
                                                if( socialMediaDetails[j].getTwitterConsumerKey() != null && !socialMediaDetails[j].getTwitterConsumerKey().isEmpty()){
                                                    twitterClientSecret = socialMediaDetails[j].getTwitterConsumerKey();
                                                }
                                                if( socialMediaDetails[j].getTwitterConsumerSecret() != null && !socialMediaDetails[j].getTwitterConsumerSecret().isEmpty()){
                                                    twitterConsumerSecret = socialMediaDetails[j].getTwitterConsumerSecret();
                                                }
                                                if( socialMediaDetails[j].getFacebookAppId() != null && !socialMediaDetails[j].getFacebookAppId().isEmpty()){
                                                    facebookAppId = socialMediaDetails[j].getFacebookAppId();
                                                }
                                                if( socialMediaDetails[j].getFacebookAppkey() != null && !socialMediaDetails[j].getFacebookAppkey().isEmpty()){
                                                    facebookAppKey = socialMediaDetails[j].getFacebookAppkey();
                                                }
                                                if( socialMediaDetails[j].getFacebookClientsecret() != null && !socialMediaDetails[j].getFacebookClientsecret().isEmpty()){
                                                    facebookClientSecret = socialMediaDetails[j].getFacebookClientsecret();
                                                }
                                                if( socialMediaDetails[j].getInstagramAppId() != null && !socialMediaDetails[j].getInstagramAppId().isEmpty()){
                                                    instagramAppId = socialMediaDetails[j].getInstagramAppId();
                                                }
                                                if( socialMediaDetails[j].getInstagramAppkey() != null && !socialMediaDetails[j].getInstagramAppkey().isEmpty()){
                                                    instagramAppKey = socialMediaDetails[j].getInstagramAppkey();
                                                }
                                                if( socialMediaDetails[j].getInstagramClientsecret() != null && !socialMediaDetails[j].getInstagramClientsecret().isEmpty()){
                                                    instagramClientSecret = socialMediaDetails[j].getInstagramClientsecret();
                                                }                                           
                                %>
                                <tr style="text-align: center">                                   
                                    <%if(i == 0){%>                                    
                                    <td>
                                        
                                    </td>
                                    <td>
                                        <p>Twitter</p>
                                    </td>
                                    <td>
                                        <p><%=twitterHandlerName%></p>
                                    </td>
                                    <td>
                                        <p class="test1" style="text-align: center"><%=twitterAppId%></p>
                                    </td>
                                    <td >
                                        <p class="test1" style="text-align: center"><%=twitterAppKey%></p>
                                    </td>
<!--                                    <td  >
                                        <p class="test1" style="text-align: center"><%=twitterClientSecret%></p>
                                    </td>-->
<!--                                    <td >
                                        <p class="test1" style="text-align: center"><%=twitterConsumerSecret%></p>
                                    </td>-->
                                    <td style="" class="tour-iconAPIConsole" align="center" valign="middle">   
                                        <a onclick = "editSocialSetting('<%=socialMediaDetails[j].getSocialmediaId()%>')" class="text-warning" data-toggle="tooltip" data-placement="right" title="Update Campaign" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                    </td>
                                    <%}else if(i==1){%>
                                    <td>
                                        
                                    </td>
                                    <td>
                                        <p>Facebook</p>
                                    </td>
                                    <td>NA</td>
                                    <td>
                                        <p><%=facebookAppId%></p>
                                    </td>
                                    <td>
                                        <p><%=facebookAppKey%></p>
                                    </td>
<!--                                    <td>
                                        <p><%=facebookClientSecret%></p>
                                    </td>-->
<!--                                    <td>
                                        <p>Not Needed</p>
                                    </td>-->
                                    <td style="" class="tour-iconAPIConsole" align="center" valign="middle">   
                                        <a onclick = "editSocialSetting('<%=socialMediaDetails[j].getSocialmediaId()%>')" class="text-warning" data-toggle="tooltip" data-placement="right" title="Update Campaign" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                    </td>
                                    <%}else if(i==2){%>
                                    <td>
                                        
                                    </td>
                                    <td>
                                        <p>Instagram</p>
                                    </td>
                                    <td>NA</td>
                                    <td>
                                        <p><%=instagramAppId%></p>
                                    </td>
                                    <td>
                                        <p><%=instagramAppKey%></p>
                                    </td>
<!--                                    <td>
                                        <p><%=instagramClientSecret%></p>
                                    </td>-->
<!--                                    <td>
                                        <p>Not Needed</p>
                                    </td>-->
                                    <td style="" class="tour-iconAPIConsole" align="center" valign="middle">   
                                        <a onclick = "editSocialSetting('<%=socialMediaDetails[j].getSocialmediaId()%>')" class="text-warning" data-toggle="tooltip" data-placement="right" title="Update Campaign" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                    </td>
                                    <%}%>
                                </tr>  
                                <%}}}%>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="11">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <a class="btn btn-info btn-sm" data-style="zoom-in" id="socialSettings" onclick="addNewSocialSettings()" href="#" >Create Social Settings</a>                                                        
                    </div>                   
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-bottom: 28%"></div>
    </div>
</div>
<script>
    $(function () {
        $('#api').footable();
    });
</script>