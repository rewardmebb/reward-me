<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.ApprovedPackageManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmApprovedpackagedetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.SocialMediaManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmSocialmediadetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.CampaignManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmCampaigndetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.CreditManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.SubscriptionManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmSubscriptionpackagedetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmCreditinfo"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmBrandownerdetails"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.net.URL"%>
<%@page import="java.math.BigDecimal"%>
<!DOCTYPE html>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="java.util.List"%>
<html>
    <%response.addHeader("X-FRAME-OPTIONS", "DENY");%>    
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Page title -->
        <title>Reward Me</title>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" type="images/front-logo.png" href="images/front-logo.png" />

        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
        <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
        <link rel="stylesheet" href="vendor/animate.css/animate.css" />
        <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="vendor/bootstrap-star-rating/css/star-rating.css" />
        <link rel="stylesheet" href="vendor/datatables.net-bs/css/dataTables.bootstrap.min.css" />
        <link rel="stylesheet" href="vendor/select2-3.5.2/select2.css" />
        <link rel="stylesheet" href="vendor/select2-bootstrap/select2-bootstrap.css" />
        <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" />
        <link rel="stylesheet" href="vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css" />
        <link rel="stylesheet" href="vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" />
        <link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css" />
        <link rel="stylesheet" href="vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
        <link rel="stylesheet" href="vendor/summernote/dist/summernote.css" />
        <link rel="stylesheet" href="vendor/summernote/dist/summernote-bs3.css" />
        <link rel="stylesheet" href="vendor/jquery-ui/themes/base/all.css" />
        <link rel="stylesheet" href="vendor/dropzone/dropzone.css">
        <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
        <link rel="stylesheet" href="vendor/ladda/dist/ladda-themeless.min.css" />
        <link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
        <link rel="stylesheet" href="vendor/codemirror/style/codemirror.css" />
        <link rel="stylesheet" href="vendor/fooTable/css/footable.core.min.css" />
        <!--         <link rel="stylesheet" href="vendor/c3/c3.min.css" />-->
        <!-- App styles -->
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
        <link rel="stylesheet" href="styles/static_custom.css">
        <link rel="stylesheet" href="styles/style.css">
        <link rel="stylesheet" href="vendor/bootstrap-tour/build/css/bootstrap-tour.min.css" />

        <!-- Need call first -->
        <script src="scripts/header.js" type="text/javascript"></script>
        <script src="vendor/jquery/dist/jquery.min.js"></script>        
        <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendor/summernote/dist/summernote.min.js"></script>
        <script src="vendor/codemirror/script/codemirror.js"></script>
        <script src="vendor/codemirror/javascript.js"></script> 
        <link rel="stylesheet" href="vendor/c3/c3.min.css" />
        <script src="vendor/d3/d3.min.js"></script>
        <script src="vendor/c3/c3.min.js"></script>
        <link rel="stylesheet" href="vendor/chartist/custom/chartist.css" />
        <script src="vendor/chartist/dist/chartist.min.js"></script>
        <script src="scripts/homepage.js" type="text/javascript"></script>
        <script src="vendor/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!--        <link rel="stylesheet" href="vendor/blueimp-gallery/css/blueimp-gallery.min.css" />
        <link rel="stylesheet" href="vendor/blueimp-gallery/css/blueimp-gallery-video.css">-->
        <!--<script src="vendor/jquery-flot/jquery.flot.pie.js" type="text/javascript"></script>-->

        <!-- Vendor scripts -->
        <script src="scripts/utilityFunction.js" type="text/javascript"></script>
        <script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
        <script src="vendor/iCheck/icheck.min.js"></script>
        <script src="vendor/moment/moment.js"></script>
        <script src="vendor/sparkline/index.js"></script>
        <script src="vendor/chartjs/Chart.min.js"></script>
        <script src="vendor/jquery-flot/jquery.flot.js"></script>
        <script src="vendor/jquery-flot/jquery.flot.resize.js"></script>
        <script src="vendor/jquery-flot/jquery.flot.pie.js"></script>
        <script src="vendor/jquery.flot.spline/index.js"></script>
        <script src="vendor/flot.curvedlines/curvedLines.js"></script>
        <script src="vendor/peity/jquery.peity.min.js"></script>
        <script src="vendor/bootstrap-star-rating/js/star-rating.min.js"></script>
        <script src="vendor/codemirror/script/codemirror.js"></script>
        <script src="vendor/codemirror/javascript.js"></script>
        <script src="vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
        <script src="vendor/select2-3.5.2/select2.min.js"></script>
        <script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
        <script src="vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="vendor/clockpicker/dist/bootstrap-clockpicker.min.js"></script>
        <script src="vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="vendor/dropzone/dropzone.js"></script>
        <script src="scripts/tokenManagement.js" type="text/javascript"></script>
        <script src="vendor/ladda/dist/spin.min.js"></script>
        <script src="vendor/ladda/dist/ladda.min.js"></script>
        <script src="vendor/ladda/dist/ladda.jquery.min.js"></script>
        <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
        <script src="vendor/sweetalert/lib/sweet-alert.min.js"></script>
        <script src="vendor/toastr/build/toastr.min.js"></script>
        <!-- DataTables -->
        <script src="vendor/datatables/media/js/jquery.dataTables.min.js"></script>
        <script src="vendor/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

        <!-- DataTables buttons scripts -->
        <script src="vendor/pdfmake/build/pdfmake.min.js"></script>
        <script src="vendor/pdfmake/build/vfs_fonts.js"></script>
        <script src="vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="vendor/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
        <script src="vendor/fooTable/dist/footable.all.min.js"></script>        
        
        <!-- App scripts -->
        <script src="scripts/homer.js"></script>
        <script src="scripts/utilityFunction.js" type="text/javascript"></script>
        <script src="scripts/charts.js"></script>
        <script src="scripts/tourToDashboard.js" type="text/javascript"></script>  
        <link href="styles/toggleButton/bootstrap-switch.css" rel="stylesheet" type="text/css"/>
        <script src="styles/toggleButton/bootstrap-switch.js" type="text/javascript"></script>
        <script src="scripts/campaignReport.js" type="text/javascript"></script>
    </head>
    <%
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        
        String SessionId = (String) request.getSession().getAttribute("_brandownerSessionId");               
        String profileImageUrl = (String) request.getSession().getAttribute("profileImageUrl");
        RmBrandownerdetails usrObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
        String socialNamed = (String) request.getSession().getAttribute("socialNamed");
        String showTourFirstSignUp = (String) request.getSession().getAttribute("showTourFirstSignUp");
        if (SessionId == null) {
            response.sendRedirect("logout.jsp");
            return;
        }
        if (socialNamed == null) {
            socialNamed = usrObj.getBrandName();
        }
        int notificationCount = 0;        
        // find the Service Count
        int serviceCount = 0;        
        String base64ProfilePic = null;                
    %>

    <body id="Ashish" class="fixed-navbar">
        <style>
            span.capitalize {
                text-transform: capitalize;
            }
        </style>
        
        <!-- Simple splash screen-->
        <div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Loading...</h1><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
        <!--[if lt IE 7]>
        <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Header -->
        <div id="header">
            <div class="color-line">
            </div>
            <div id="logo" class="light-version">
                <span>
                    Reward Me
                </span>
            </div>
            <nav role="navigation">
                <div id="menu-button" class="header-link hide-menu"><i class="fa fa-bars"></i></div>
                <div class="small-logo">
                    <span class="text-primary">Reward Me</span>
                </div>
                <div class="mobile-menu">
                    <button type="button" class="navbar-toggle mobile-menu-toggle" data-toggle="collapse" data-target="#mobile-collapse">
                        <i class="fa fa-chevron-down"></i>
                    </button>
                    <div class="collapse mobile-navbar" id="mobile-collapse">
                        <ul class="nav navbar-nav">
                            <li>
                                <a onclick="changePassword()"> <i class="pe pe-7s-key text-info"></i>  Change Password</a>
                            </li>
                            <li>
                                <a onclick="helpDesk()"><i class="pe pe-7s-mail text-warning"></i> Raise Ticket</a>
                            </li>                           
                            <li>
                                <a onclick="subscription()"><i class="pe pe-7s-shopbag text-info"></i> Subscription</a>
                            </li>
                            <li>
                                <a onclick="videoGallery()"><i class="pe pe-7s-video text-warning"></i><h5>Videos</h5></a>
                            </li>
                            <li>
                                <a onclick="firstTimeTour()"><i class="pe pe-7s-info text-danger"></i>Quick Tour </a>
                            </li>                            
                            <li>
                                <a href="logout.jsp">
                                    <i class="pe-7s-upload" title="Logout"> Logout</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="navbar-right">
                    <ul class="nav navbar-nav no-borders">
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                <i class="pe-7s-keypad" title="My Menu"></i>
                            </a>

                            <div class="dropdown-menu hdropdown bigmenu animated flipInX">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <a onclick="changePassword()">
                                                    <i class="pe pe-7s-key text-info"></i>
                                                    <h5>Change Password</h5>
                                                </a>
                                            </td>
                                            <!--                                            <td>
                                                                                            <a href="my_profile.jsp">
                                                                                                <i class="pe pe-7s-users text-success"></i>
                                                                                                <h5>My Profile</h5>
                                                                                            </a>
                                                                                        </td>-->
                                            <td>
                                                <a onclick="helpDesk()">
                                                    <i class="pe pe-7s-mail text-warning"></i>
                                                    <h5>Raise Ticket</h5>
                                                </a>
                                            </td>
                                            <td>
                                                <a onclick="myProfile()">
                                                    <i class="pe pe-7s-user text-success"></i>
                                                    <h5>My Profile</h5>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a onclick="subscription()">
                                                    <i class="pe pe-7s-shopbag text-info"></i>
                                                    <h5>Subscription</h5>
                                                </a>
                                            </td> 
                                            <td>
                                                <a onclick="videoGallery()">
                                                    <i class="pe pe-7s-video text-warning"></i>
                                                    <h5>Videos</h5>
                                                </a>
                                            </td>
                                            <td>
                                                <a onclick="firstTimeTour()">
                                                    <i class="pe pe-7s-info text-danger"></i>
                                                    <h5>Quick Tour</h5>
                                                </a>
                                            </td> 

                                        </tr>
                                        <tr style="display: none">
                                            <td>
                                                <!-- Jack Remark: Temporary hide it as no need from now -->
                                                <a href="access_point_policy.jsp">
                                                    <i class="pe pe-7s-speaker text-warning"></i>
                                                    <h5>Access Point Policy</h5>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        
                        <li class="dropdown">
                            <a href="#" onclick="socialSettingStep()" >
                                <i class="fa fa-twitter" title="Twitter Social Setting"></i>
                            </a>                            
                        </li>
                        
                        <%                            
                            double mainCredit = 0;
                            RmCreditinfo creditObj1 = null;
                            RmSubscriptionpackagedetails subscriObject1 = new SubscriptionManagement().getSubscriptionbyOwnerId(usrObj.getOwnerId());                            
                            creditObj1 = new CreditManagement().getDetailsByOwnerId(usrObj.getOwnerId());
                            session.setAttribute("advertiserSubscriptionObject", subscriObject1);
                        %> 
                                                
                        <li class="dropdown">
                            <a href="logout.jsp">
                                <i class="pe-7s-upload pe-rotate-90" title="Logout"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

        <!-- Navigation -->
        <aside id="menu">
            <div id="navigation">
                <div class="profile-picture">
                    <a href="./header.jsp">                                                                      
                        <img src="images/images.png" class="img-circle m-b" alt="profile" width="90"/>                       
                    </a>
                    <div class="stats-label text-color">
                        <span class="font-extra-bold font-uppercase"><%=socialNamed%></span>
                    </div>
                    <div class="tour-1">
                        <div id="sparkline1" class="small-chart m-t-sm"></div>
                    </div>
                    <hr/>
                    <div class="tour-2">
                        <h4 class="font-extra-bold m-b-xs">
                            <%
                                String expiry = "NA";
                                Calendar calendar = Calendar.getInstance();
                                Calendar expireDate = Calendar.getInstance();
                                BigDecimal bdRemainTotalCredit = new BigDecimal(0);
                                if (creditObj1 != null) {
                                    mainCredit = creditObj1.getMainCredit();                                    
                                    bdRemainTotalCredit = new BigDecimal(mainCredit);
                                    bdRemainTotalCredit = bdRemainTotalCredit.setScale(2, BigDecimal.ROUND_HALF_EVEN);
                                }
                                if(subscriObject1 != null){
                                    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a");
                                    expiry = dateFormatter.format(subscriObject1.getExpiryDate());
                                    calendar.setTime(subscriObject1.getExpiryDate());
                                    calendar.add(Calendar.DATE, -1);
                                    expireDate.setTime(subscriObject1.getExpiryDate());
                                }
                                boolean campaignDetailsFind = false; boolean socialMediaFind = false;
                                RmCampaigndetails[] adDetails = new CampaignManagement().getCampaignByOwnerId(usrObj.getOwnerId());
                                if(adDetails!= null){
                                   campaignDetailsFind = true; 
                                }
                                RmSocialmediadetails[] socialDetails = new SocialMediaManagement().getSocialMediaDetailsByOwnerId(usrObj.getOwnerId());
                                if(socialDetails != null){
                                    socialMediaFind = true;
                                }
//                                if(adDetails!= null && adDetails.getEmailAdImage() != null){
//                                   emailAdDetailsFind = true; 
//                                }
                                      
                            %>
                            <font id="mainCreditOfDev"> <%=bdRemainTotalCredit%></font>       
                        </h4>
                        <small class="text-muted">Remaining Credits</small></br>
                        <%if (new Date().getTime() >= expireDate.getTime().getTime() && !expiry.equalsIgnoreCase("NA")) {%>
                        <small class="text-muted"><font color="red">Expired On <%=expiry%></font></small>
                            <%} else if (new Date().getTime() >= calendar.getTime().getTime() && !expiry.equalsIgnoreCase("NA")) {%>
                        <small class="text-muted"><font color="red">Expires On <%=expiry%></font></small>
                            <%} else if (!expiry.equalsIgnoreCase("NA")) {%>
                        <small class="text-muted">Expires On <%=expiry%></small>
                        <%}%>
                    </div>
                </div>

                <ul class="nav" id="side-menu">
                    <li class="tour-3">                        
                        <%if(socialMediaFind){%>
                        <a onclick="socialMediaListDetails()"><span class="capitalize">Social Settings </span> </a>
                        <%}else{%>
                        <a onclick="socialMediaDetails()"><span class="capitalize">Social Settings </span> </a>
                        <%}%>
                    </li>
                    <li class="tour-4">
                        <%if(campaignDetailsFind){%>
                        <a onclick="campaignListDetails()"><span class="capitalize">Campaigns </span> </a>
                        <%}else{%>
                        <a onclick="campaignDetails()"><span class="capitalize">Campaigns </span> </a>
                        <%}%>
                    </li>
                    <li class="tour-4">
                        <a onclick='reports()'><span class="capitalize">Reports</span> </a>
                    </li>
                    <li class="tourVideo">                        
                        <a onclick='myInvoice()'> <span class="capitalize">Invoices</span></a>
                    </li>
                    <li class="tourVideo">                        
                        <%if (usrObj.getStripeSubscriptionId() != null) {%>
                        <a onclick='subscribe()'><span class="capitalize">Subscription</span></a>
                        <%} else {%>
                        <a onclick='subscribeNew()'><span class="capitalize">Subscription</span></a>
                        <%}%>
                    </li>
                </ul>
            </div>
        </aside>
        <div id="wrapper3"  style="display: none">
            <div id="wrapper"><br><br><br><br><br><br><br><br><br><br><br>
                <div id="loading" ><div class="text-center"><h3>Loading...</3></div><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> 
            </div>
        </div>

        <div id="wrapper2">
            <div id="wrapper">
                <div id="otherPage"></div>
                <%@include file="footer.jsp"%>
            </div>
        </div>
        <div id="wrapper1">
            <div id="wrapper">
                <div id="homePage">
                    <div class="content animate-panel">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="hpanel stats">
                                    <div class="panel-heading">

                                        Today's total credit used
                                    </div>
                                    <div class="panel-body h-200">
                                        <div class="stats-icon text-center ">
                                            <i class="pe-7s-share fa-4x"></i>
                                        </div>
                                        <div class="m-t-xl">
                                            <div class="progress m-t-xs full progress-small">
                                                <div id="percentage" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" role="progressbar" class=" progress-bar progress-bar-info">
                                                    <span class="sr-only">100%</span>
                                                </div>
                                            </div> 
                                            <div class="row" style="height: 60px">
                                                <div class="col-xs-6">
                                                    <small class="stats-label">Credit used</small>
                                                    <h4  id='creditUsed'></h4>
                                                </div>
                                                <div class="col-xs-6">
                                                    <small class="stats-label">% Credit used</small>
                                                    <h4 id='perCreditUsed'></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="hpanel">
                                    <div class="panel-heading">
                                        Last week total credit used
                                    </div>
                                    <div class="panel-body text-center h-200">
                                        <i class="pe-7s-graph1 fa-4x"></i>

                                        <h1 class="m-b-xs text-info" id="lastWeekAmount"></h1>
                                        <small id="weekMsg"></small>
                                        <h3 class="font-bold no-margins lead text-info row">
                                            Weekly Usage
                                        </h3> <!--<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit</small>-->
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="hpanel">
                                    <div class="panel-heading">

                                        Last month total credit used
                                    </div>
                                    <div class="panel-body text-center h-200">
                                        <i class="pe-7s-global fa-4x"></i>
                                        <h1 class="m-b-xs text-info" id="monthCredit"></h1>

                                        <small id="monthMsg"></small>
                                        <h3 class="font-bold no-margins lead text-info row">
                                            Monthly Usage
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-lg-3 col-sm-3" id="topmostServices" data-child="hpanel" data-effect="fadeInLeft">
                                <div class="hpanel">
                                    <div class="panel-heading">

                                        <span id="mostlyFive"></span>
                                    </div>
                                    <div class="panel-body" style="height: 213px">
                                        <div class="flot-chart text-center" style="width: 200;height:250">
                                            <div class="flot-chart-content"  id="flot-pie-chart"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3" id = "mostlyservices" style="display: none">
                                <div class="hpanel">
                                    <div class="panel-heading">
                                        Your mostly used services
                                    </div>
                                    <div class="panel-body" style="text-align: center;">                                        
                                        <img src="images/no_record_found.png" alt="No record found" width="200" height="170"/>
                                        <br>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12" data-child="hpanel" data-effect="fadeInLeft">
                                <div class="hpanel">
                                    <div class="panel-heading">
                                        Campaign Information And Statistics
                                    </div>
                                    <div class="panel-body">
                                        <div class="text-left">
                                            <i class="fa fa-bar-chart-o fa-fw"></i><span id="homeReportLable">  Today's Expense</span>
                                        </div>
                                        <div class="btn-group" style="float: right;">
<!--                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="generateTopServiceV2()">Top Five</button>-->
                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="creditperSer()">Today's Expense</button>
                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="toptrndingServices()">Today's Trending</button>
                                        </div>
                                        <div style="clear: both"></div>
                                        <div  id="topFiveSer" style="height: 215px" class="c3"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
                <%@include file="footer.jsp"%>
            </div>
        </div>
        <div id="tourDiv">
            <!-- Tour API Console -->        
            <div id="apiTour">     
                <div id="wrapper">
                    <div id="wrapperAPIConsoleTour" ></div>
                </div>
            </div>
            <!-- API Console End -->

            <!-- Tour API Console Window -->        
            <div id="apiConsoleTour">     
                <div id="wrapper">
                    <div id="wrapperAPIConsoleWindowTour" ></div>
                </div>
            </div>
            <!-- API Console Window End -->

            <!-- Tour report Window -->        
            <div id="reportTour">     
                <div id="wrapper">
                    <div id="reportWindowTour" ></div>
                </div>
            </div>
            <!-- Report Window End -->

            <!-- Tour invoice Window -->        
            <div id="invoiceTour">     
                <div id="wrapper">
                    <div id="invoiceWindowTour" ></div>
                </div>
            </div>
            <!-- Invoice Window End -->

            <!-- Tour subscribe Window -->        
            <div id="subscribeTour">     
                <div id="wrapper">
                    <div id="subscribeWindowTour" ></div>
                </div>
            </div>
            <!-- Subscribe Window End -->

            <!-- Tour Helpdesk Window -->        
            <div id="helpDeskTour">                 
                <div id="wrapper">
                    <div id="helpDeskWindowTour" ></div>  
                </div>
            </div>


        </div>        
        <script type="text/javascript">

            document.onreadystatechange = function () {
                if (document.readyState === 'complete') {

                    $('#perCreditUsed').ready(function () {
                        todayusedCredit("today");
                    });
                }
            };

            $('#wrapper2').hide();
            $('#wrapper3').hide();
        </script>

        <script>            
            $(document).ready(function () {
                var creditUsedData = fifteenDaysCreditExpenditure();
                var obj = [];
                for (var key in creditUsedData) {
                    var value = creditUsedData[key];
                    obj.push(value.txAmount);
                    //        alert(JSON.stringify(key)+" :: "+JSON.stringify(value.apiName));
                }
                $("#sparkline1").sparkline(obj, {
                    type: 'bar',
                    barWidth: 7,
                    height: '30px',
                    barColor: '#2381c4',
                    negBarColor: '#53ac2a'
                });

            });
            //    
            function fifteenDaysCreditExpenditure() {
                var s = './DailyTransactionAmount';
                var jsonData = $.ajax({
                    url: s,
                    dataType: "json",
                    async: false
                }).responseText;
                if (jsonData.length === 0) {
                    jsonData = [{"label": "NA", "value": 0, "value5": 0}];
                    return jsonData;
                } else {
                    var myJsonObj = JSON.parse(jsonData);
                    return myJsonObj;
                }
            }

            function getCreditStatus() {
                var s = './GetCredit';
                $.ajax({
                    type: 'POST',
                    url: s,
                    datatype: 'json',
                    data: $("#getBody").serialize(),
                    success: function (data) {
                        if (data.credits !== 'NA') {
                            $('#mainCreditOfDev').html(data.credits);
                        }
                    }});
            }
            getCreditStatus();
            setTimeout(function () {
                if (typeof (tour) != "undefined") {
                    tour.end();
                }
                startTour();
            }, 500);
            setTimeout(function () {
               creditperSer();
            }, 2000);
            test = document.getElementById("Ashish");
            collapse = document.getElementById("mobile-collapse");

        </script>
        <%if (showTourFirstSignUp != null && showTourFirstSignUp.equalsIgnoreCase("yes")) {
                request.getSession().setAttribute("showTourFirstSignUp", null);
        %>
        <script>
            $(document).ready(function () {
                setTimeout(function () {
                    firstTimeTour();
                    tour.restart();
                }, 1000);
            });
        </script>
        <%}%>
        
    </body>
</html>