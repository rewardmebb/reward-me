<%@page import="com.mollatech.rewardme.nucleus.db.RmPaymentdetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.PaymentManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.SubscriptionManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmSubscriptionpackagedetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmBrandownerdetails"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.mollatech.rewardme.nucleus.commons.GlobalStatus"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DecimalFormat"%>
<script src="scripts/my_invoice.js" type="text/javascript"></script>
<%
    String SessionId = (String) request.getSession().getAttribute("_brandownerSessionId");
    if (SessionId == null) {
        response.sendRedirect("logout.jsp");
        return;
    }
    RmBrandownerdetails parObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
    RmSubscriptionpackagedetails[] subscriptionObj = new SubscriptionManagement().listOfPackageSubscripedbyOwnerId(parObj.getOwnerId());
%>
<!-- Main Wrapper -->
<!--<div id="wrapper">-->
<div class="small-header transition animated fadeIn" id="invoiceHeader">
    <div class="hpanel">
        <div class="panel-body">
            <h2 class="font-light m-b-xs">
                Invoice
            </h2>
            <small>Know your billing history</small>
        </div>
    </div>
</div>
<div class="content animate-panel">
    <div class="row tour-invoiceTour">
        <div class="col-lg-12">
            <div class="hpanel">               
                <div class="panel-body">
                    <div class="table-responsive">
                        <%
                            if (subscriptionObj != null) {%>
                        <table id="apiInvoiceTableWindow" class="table table-striped table-bordered table-hover">
                            <thead id="invoiceHeader">
                                <tr>
                                    <th>Sr. No</th>
                                    <th>Package Name</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Invoice No.</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <th>Download</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    //if(false){
                                    Float paidAmount = 0.0f;
                                    PaymentManagement ppw = new PaymentManagement();
                                    DecimalFormat df = new DecimalFormat("#0.00");
                                    df.setMaximumFractionDigits(2);
                                    RmPaymentdetails paymentdetails = null;
                                    String paidDate = "NA";
                                    String paidTime = "NA";
                                    String invoiceNo = "NA";
                                    boolean recordFound = true;
                                    int count = 0;
                                    int paymentId = 0;
                                    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
                                    SimpleDateFormat timeFormatter = new SimpleDateFormat("hh:mm a");
                                    for (int i = 0; i < subscriptionObj.length; i++) {
                                        paymentdetails = ppw.getPaymentDetailsbyBrandOwnerAndSubscriptionID(subscriptionObj[i].getSubscriptionId(), parObj.getOwnerId());
                                        if (paymentdetails != null) {
                                            if (paymentdetails.getPaidOn() != null) {
                                                paidDate = dateFormatter.format(paymentdetails.getPaidOn());
                                                paidTime = timeFormatter.format(paymentdetails.getPaidOn());
                                            }
                                            if (paymentdetails.getPaidAmount() != null) {
                                                paidAmount = paymentdetails.getPaidAmount();
                                            }
                                            if (paymentdetails.getInvoiceNo() != null) {
                                                invoiceNo = paymentdetails.getInvoiceNo();
                                            }
                                            paymentId = paymentdetails.getPaymentId();
                                        }
                                        
//                                        if (subscriptionObj[i].getStatus() != GlobalStatus.DEFAULT) {
//                                            recordFound = false;
//                                            count++;
//                                            String securityAlert = subscriptionObj[i].getSecurityAndAlertDetails();
//                                            JSONObject reqJSONObj = null;
//                                            if (securityAlert != null) {
//                                                JSONArray alertJson = new JSONArray(securityAlert);
//                                                for (int iser = 0; iser < alertJson.length(); iser++) {
//                                                    JSONObject jsonexists1 = alertJson.getJSONObject(iser);
//                                                    if (jsonexists1.has(subscriptionObj[i].getBucketName())) {
//                                                        reqJSONObj = jsonexists1.getJSONObject(subscriptionObj[i].getBucketName());
//                                                        if (reqJSONObj != null) {
//                                                            break;
//                                                        }
//                                                    }
//                                                }
//                                            }
                                            //String passwordProtected = "";
//                                            if (reqJSONObj != null && reqJSONObj.getString("encryptedPDF").equals("enable")) {
//                                                if (parObj.getPartnerEmailid().length() >= 6) {
//                                                    passwordProtected = "This invoice is password protected. Open it with your First five letter of your registered email id with us.";
//                                                } else {
//                                                    passwordProtected = "This invoice is password protected. Open it with your email id, which is registered with us.";
//                                                }
//                                            }
                                            String packageName= subscriptionObj[i].getPackageName();
                                            String bucketName = packageName.toLowerCase();
                                            if (bucketName.contains("basic") && bucketName.contains("month")) {
                                                packageName = "Basic";
                                            } else if (bucketName.contains("basic") && bucketName.contains("year")) {
                                                packageName = "Basic";
                                            } else if (bucketName.contains("student") && bucketName.contains("month")) {
                                                packageName = "Student";
                                            } else if (bucketName.contains("student") && bucketName.contains("year")) {
                                                packageName = "Student";
                                            } else if (bucketName.contains("standard") && bucketName.contains("month")) {
                                                packageName = "Standard";
                                            } else if (bucketName.contains("standard") && bucketName.contains("year")) {
                                                packageName = "Standard";
                                            } else if (bucketName.contains("enterprise") && bucketName.contains("month")) {
                                                packageName = "Enterprise";
                                            } else if (bucketName.contains("enterprise") && bucketName.contains("year")) {
                                                packageName = "Enterprise";
                                            }
                                            count++;
                                            recordFound = false;
                                %>
                                <tr>
                                    <td><%=count%></td>
                                    <td><%=packageName%></td>
                                    <td><%=paidDate%></td>
                                    <td><%=paidTime%></td>
                                    <td><%=invoiceNo%></td>
                                    <td>AUD <%= df.format(paidAmount)%></td>
                                    <%if (subscriptionObj[i].getStatus() == GlobalStatus.PAID) {%>
                                    <td>Paid</td>
                                    <%} else if (subscriptionObj[i].getStatus() == GlobalStatus.UNPAID) {%>
                                    <td>Unpaid</td>
                                    <%} %>                                                                        
                                    <%if (subscriptionObj[i].getStatus() == GlobalStatus.PAID) {%>                                                                                                                     
                                    <td><a href="#" class="" onclick="getOnlineBillReceipt('<%=paymentId%>', '<%=subscriptionObj[i].getSubscriptionId()%>', '<%=invoiceNo%>')"><i class="fa fa-file-pdf-o fa-2x"></i></a></td>
                                            <%
                                    } else {%>
                                    <td><a href="#" class="disabled"><i class="fa fa-file-pdf-o"></i> View Invoice</a></td>
                                    <%}%>
                                </tr>
                                <%
                                    //}
                                %>
                           
                            <%
                                    
                                }
                                if (!recordFound && count != 0) {
                            %>
                            <script>
                                $(function () {
                                    // Initialize Example 2
                                    $('#apiInvoiceTableWindow').dataTable();
                                });
                            </script>
                            <%                   }%>
                            </tbody>
                        </table><%} else {
                        %>                             
                        <img src="images/no_record_found.jpg" alt="No record found" width="300px" height="300px" style="margin-left: 35%"/>                        
                        <script>
                            document.getElementById("invoiceHeader").style.display = 'none';
                        </script>
                        <%}%>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-bottom: 33%"></div>
    </div>        
</div>

<script>
    setInterval(function () {
        checkValidSession();
    }, 180000);
</script>
