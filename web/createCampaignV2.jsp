<%@page import="com.mollatech.rewardme.nucleus.db.RmBrandownerdetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.SocialMediaManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmSocialmediadetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmCountries"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.AddressManagement"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.CampaignManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmCampaigndetails"%>
<%@page import="org.json.JSONObject"%>
<!--<script src="scripts/coutrySelectBox.js" type="text/javascript"></script>-->
<script src="vendor/bootstrap-time/bootstrap.timepicker.min.js" type="text/javascript"></script>
<link href="vendor/bootstrap-time/bootstrap-timepicker.css" rel="stylesheet" type="text/css"/>
<link href="vendor/bootstrap-time/bootstrap-timepicker.css" rel="stylesheet" type="text/css"/>
<script src="vendor/bootstrap-time/bootstrap-timepicker.js" type="text/javascript"></script>
<link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css" />
<link rel="stylesheet" href="vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
<script src="scripts/profile.js" type="text/javascript"></script>
<div class="small-header transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <h2 class="font-light m-b-xs">
                Create Campaign 
            </h2>
            <small>View/Update your campaign </small>
        </div>
    </div>
</div>
<%
    String campaignId = request.getParameter("campaignId");
    String SessionId = (String) request.getSession().getAttribute("_brandownerSessionId");
    RmBrandownerdetails usrObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
    if (SessionId == null) {
        response.sendRedirect("logout.jsp");
        return;
    }
            
    RmCampaigndetails campaignDetails = null;
    if(campaignId != null && !campaignId.isEmpty()){
        campaignDetails = new CampaignManagement().getCampaignByIdDetails(Integer.parseInt(campaignId));
        
    }
    String message = null;int newCharLength =0; Integer messageLength = 240;
    if(campaignDetails != null && campaignDetails.getMessage() != null){
        message = campaignDetails.getMessage();
        char[] countemailAdContent = message.toCharArray();
        int charPresent = countemailAdContent.length;
        messageLength = messageLength - charPresent;
        // adjustment for Character count JS require 
        newCharLength = messageLength - charPresent;
        newCharLength = messageLength + charPresent;
        //adjustment end
    }
    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    //DateFormat timeformat = new SimpleDateFormat("hh:mm a");
    DateFormat timeformat = new SimpleDateFormat("HH:mm");    
    
    AddressManagement addobj = new AddressManagement();
    RmCountries [] allCountries = addobj.getAllCountries();    
    RmSocialmediadetails[] socialsettings = new SocialMediaManagement().getSocialMediaDetailsByOwnerId(usrObj.getOwnerId());
    
    if(socialsettings == null){
%>
    <script>
        function redirectToSocialSetting(){
            initializeToastr();
            toastr.error('Error - Please add your social settings first');                
            setTimeout(function () {
                socialMediaDetails();
            }, 4000);
        }
        redirectToSocialSetting();
    </script>
<%}    
%>                                       
<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">                                
                    <form id="personalInfo" class="form-horizontal">
                        <input type="hidden" id="partnerId" name="partnerId" value="">
                        <h6><i class="fa fa-user fa-2x text-info">  Campaign Information</i></h6><br/>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Title <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if(campaignDetails != null){%>
                                <input type="text" id="campaignTitle" title="Please enter campaign title" name="campaignTitle" class="form-control" value="<%=campaignDetails.getRewardTitle()%>" required>
                                
                                <%}else{%>
                                <input type="text" id="campaignTitle" title="Please enter campaign title" name="campaignTitle" class="form-control" required>
                                <%}%>
                            </div>
                            <label class="col-sm-2 control-label">Coupon Code. <span style="color: red">*</span></label>
                            <div class="col-sm-3">                               
                                <%if(campaignDetails != null){%>
                                <input type="text" id="couponCode" name="couponCode" class="form-control" value="<%=campaignDetails.getRewardCouponCode()%>" required>                               
                                <%}else{%>
                                <input type="text" id="couponCode" name="couponCode" class="form-control" required>                               
                                <%}%>
                            </div>                                                                        
                        </div>                        
                        <div class="form-group">                                    
<!--                            <label class="col-sm-2 control-label" >Reward Point <span style="color: red">*</span></label>
                            <div class="col-sm-3">                               
                                <%if(campaignDetails != null){%>
                                <input type="text" id="rewardPoint" name="rewardPoint" title="Please enter reward point" class="form-control" value="<%=campaignDetails.getRewardPoint()%>">
                                <%}else{%>
                                <input type="text" id="rewardPoint" name="rewardPoint" title="Please enter reward point" class="form-control">
                                <%}%>
                            </div>-->
                                
                            <label class="col-sm-2 control-label" >Promotion URL <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if(campaignDetails != null && campaignDetails.getPromotionUrl() != null){%>
                                <input type="text" id="promotionURL" name="promotionURL" title="Please enter promotion URL" class="form-control" value="<%=campaignDetails.getPromotionUrl()%>">
                                <%}else{%>
                                <input type="text" id="promotionURL" name="promotionURL" title="Please enter promotion URL" class="form-control">
                                <%}%>
                            </div>
                            <label class="col-sm-2 control-label" >Reward to no of user <span style="color: red">*</span></label>
                            <div class="col-sm-3">                               
                                <%if(campaignDetails != null){%>
                                <input type="text" id="noOfUser" name="noOfUser" title="Please enter no of user" value="<%=campaignDetails.getTargetUser()%>" class="form-control">
                                <%}else{%>
                                <input type="text" id="noOfUser" name="noOfUser" title="Please enter no of user" class="form-control">
                                <%}%>
                            </div>
                        </div>
                        <div class="form-group">                                                                
                            <label class="col-sm-2 control-label" >Campaign phrase </label>
                            <div class="col-sm-8">
                                <%if(campaignDetails != null && campaignDetails.getCampaignPhrase() != null){%>
                                <textarea id="campaignPhrase" name="campaignPhrase" title="Please enter campaign phrase" class="form-control"><%=campaignDetails.getCampaignPhrase()%></textarea>
                                <%}else{%>
                                <textarea id="campaignPhrase" name="campaignPhrase" title="Please enter campaign phrase" class="form-control"></textarea>
                                <%}%>
                                <small>You can add multiple meaningful pharse by put comma at the end</small>
                            </div>
                        </div>
                        <div class="form-group">                                                                
                            <label class="col-sm-2 control-label" >Reward message <span style="color: red">*</span></label>
                            <div class="col-sm-8">                                
                                <%if(message == null){%>
                                <textarea rows="4" cols="80" class="form-control" name="message" id="message" title="Please enter message" placeholder="Your message" onkeydown="limitText(this.form.message,this.form.countdown,<%=messageLength%>);" onkeyup='limitText(this.form.message,this.form.countdown,<%=messageLength%>);'></textarea>
                                <%}else{%>
                                <textarea rows="4" cols="80" class="form-control" name="message" id="message" title="Please enter message" placeholder="Your message" onkeydown="limitText(this.form.message,this.form.countdown,<%=newCharLength%>);" onkeyup='limitText(this.form.message,this.form.countdown,<%=newCharLength%>);'><%=message%></textarea>
                                <%}%>
                                <br>
                                You have
                                <input readonly type="text" name="countdown" size="3" value="<%=messageLength%>"> chars left                                
                            </div>
                        </div>
                        <h6><i class="fa fa-clock-o fa-2x text-info">  Campaign Period</i></h6><br/>    
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Start Date</label>
                            <div class="col-sm-3">                               
                                <%if(campaignDetails != null && campaignDetails.getCampaignStartDate() != null){
                                    String sDate = formatter.format(campaignDetails.getCampaignStartDate());
                                %>
                                <input type="text" id="datapicker1" name="datapicker1" class="form-control" value="<%=sDate%>">
                                <%}else{%>
                                <input type="text" id="datapicker1" name="datapicker1" class="form-control">
                                <%}%>
                            </div>
                            <label class="col-sm-2 control-label" >End Date</label>
                            <div class="col-sm-3">
                                <%if(campaignDetails != null && campaignDetails.getCampaignEndDate() != null){
                                    String eDate = formatter.format(campaignDetails.getCampaignEndDate());
                                %>
                                <input type="text" id="datapicker2" name="datapicker2" class="form-control" value="<%=eDate%>">  
                                <%}else{%>
                                <input type="text" id="datapicker2" name="datapicker2" class="form-control">  
                                <%}%>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Start Time</label>
                            <div class="col-sm-3">                               
                                <%if(campaignDetails != null && campaignDetails.getCampaignStartDate() != null){
                                    String stime = timeformat.format(campaignDetails.getCampaignStartDate());
                                %>
                                <input type="text" id="datapicker3" name="datapicker3" class="form-control clockpicker" value="<%=stime%>" data-autoclose="true">  
                                <%}else{%>
                                <input type="text" id="datapicker3" name="datapicker3" class="form-control clockpicker" >
                                <%}%>
                            </div>
                            <label class="col-sm-2 control-label" >End Time</label>
                            <div class="col-sm-3">                                
                                <%if(campaignDetails != null && campaignDetails.getCampaignEndDate() != null){
                                    String etime = timeformat.format(campaignDetails.getCampaignEndDate());
                                    
                                %>
                                <input type="text" id="datapicker4" name="datapicker4" class="form-control clockpicker" value="<%=etime%>"> 
                                <%}else{%>
                                <input type="text" id="datapicker4" name="datapicker4" class="form-control clockpicker"> 
                                <%}%>
                            </div>
                        </div>
                        <h6><i class="fa fa-map-marker fa-2x text-info"> Campaign For</i></h6><br/>    
                        <div class="form-group">                                                                        
                            <label class="col-sm-2 control-label">Country </label>
                            <div class="col-sm-3">                                                                                                
                                <%if(allCountries != null){%>
                                <select onchange="showstate(this.selectedIndex)" id="country" name="country" class="form-control">
                                    <option id="-1" value="-1">All over the world</option>
                                    <%
                                    for(int i = 0; i < allCountries.length; i++){
                                        
                                    %>
                                    
                                    <option  value="<%=allCountries[i].getId()%>" id="<%=allCountries[i].getId()%>"><%=allCountries[i].getName()%></option>
                                <%}%>
                                </select>
                                <%}%>
                                <%if(campaignDetails != null && campaignDetails.getCampaignInCountry() != null && campaignDetails.getCampaignInRegion() != null  && campaignDetails.getCampaignInCity()!= null){%>    
                                <script>                                    
                                    document.getElementById("<%=campaignDetails.getCampaignInCountry()%>").selected=true;                                                                          
                                    showstateV3(<%=campaignDetails.getCampaignInCountry()%>,<%=campaignDetails.getCampaignInRegion()%>,<%=campaignDetails.getCampaignInCity()%>);                                                                         
                                </script>
                                <%}else if(campaignDetails != null && campaignDetails.getCampaignInCountry() != null && campaignDetails.getCampaignInRegion() != null){%>
                                <script>
                                    document.getElementById("<%=campaignDetails.getCampaignInCountry()%>").selected=true; 
                                    showstateV2(<%=campaignDetails.getCampaignInCountry()%>,<%=campaignDetails.getCampaignInRegion()%>);
                                </script>
                                <%}else if(campaignDetails != null && campaignDetails.getCampaignInRegion() != null){%>                                
                                <script>
                                    document.getElementById("<%=campaignDetails.getCampaignInCountry()%>").selected=true; 
                                    showstate(<%=campaignDetails.getCampaignInCountry()%>);
                                </script>
                                <%}%>
                            </div>
                            <label class="col-sm-2 control-label" >State </label>
                            <div class="col-sm-3">                                
                                <select name="state1" id="state1" onchange="showcities(this.value)" class="form-control"> 
                                    <option id="-1" value="-1">Select State </option>
                                </select>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >City </label>
                            <div class="col-sm-3">                                
<!--                                <select name="state" id="state" class="form-control" onchange="print_cities('cities',this.selectedIndex);"> -->
                                <select name="city1" id="city1" class="form-control"> 
                                    <option id="-1" value="-1">Select City </option>
                                </select>
                            </div> 
                        </div>                            
                        <h6><i class="fa fa-users fa-2x text-info">  Target User have</i></h6><br/>    
                        <div class="form-group">                                                                        
                            <label class="col-sm-2 control-label">No of follower </label>
                            <div class="col-sm-3">                                
                                <%if(campaignDetails != null && campaignDetails.getNoOfFollowers() != null){%>
                                <input type="text" id="noOfFollower" name="noOfFollower" title="Please enter no of friends" value="<%=campaignDetails.getNoOfFollowers()%>" class="form-control">
                                <%}else{%>
                                <input type="text" id="noOfFollower" name="noOfFollower" title="Please enter no of friends" class="form-control">
                                <%}%>
                            </div>
                            <label class="col-sm-2 control-label" >No of friends </label>
                            <div class="col-sm-3">                                
                                <%if(campaignDetails != null && campaignDetails.getNoOfFriends() != null){%>
                                <input type="text" id="noOfFriends" name="noOfFriends" title="Please enter no of friends" value="<%=campaignDetails.getNoOfFriends()%>" class="form-control">
                                <%}else{%>
                                <input type="text" id="noOfFriends" name="noOfFriends" title="Please enter no of friends" class="form-control">
                                <%}%>
                            </div>                                    
                        </div>
                        <h6><i class="fa fa-street-view fa-2x text-info">  Social Settings</i></h6><br/>
                        <div class="form-group">    
                            <label class="col-sm-2 control-label">Twitter Setting </label>
                            <div class="col-sm-3">  
                                <select id="twitterSetting" name="twitterSetting" class="form-control">
                                    <option id="-1" value="-1">Select Twitter Setting</option>
                                        <%  
                                            if(socialsettings != null){
                                                for(int i = 0; i < socialsettings.length; i++){
                                                    if(socialsettings[i].getTwitterHandlerName()!=null && socialsettings[i].getUniqueSocialMediaId() != null){
                                        %>
                                        <option value="<%=socialsettings[i].getUniqueSocialMediaId()%>" id="<%=socialsettings[i].getUniqueSocialMediaId()%>"><%=socialsettings[i].getTwitterHandlerName()%></option>
                                        <%
                                                    }
                                                }
                                            }
                                        %>  
                                 </select>
                            </div>
                            <%if( campaignDetails != null && campaignDetails.getTwitterSocialSetting() != null && campaignDetails.getTwitterSocialSetting() != "null"){%>    
                            <script>
                                document.getElementById("<%=campaignDetails.getTwitterSocialSetting()%>").selected=true;
                            </script>
                            <%}%>
                        </div>
                        <br>  
                        <%if(campaignDetails != null){%>    
                        <button class="btn btn-primary ladda-button validateProfileForm" data-style="zoom-in" onclick="validateProfileFormV2('<%=campaignDetails.getCampaignId()%>')" style="display: none">Save changes</button>    
                        <%}else{%>
                        <button class="btn btn-primary ladda-button validateProfileForm" data-style="zoom-in" onclick="validateProfileForm()" style="display: none">Save changes</button>    
                        <%}%>
                    </form>
                    <%if(campaignDetails != null && campaignDetails.getLogos()!= null){
                        String[] arrlogos = campaignDetails.getLogos().split(",");
                    %> 
                    <h6><i class="fa fa-image fa-2x text-info"> Campaign Logo</h6></i> <br/>
                    <%
                            for(int j=0; j< arrlogos.length; j++){
                                if(j == 0){
                    %>                            
                    <img src="data:image/jpg;base64,<%=arrlogos[j]%>" class="m-b" alt="logo" width="100" height="100" style="margin-left: 18%"/>                         
                        <%}else{%>
                        <img src="data:image/jpg;base64,<%=arrlogos[j]%>" class="m-b" alt="logo" width="100" height="100" style="margin-left: 5%"/>                         
                        <%}%>                        
                    <%}}%>                                                    
                    <h6><i class="fa fa-image fa-2x text-info"> Upload Logo</h6></i> <br/>
                    <div style="background: #fff; height: 200px">
                        <div class="col-sm-8 col-lg-8 col-md-8" style="width: 68%;margin-left: 16%">
                            <form action="UploadEmailAttachme" method="post" class="dropzone" id="my-dropzone"></form>	
                            <small>Upload Logo pic. You can upload mutiple logo for campaign</small>
                        </div>
                    </div>
                    <br><br>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-8" style="margin-left: 16%">
                            <a class="btn btn-default" href="./header.jsp">Cancel</a>                            
                            <%if(campaignDetails != null){%>
                            <a class="btn btn-info ladda-button" id="partUpdate" data-style="zoom-in"  onclick="callValidateProfileFormV2()">Save changes</a>
                            <%}else{%>
                            <a class="btn btn-info ladda-button" id="partUpdate" data-style="zoom-in"  onclick="callValidateProfileForm()">Save changes</a>
                            <%}%>
                        </div>
                    </div>
                </div>
            </div>
        </div>                             
    </div>
</div>
<%
    int a = 250; int b = 250;
    session.setAttribute("logoImagesCount",0);
%>
<script src="vendor/clockpicker/dist/bootstrap-clockpicker.min.js"></script>
<script src="vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
       function limitText(limitField, limitCount, limitNum) {
          if (limitField.value.length > limitNum) {
            limitField.value = limitField.value.substring(0, limitNum);
          } else {
            limitCount.value = limitNum - limitField.value.length;
          }
        }
</script>
<script>
    $(document).ready(function () {        
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("#my-dropzone", {
            url: "UploadCampaignLogo",
            uploadMultiple: false,            
            maxFilesize: 5,
            maxFiles: 10,
            acceptedFiles: "image/*",
            dictInvalidFileType: "You can't upload files of this type, only Image file",
            autoProcessQueue: true,
            parallelUploads: 1,
            addRemoveLinks: true,
            dictDefaultMessage: "Drop your logo here to upload",
            init: function() {
                // Register for the thumbnail callback.
                // When the thumbnail is created the image dimensions are set.
                this.on("thumbnail", function(file) {
                  // Do the dimension checks you want to do
                  console.log("file.width == "+file.width);
                  console.log("file.height == "+file.height);
                  console.log("file.a == "+<%=a%>);
                  console.log("file.b == "+<%=b%>);
                  if (file.width > <%=a%> || file.height > <%=b%>) {
                    file.rejectDimensions()
                  }
                  else {
                    file.acceptDimensions();
                    //file.rejectDimensions()
                  }
                });
              },
//               Instead of directly accepting / rejecting the file, setup two
//             functions on the file that can be called later to accept / reject
//             the file.
            accept: function(file, done) {
              file.acceptDimensions = done;
              file.rejectDimensions = function() { done("The image must be or less than 250 x 250"); };
              // Of course you could also just put the `done` function in the file
              // and call it either with or without error in the `thumbnail` event
              // callback, but I think that this is cleaner.
            },
            removedfile: function(file) {
                removeImageFromSession(file.name);    
                var _ref;
                if (file.previewElement) {
                    if ((_ref = file.previewElement) != null) {
                        _ref.parentNode.removeChild(file.previewElement);
                    }
                }
                return this._updateMaxFilesReachedClass();
            }
        });
        
//        populateCountries("country", "state");           
        $("#campaignPhrase").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
    });
</script>
<script>
            $(function () {                
                $('#datapicker1').datepicker({
                    autoclose: true                    
                });
                $('#datapicker2').datepicker({
                    autoclose: true
                });                
                // ClockPicker
            $('.clockpicker').clockpicker({autoclose: true});
                
            });

</script>