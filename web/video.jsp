<link href="styles/mainVideo.css" rel="stylesheet" type="text/css"/>   
<div class="transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">            
            <h2 class="font-light m-b-xs" style="font-size: 22px!important">
                <font id="resourceName" style="font-weight: 80%"><b>Video Gallery</b></font>
            </h2> 
            <small>Learn with video tutorials </small>
        </div>
    </div>
</div>
<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="hpanel">
                <div class="panel-body">
                    <p>
                        <strong>Note,</strong>  
                    Videos currently under construction, Link are not enable.
                    </p>
                    <br>
<!--                    <ul class="nav nav-tabs">                        
                        <li class="active"><a data-toggle="tab" href="#tab-2"> Dashboard</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3"> Services</a></li>                        
                    </ul>
                    <div class="tab-content">
                        <div id="tab-2" class="tab-pane active"> -->
                            <br>
                                <article class="video" data-toggle="tooltip" data-placement="right" title="This video gives the details about how you can change your password and your profile details.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="#"><img class="videoThumb" src="images/video/dashoard.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;1) Dashboard</h5>
                                </article>

                                <article class="video" data-toggle="tooltip" data-placement="right" title="This video gives the details about how you can change your password and your profile details.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="#"><img class="videoThumb" src="images/video/dashoard.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle" >&nbsp;2) My Profile</h5>
                                </article>

<!--                                <article class="video" data-toggle="tooltip" data-placement="right" title="This video gives the details about the tour option and how it work.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/gpyyIJ1d-a0"><img class="videoThumb" src="images/video/quicktour.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;3) Quick Tour</h5>
                                </article>-->

                                <article class="video" data-toggle="tooltip" data-placement="right" title="This video gives the details about the services and how it work.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="#"><img class="videoThumb" src="images/video/service.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;4) Campaign</h5>
                                </article>

                                <article class="video" data-toggle="tooltip" data-placement="right" title="This video gives the details about the reports menu and explain each report options.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="#"><img class="videoThumb" src="images/video/dashoard.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;5) Reports</h5>
                                </article>                               
                                <article class="video" data-toggle="tooltip" data-placement="right" title="This video gives the details about the invoice menu and explain how it works.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="#"><img class="videoThumb" src="images/video/invoice.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;6) Invoices</h5>
                                </article>
                        </div>
<!--                        <div id="tab-3" class="tab-pane">                            

                                <article class="video" data-toggle="tooltip" data-placement="right" title="By using DB As Service we can expose our database like oracle, mysql and cassandra . User can fired various queries on database as per the requirement and easily access the database. We can also save our database connection using this services.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="#"><img class="videoThumb" src="images/video/dashoard.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;1) DB As Service<br></h5>
                                </article>

                                <article class="video" data-toggle="tooltip" data-placement="right" title="Service that lets us sign documents digitally without printing, scanning, or faxing. It's a win for both business and personal users because electronic signing is fast, easy, and secure. Electronic signatures are trusted by millions around the world and are available in dozens of languages. Send and sign anywhere, anytime, on any Internet-enabled device. Contracts, agreements, loans, leases, all can be signed quickly and securely.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="#"><img class="videoThumb" src="images/video/dashoard.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;2) Document Utility</h5>
                                </article>

                                
                                <article class="video" data-toggle="tooltip" data-placement="right" title="We have build for you to a true value add service for Instant Web Chat (with no client software installation) into your workflow. You can generate conference video call and it works on 97% browsers like chrome, opera, Firefox as well android and iOS browsers. Also you can password protect session for the users joining in.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="#"><img class="videoThumb" src="images/video/dashoard.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;3) Instant WebChat</h5>
                                </article>
                                
                                <article class="video" data-toggle="tooltip" data-placement="right" title="The Image Utility service provides various image processing features like image format conversion to convert an image form one to another image format, extract image metadata feature to retrieve meta data of image, making GIF file from using multiple images, adding text watermark on image, encrypt data in the image and retrieve encrypted data from image.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="#"><img class="videoThumb" src="images/video/dashoard.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;4) Image Utility</h5>
                                </article>
                                
                                <article class="video" data-toggle="tooltip" data-placement="right" title="A reverse proxy provides an additional level of abstraction and control to ensure the smooth flow of network traffic between clients and servers. This service provides high level of security for servers using reverse proxy url.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="#"><img class="videoThumb" src="images/video/dashoard.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;5) Reverse Proxy</h5>
                                </article>
                                <article class="video" data-toggle="tooltip" data-placement="right" title="We have built a lot of Value Added Services that help in quick rapid development of your workflows. Few of them to mention are EmailId Validation, QR Code And Secure QR Code Generation, Shorten URL with Hit count, ICS Calendar Generation, JSON/CSV/XML Format validator, Credit Card Number Validator, Phone Number Validator and Bank Finder.">
                                    <figure>
                                        <a class="fancybox fancybox.iframe" href="#"><img class="videoThumb" src="images/video/dashoard.jpg"></a>
                                    </figure>
                                    <h5 class="videoTitle">&nbsp;6) Value Added Service</h5>
                                </article> 
                            
                        </div>-->
                        
                    </div>
                 
                </div>
                  
            </div>
        </div>
<!--        <div class="col-lg-12" style="margin-top: 35%"></div>-->
    </div>    
<!--    <script src="scripts/video2/jquery.fancybox.js" type="text/javascript"></script>-->
    <script src="scripts/video2/global.min.js" type="text/javascript"></script>  
