<%@page import="com.mollatech.rewardme.nucleus.commons.TaxCalculation"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.ApprovedPackageManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmApprovedpackagedetails"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="com.mollatech.rewardme.nucleus.commons.GlobalStatus"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.bouncycastle.util.encoders.Base64"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.rewardme.common.GenerateInvoiceId"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmBrandownerdetails"%>
<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Page title -->
    <title>Advertisement Portal</title>
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="styles/style.css">
</head>
<body>
    <%            
        RmBrandownerdetails ownerObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");        
        String SessionId = (String) request.getSession().getAttribute("_brandownerSessionId");
        String packageDetails = (String) request.getSession().getAttribute("selectedBrandOwnerPackageDetails");
        String stripePublishableKey = LoadSettings.g_sSettings.getProperty("stripe.publishableKey");
        String packageName = "";
        SimpleDateFormat invoiceDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:MM a");
        String invoiceid = GenerateInvoiceId.getDate() + ownerObj.getOwnerId() + GenerateInvoiceId.getRandom();                        
        RmApprovedpackagedetails[] packageObj = null;       
        packageObj = new ApprovedPackageManagement().getPackageRequestsbyStatus(SessionId, GlobalStatus.APPROVED);        
        if (packageObj != null) {            
            if(packageDetails != null){    
            for (RmApprovedpackagedetails reqbucketdetails : packageObj) {
                    String bucketName = reqbucketdetails.getPackageName().toLowerCase();
                    if (bucketName.contains("basic") && bucketName.contains("month") && packageDetails.equalsIgnoreCase("baMo")) {
                        packageName = reqbucketdetails.getPackageName();
                    } else if (bucketName.contains("basic") && bucketName.contains("year") && packageDetails.equalsIgnoreCase("baYr")) {
                        packageName = reqbucketdetails.getPackageName();
                    } else if (bucketName.contains("student") && bucketName.contains("month") && packageDetails.equalsIgnoreCase("stMo")) {
                        packageName = reqbucketdetails.getPackageName();
                    } else if (bucketName.contains("student") && bucketName.contains("year") && packageDetails.equalsIgnoreCase("stYr")) {
                        packageName = reqbucketdetails.getPackageName();
                    } else if (bucketName.contains("standard") && bucketName.contains("month") && packageDetails.equalsIgnoreCase("stdMo")) {
                        packageName = reqbucketdetails.getPackageName();
                    } else if (bucketName.contains("standard") && bucketName.contains("year") && packageDetails.equalsIgnoreCase("stdYr")) {
                        packageName = reqbucketdetails.getPackageName();
                    } else if (bucketName.contains("enterprise") && bucketName.contains("month") && packageDetails.equalsIgnoreCase("entMo")) {
                        packageName = reqbucketdetails.getPackageName();
                    } else if (bucketName.contains("enterprise") && bucketName.contains("year") && packageDetails.equalsIgnoreCase("entYr")) {
                        packageName = reqbucketdetails.getPackageName();
                    }
                }
            }            
        }

        RmApprovedpackagedetails packageObject = new ApprovedPackageManagement().getReqPackageByName(SessionId,packageName);               
        NumberFormat form = new DecimalFormat("#0.00");
        Double totalPaid = new Double(0);        

        // variable for gst tax cal
        Double totalAmountWithoutTax = 0.00;        
        //Double gstTax = 0.00;
        Map<String, Double> amount = new LinkedHashMap<String, Double>();
        Map<String, String> taxMap = new LinkedHashMap<String, String>();
        if (packageObject != null) {            
            packageName = packageObject.getPackageName();
            session.setAttribute("_originalPackageName", packageName);
            double planAmount = packageObject.getPlanAmount();            
            String bucketName = packageName.toLowerCase();                
            if (bucketName.contains("basic") && bucketName.contains("month")) {
                packageName = "Basic";
            } else if (bucketName.contains("basic") && bucketName.contains("year")) {
                packageName = "Basic";
            } else if (bucketName.contains("student") && bucketName.contains("month")) {
                packageName = "Student";
            } else if (bucketName.contains("student") && bucketName.contains("year")) {
                packageName = "Student";
            } else if (bucketName.contains("standard") && bucketName.contains("month")) {
                packageName = "Standard";
            } else if (bucketName.contains("standard") && bucketName.contains("year")) {
                packageName = "Standard";
            } else if (bucketName.contains("enterprise") && bucketName.contains("month")) {
                packageName = "Enterprise";
            } else if (bucketName.contains("enterprise") && bucketName.contains("year")) {
                packageName = "Enterprise";
            }
            amount.put(packageName, planAmount);
            totalPaid += planAmount;                                    
            totalAmountWithoutTax = totalPaid;
            String taxRate = packageObject.getTax();
            taxMap = TaxCalculation.calculateTax(taxRate, totalPaid);
            String calculatedGSTDetail = (String) taxMap.get("GST Tax");            
            String[] calculatedGST = calculatedGSTDetail.split(":");           
            totalPaid += Double.parseDouble(calculatedGST[1]);                   
        }
        double roundedTMREQAMOUNT = (double) Math.round(totalPaid * 100) / 100;
        String tmWellFormAmount = form.format(roundedTMREQAMOUNT);    
        session.setAttribute("_invoiceId", invoiceid);
        session.setAttribute("_packageObject", packageObject);
        String stripeAmount = "0";
        String gstNumber = "NA";
        String strgstNum = (String) LoadSettings.g_sSettings.getProperty("gst.number");
        if (strgstNum != null) {
            gstNumber = strgstNum;
        }
    %>
    <div id="wrapper" style="margin: 0 0 0 0px!important;min-height: 80px!important">
        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12 col-centered">
                    <div class="hpanel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <img src="images/imageedit_1_9895468797.png" width="150" style="padding-left: 20px!important" alt=""/>
                                    <!--                            <h4>Invoice <small>IN-9177283-2016</small></h4>-->
                                </div>
                                <div class="col-md-6">
                                    <div class="text-right">                                
                                        <form action="./CreateStripeCharge" method="POST">
                                            <script
                                                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                                data-key="<%=stripePublishableKey%>"
                                                data-amount="<%=stripeAmount%>"
                                                data-name="Reward Me"
                                                data-description="<%=packageName%>"
                                                data-image="images/front-logo.png"
                                                data-locale="auto"                                                    
                                                data-currency="aud">
                                            </script>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body p-xl">
                            <div class="row m-b-xl">
                                <div class="col-sm-6">
                                    <h4>Invoice No. <%=invoiceid%></h4>

                                    <address>
                                        <strong>Blue Bricks Pty Ltd.</strong><br>
                                        Unit 39, 118 Adderton Road,<br>
                                        Carlingford 2118 NSW,</br>
                                        Australia</br>
                                        <%if(!gstNumber.equalsIgnoreCase("na")){%>
                                        GST No. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=gstNumber%>
                                        <%}%>
                                        <!--                                <abbr title="Phone">P:</abbr> (831) 758-7200-->
                                    </address>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <span>To:</span>
                                    <address>
                                        <strong><%=ownerObj.getBrandName()%></strong><br>
                                        <!--                                60 Mortensen Avenue<br>
                                                                        Salinas, CA 123343<br>-->
                                        <abbr title="Email">E:</abbr> <%=ownerObj.getEmail()%></br>                                                                                          
                                        <p>
                                            <span><strong>Date:</strong> <%=invoiceDateFormat.format(new Date())%></span><br/>                                
                                        </p>
                                    </address>                            

                                </div>
                            </div>

                            <div class="table-responsive m-t col-md-12">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Item List</th>
                                            <th>Details</th>
                                            <th></th>                             
                                            <th>Total Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            int count = 1;
                                            Double totalAmount = 0.0;
                                            DecimalFormat df = new DecimalFormat("#0.00");
                                            df.setMaximumFractionDigits(2);                                                                                                                                  
                                        %>
                                        
                                        <%
                                                 
                                            for (Map.Entry<String, Double> entry : amount.entrySet()) {
                                        %>     
                                        <tr>

                                            <!--<td colspan="2"></td>-->
                                            <td><%=entry.getKey()%></td>
                                            <td></td>
                                            <td></td>
                                            <td style="">AUD <%=df.format(entry.getValue())%></td>
                                        </tr>
                                        <%
                                                count++;
                                                totalAmount = totalAmount + entry.getValue();
                                            }
                                            totalAmountWithoutTax = totalAmount;                                            
                                            session.setAttribute("totalPaymentAmountWithoutTax", totalAmountWithoutTax);                                            
                                            if (!taxMap.isEmpty()) {
                                                for (Map.Entry<String, String> entry : taxMap.entrySet()) {
                                                    if (entry.getKey().equalsIgnoreCase("GST Tax")) {
                                                        String[] taxArr = entry.getValue().split(":");

                                        %>
                                        <tr>
                                            <!--<td colspan="2"></td>-->

                                            <td><%=entry.getKey()%></td>

                                            <td><%=taxArr[0]%> %</td>
                                            <td></td>
                                            <td style="">AUD <%=df.format(Double.parseDouble(taxArr[1]))%></td>
                                        </tr>
                                        <%
                                                        count++;
                                                        totalAmount = totalAmount + Float.parseFloat(taxArr[1]);
                                                    }
                                                }
                                            }
                                            session.setAttribute("packageNameStripe", packageName);
                                            session.setAttribute("_grossAmount", tmWellFormAmount);
                                            stripeAmount = tmWellFormAmount.replace(".", "");
                                        %> 
                                        <tr>
                                            <!--<td colspan="2"></td>-->
                                            <td></td>  
                                            <td colspan="2" style="text-align: right"><strong>TOTAL</strong></td>
                                            <td  id="totalAmount">AUD <%=df.format(totalAmount)%></td>
                                        </tr>        
                                    </tbody>
                                </table>
                            </div>                    
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="m-t"><strong>PRIVACY STATEMENT</strong></br></br>
                                        Blue Bricks'S PRIVACY STATEMENT In its effort to ensure compliance to the Personal Data Protection Act 2010 (PDPA), Blue Bricks has put in place a personal data protection policy which shall govern the use and protection of your personal data as Blue Bricks's customer. For details of the policy, 
                                        please refer to Blue Bricks's Privacy Statement at http://www.blue-bricks.com, which may be reviewed by Blue Bricks from time to time.
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Vendor scripts -->
    <script src="vendor/jquery/dist/jquery.min.js"></script>
    <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
    <script src="vendor/iCheck/icheck.min.js"></script>
    <script src="vendor/sparkline/index.js"></script>
    <!-- App scripts -->
    <script src="scripts/homer.js"></script>
</body>


