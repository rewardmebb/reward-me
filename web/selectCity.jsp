
<%@page import="com.mollatech.rewardme.nucleus.db.RmCities"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmStates"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.AddressManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    String reqcityId = request.getParameter("cityId");
    System.out.println("reqcityId >> "+reqcityId);
%>
    <div class="control-group">
        <label class="control-label"  for="username"></label>
        <div class="controls">
            <select id="_city12" name="_city12"  class="form-control span2"  style="width: 100%">
                <option value="-1">Select City</option> 
                <%
                    String SessionId = (String) request.getSession().getAttribute("_brandownerSessionId");
                    String stateId = request.getParameter("_stateID");
                    int _stateId = Integer.parseInt(stateId);
                    if (!stateId.equals("-1")) {                        
                        AddressManagement addObj = new AddressManagement();
                        RmCities [] cityObj = addObj.getAllCityByStateId(SessionId, _stateId);
                        if(cityObj != null){           
                            for(int i = 0; i < cityObj.length; i++){  
                                if(reqcityId != null && !reqcityId.equalsIgnoreCase("null") && reqcityId.equalsIgnoreCase(String.valueOf(cityObj[i].getId()))){
                %>
                <option value="<%=cityObj[i].getId()%>" selected><%=cityObj[i].getName()%></option>               
                <%}else{
%>
                <option value="<%=cityObj[i].getId()%>"><%=cityObj[i].getName()%></option> 
                
                <%    }
                      }
                    }else{%>
                            <option value="-1">Select City</option>  
                            <% }
                        } else {%>
                <option value="-1">Select City</option>  
                <% }%>
                </select>           
        </div>
    </div>
                