<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.SubscriptionManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmSubscriptionpackagedetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmBrandownerdetails"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.Iterator"%>
<script src="scripts/packageDetails.js" type="text/javascript"></script>
<%    
    String packageName = "NA";
    String gstRate = "0";
    String perCampaign = "0";
    String userSearched = "0";
    RmBrandownerdetails parObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
    RmSubscriptionpackagedetails subscriObject1 = new SubscriptionManagement().getSubscriptionbyOwnerId(parObj.getOwnerId());
    if (subscriObject1 != null) {
        packageName = subscriObject1.getPackageName();
        String taxRate = subscriObject1.getTax();
        if (taxRate != null) {            
            gstRate = taxRate;            
        }
        String creditDeduction = subscriObject1.getCreditDeductionConfiguration();
        if(creditDeduction != null){
            JSONObject jsonObj = new JSONObject(creditDeduction);
            if(jsonObj.has("perCampaign")){
                perCampaign = jsonObj.getString("perCampaign");
            }
            if(jsonObj.has("userSearched")){
                userSearched = jsonObj.getString("userSearched");
            }
        }
        
        String bucketName = subscriObject1.getPackageName().toLowerCase();
            if (bucketName.contains("basic") && bucketName.contains("month")) {
                packageName = "Basic";
            } else if (bucketName.contains("basic") && bucketName.contains("year")) {
                packageName = "Basic";
            } else if (bucketName.contains("student") && bucketName.contains("month")) {
                packageName = "Student";
            } else if (bucketName.contains("student") && bucketName.contains("year")) {
                packageName = "Student";
            } else if (bucketName.contains("standard") && bucketName.contains("month")) {
                packageName = "Standard";
            } else if (bucketName.contains("standard") && bucketName.contains("year")) {
                packageName = "Standard";
            } else if (bucketName.contains("enterprise") && bucketName.contains("month")) {
                packageName = "Enterprise";
            } else if (bucketName.contains("enterprise") && bucketName.contains("year")) {
                packageName = "Enterprise";
            }
    }
%>
<!-- Main Wrapper -->
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <h4 class="font-light m-b-xs">
                    <b> My Subscription - <%=packageName%></b>
                </h4>
                <small>Your subscribed package details</small>
            </div>
        </div>
    </div>
    <div class="content animate-panel">
        <%if(subscriObject1 != null){%>
        <div class="row">
        <div class="col-lg-12">
            <div class="hpanel stats">
                <div class="panel-body">
                    <div>
                        <i class="pe-7s-cash fa-4x"></i>
                        <span class="m-xs text-success" style="font-size: 40px;">AUD <%=subscriObject1.getPlanAmount()%></span>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div>
            <div class="row">
                <div class="col-lg-12 form-horizontal">
                    <div class="hpanel">
                        <div class="panel-heading">                            
                            Rate
                        </div>
                        <div class="panel-body">
                            <div class="col-md-6">                               
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Credits</label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value="<%=new BigDecimal(subscriObject1.getMainCredits())%>" disabled></div>
                                </div>                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Period</label>
                                    <div class="col-sm-8">
                                        <% if (subscriObject1.getPackageDuration() != null && !subscriObject1.getPackageDuration().equals("")) {
                                                if (subscriObject1.getPackageDuration().equals("7")) {%>
                                        <input type="text" class="form-control" value="7 Days" disabled/>
                                        <%} else if (subscriObject1.getPackageDuration().equals("14")) {%>
                                        <input type="text" class="form-control" value="14 Days" disabled/>
                                        <%} else if (subscriObject1.getPackageDuration().equals("monthly")) {%>
                                        <input type="text" class="form-control" value="1 Month" disabled/>
                                        <%} else if(subscriObject1.getPackageDuration().equals("yearly")){%>
                                        <input type="text" class="form-control" value="1 year" disabled/>  
                                        <%}else{%>
                                        <input type="text" class="form-control" value="NA" disabled/>  
                                        <%}} else {%>                                                
                                        <input type="text" class="form-control" value="NA" disabled>
                                        <%}%>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">GST Tax Rate (%)</label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value=<%=gstRate%> % disabled /></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Credit deduction per campaign</label>
                                    <div class="col-sm-8">                                        
                                        <input type="text" class="form-control" value="<%=perCampaign%>" disabled>                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Credit deduction per user search</label>
                                    <div class="col-sm-8">                                        
                                        <input type="text" class="form-control" value="<%=userSearched%>" disabled>                                          
                                    </div>
                                </div>                                                                                                
                            </div>
                        </div>
                    </div>
                </div>
               
                <div class="col-md-12" style="margin-bottom: 7%"></div>
            </div>
        </div>
        <%}else{%>
        <div class="row">
            <div class="col-md-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <i class="pe-7s-wallet text-success big-icon"></i>
                        <h1></h1>
                        <strong>You have not subscribed yet</strong>
                        <p>
                            Sorry, please subscribe package first.
                        </p>
                        <a href="home.jsp" class="btn btn-xs btn-success">Go back to home page</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="margin-bottom: 10%"></div>
        </div>    
        <%}%>
    </div>


