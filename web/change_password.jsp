
<%@page import="com.mollatech.rewardme.nucleus.db.RmBrandownerdetails"%>
<%
    RmBrandownerdetails ownerObj = null;
    final int ADMIN = 0;
    final int OPERATOR = 1;
    final int REPORTER = 2;
    String PartnerType = "ADMIN";
     ownerObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
    
%>
<!-- Main Wrapper -->

    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <h2 class="font-light m-b-xs">
                    Change Password
                </h2>
                <!--<small>Change your password from here</small>-->
            </div>
        </div>
    </div>
    <div class="content animate-panel">
        <div>
            <div class="row">
                <form id="change_password" class="form-horizontal">
                    <div class="col-lg-12">
                        <div class="hpanel">
                            
                            <div class="panel-body">
                                <%if (ownerObj.getPassword() != null) {%>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Old Password</label>
                                    <div class="col-sm-3"><input type="password" id="current_password" name="current_password" placeholder="Enter the current password" class="form-control"></div>
                                </div>
                                <%}%>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">New Password</label>
                                    <div class="col-sm-3"><input type="password" class="form-control" id="new_password" name="new_password" placeholder="Enter the new password"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Confirm Password</label>
                                    <div class="col-sm-3"><input type="password" id="confirm_password" name="confirm_password" placeholder="Confirm the new password" class="form-control"></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <a class="btn btn-default" onclick="home()">Cancel</a>
                                        <button class="btn btn-primary ladda-button" data-style="zoom-in" id="changePasswordButton" onclick="ChangePassword()">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="col-md-12" style="margin-bottom: 22%"></div>           
            </div>
        </div>
    </div>
<!-- Footer-->