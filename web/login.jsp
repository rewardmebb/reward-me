<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Page title -->
        <title>Reward Me | Login</title>
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" type="images/front-logo.png" href="images/front-logo.png" />
        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
        <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
        <link rel="stylesheet" href="vendor/animate.css/animate.css" />
        <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
        <link rel="stylesheet" href="vendor/ladda/dist/ladda-themeless.min.css" />
        <link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
        <script src="scripts/utilityFunction.js" type="text/javascript"></script>
        <script src="scripts/partnerRequest.js" type="text/javascript"></script>
        <!-- App styles -->
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
        <link rel="stylesheet" href="styles/style.css">
    </head>
    <body class="blank" style="background: rgba(43,48,131,1);
          background: -moz-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          background: -webkit-gradient(left top, right top, color-stop(0%, rgba(43,48,131,1)), color-stop(50%, rgba(35,129,196,1)), color-stop(100%, rgba(43,48,131,1)));
          background: -webkit-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          background: -o-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          background: -ms-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          background: linear-gradient(to right, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2b3083', endColorstr='#2b3083', GradientType=1 );">
        <%
            String showTourFirstSignUp = (String) request.getSession().getAttribute("showTourFirstSignUp");
            if (showTourFirstSignUp == null) {
                response.setHeader("Cache-Control", "no-cache");
                response.setHeader("Cache-Control", "no-store");
                response.setHeader("Pragma", "no-cache");
                response.setDateHeader("Expires", 0);
                session.invalidate();
        %>
        <script>
            function deleteAllCookies() {

                var cookies = document.cookie.split(";");
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = cookies[i];
                    var eqPos = cookie.indexOf("=");
                    var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
                    document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
                }
            }
            deleteAllCookies();
        </script>
        <%}

        %>
        <!-- Simple splash screen-->
        <div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Loading...</h1><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
        <!--[if lt IE 7]>
        <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="color-line"></div>
        <!--
                <div class="back-link">
                    <a href="registration.jsp" class="btn btn-primary">Guest? Register here</a>
                </div>-->

        <div class="login-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center m-b-md">
                        <h3 style="color:#fff">Reward Me</h3>
                        <small></small>
                    </div>
                    <div class="hpanel">
                        <div class="panel-body">
                            <form action="#" id="login_form">
                                <div class="form-group">

                                    <input type="hidden" id="loginPage" name="loginPage" value="1">
                                    <label class="control-label" for="email">Email</label>
                                    <input type="text" placeholder="example@gmail.com" onkeypress="goFunction(event)" title="Please enter you email" required="" value="" id="_name" name="_name" class="form-control" autofocus>
                                    <!--                                    <span class="help-block small">Your registered email</span>-->
                                </div>
                                <div class="form-group"  id="passDiv">
                                    <label class="control-label" for="password">Password</label>
                                    <input type="password" onkeypress="loginFunction(event)" title="Please enter your password" placeholder="******" required="" value="" id="_passwd" name="_passwd" class="form-control" >
                                    <span class="help-block small"></span>
                                </div>
                                <a  id="goButton" href="#goButton" class="btn btn-success ladda-button btn-block" data-style="zoom-in" style="display: none" onclick="enableLogin()">Go</a>
                                <a  class="btn btn-success ladda-button btn-block" data-style="zoom-in" onclick="userLogin('html')" id="loginButton" href="#loginButton">Login</a>
                                <a class="btn btn-default btn-block" href="reset_password.jsp">Forgot Password?</a>
                            </form>


                            <br><br><br>
<!--                            <label class="control-label" for="email">
                                Or, Sign in With Social Networks </label><br>  <br>  
                            
                            <div style="text-align: center">
                                <a href="#" class="btn btn-danger2 btn-outline ladda-button" id="googleSignIn" data-style="zoom-in"  type="button"><i class="fa fa-google"></i> <br/>Google</a>
                                <a href="#" class="btn btn-info btn-outline ladda-button" onclick="fb_login();" data-style="zoom-in" type="button" id="facebookLadda"><i class="fa fa-facebook-official"></i> <br/>Facebook</a>
                                <a href="#" class="btn btn-primary btn-outline ladda-button" type="button" data-style="zoom-in" onclick="liAuth()" id="linkedInLadda"><i class="fa fa-linkedin"></i> <br/>LinkedIn</a>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center" style="color:#fff">
                    <strong>Powered by Axiom Protect 2.0</strong>
                </div>
                <%                    java.util.Date dFooter = new java.util.Date();
                    SimpleDateFormat sdfFooter = new SimpleDateFormat("yyyy");
                    String strYYYY = sdfFooter.format(dFooter);                                                            

                %>
                <div align="center"  class="col-md-12 text-center" style="color:#fff">
                    <p>Platform & Owned By BlueBricks Technologies 2009-<%=strYYYY%><br> (<a href="http://www.blue-bricks.com" target="_blank">www.blue-bricks.com</a>)</p>
                </div>
            </div>
        </div>


        <!-- Vendor scripts -->
        <script src="vendor/jquery/dist/jquery.min.js"></script>
        <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
        <script src="vendor/iCheck/icheck.min.js"></script>
        <script src="vendor/sparkline/index.js"></script>
        <script src="vendor/ladda/dist/spin.min.js"></script>
        <script src="vendor/ladda/dist/ladda.min.js"></script>
        <script src="vendor/ladda/dist/ladda.jquery.min.js"></script>
        <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
        <script src="vendor/sweetalert/lib/sweet-alert.min.js"></script>
        <script src="vendor/toastr/build/toastr.min.js"></script>
<!--        <script src="https://apis.google.com/js/platform.js?onload=onLoadGmail" async defer></script>-->
        <!--        <script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>-->
        <!-- App scripts -->
        <script src="scripts/homer.js"></script>
<!--        <script src="scripts/socialMedia.js"></script>-->

        <script>
                                    setLoginFocus();
                                    //onLoadfacebook();
                                    $('body').on('keydown', '#_name', function (e) {
                                        if (e.which == 9) {
                                            e.preventDefault();
//                                        document.getElementById("goButton").focus();
//                                        document.getElementById('goButton').href = "#/  ";
                                            document.getElementById("_passwd").focus();
                                        }
                                    });
                                    //server linkedIn key
                                    //var linkedInKey = '8137zz6ug56k1w';

                                    // localhost linkedIn key
                                    var linkedInKey = '81qhnzg756okk4';

                                    initializeToastr();


        </script>
          
    </body>
</html>


