
<%@page import="com.mollatech.rewardme.nucleus.db.RmBrandownerdetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.SocialMediaManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmSocialmediadetails"%>
<%@page import="org.json.JSONObject"%>
<script src="scripts/coutrySelectBox.js" type="text/javascript"></script>
<script src="vendor/bootstrap-time/bootstrap.timepicker.min.js" type="text/javascript"></script>
<link href="vendor/bootstrap-time/bootstrap-timepicker.css" rel="stylesheet" type="text/css"/>
<link href="vendor/bootstrap-time/bootstrap-timepicker.css" rel="stylesheet" type="text/css"/>
<script src="vendor/bootstrap-time/bootstrap-timepicker.js" type="text/javascript"></script>
<script src="scripts/profile.js" type="text/javascript"></script>
<%
    String SessionId = (String) request.getSession().getAttribute("_brandownerSessionId");
    if (SessionId == null) {
        response.sendRedirect("logout.jsp");
        return;
    }
    
%>
<div class="small-header transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            
   <%
       String socialsettingId = request.getParameter("socialSetting");
       SocialMediaManagement obj = new SocialMediaManagement();
       RmSocialmediadetails socialMediaSetting = null;
       if(socialsettingId != null && !socialsettingId.isEmpty()){
           int socialId = Integer.parseInt(socialsettingId);           
           socialMediaSetting = obj.getSocialMeidaDetailsById(socialId);
       }
       RmBrandownerdetails usrObject = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
       RmSocialmediadetails[] socialDetails = new SocialMediaManagement().getSocialMediaDetailsByOwnerId(usrObject.getOwnerId());
       if(socialDetails != null && socialMediaSetting == null){
    %>   
    <script>
        function socialMediaListDetails(){
            if ((test.className).indexOf("page-small") > -1) {
                test.classList.remove("show-sidebar");
                test.classList.add("hide-sidebar");
            }
            if ((collapse.className).indexOf("in") > -1) {
                collapse.classList.remove("in");
            }
            $('#otherPage').empty();
            $('#wrapper1').hide();
            document.getElementById("wrapper3").style.display = "block";
            var s = "getSocialSettingDetails.jsp";    
            $.ajax({
                type: 'GET',
                url: s,
                success: function (data) {
                    document.getElementById("wrapper3").style.display = "none";
                    $('#otherPage').html(data);
                    $('#wrapper2').show();
                }
            });
        }
        socialMediaListDetails();
    </script>    
    
    <%
        }if(socialMediaSetting != null){                   
    %>
<h2 class="font-light m-b-xs">
                Update Social Media Settings 
            </h2>
            <small>View/Update your Social media settings </small>
        </div>
    </div>
</div>
<%}else{%>
<h2 class="font-light m-b-xs">
                Create Social Media Settings 
            </h2>
            <small>Create your Social media settings </small>
        </div>
    </div>
</div>
<%}%>
<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">                                
                    <form id="socialinfo" class="form-horizontal">
                        <h6><i class="fa fa-facebook fa-2x text-info"> Facebook</i></h6><br/>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >App Id </label>
                            <div class="col-sm-4">
                                <%if(socialMediaSetting != null && socialMediaSetting.getFacebookAppId() != null){%>
                                <input type="text" id="fbappid" value="<%=socialMediaSetting.getFacebookAppId()%>" placeholder="Please enter application id" name="fbappid" class="form-control">
                                <%}else{%>
                                <input type="text" id="fbappid" placeholder="Please enter application id" name="fbappid" class="form-control">
                                <%}%>
                                
                            </div>
                            <label class="col-sm-2 control-label">App Key</label>
                            <div class="col-sm-4">
                                <%if(socialMediaSetting != null && socialMediaSetting.getFacebookAppkey() != null){%>
                                <input type="text" id="fbappkey" value="<%=socialMediaSetting.getFacebookAppkey()%>" placeholder="Please enter application key" name="fbappkey" class="form-control">                               
                                <%}else{%>
                                <input type="text" id="fbappkey" placeholder="Please enter application key" name="fbappkey" class="form-control">                               
                                <%}%>
                                
                            </div> 
                            
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Client Secret </label>
                            <div class="col-sm-4">  
                                <%if(socialMediaSetting != null && socialMediaSetting.getFacebookClientsecret() != null){%>
                                <input type="text" id="fbclientsecret" name="fbclientsecret" value="<%=socialMediaSetting.getFacebookClientsecret()%>" placeholder="Please enter facebook client secret" class="form-control">
                                <%}else{%>
                                <input type="text" id="fbclientsecret" name="fbclientsecret" placeholder="Please enter facebook client secret" class="form-control" >
                                <%}%>
                                
                            </div>
                        </div>        

                        <h6><i class="fa fa-twitter fa-2x text-info">  Twitter</i></h6><br/>    
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Handler name</label>
                            <div class="col-sm-4">  
                                <%if(socialMediaSetting != null && socialMediaSetting.getTwitterHandlerName()!= null){%>
                                <input type="text" id="twitterHandlerId" value="<%=socialMediaSetting.getTwitterHandlerName()%>" placeholder="Please enter twitter handler name" name="twitterHandlerId" class="form-control" onblur="checkAvailability()">
                                <%}else{%>
                                <input type="text" id="twitterHandlerId" placeholder="Please enter twitter access token" name="twitterHandlerId" class="form-control" onblur="checkTwitterAvailability()">
                                <%}%>
                                <small id="checkAvailability-result"></small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Access token</label>
                            <div class="col-sm-4">  
                                <%if(socialMediaSetting != null && socialMediaSetting.getTwitterAccessToken() != null){%>
                                <input type="text" id="twitterAppId" value="<%=socialMediaSetting.getTwitterAccessToken()%>" placeholder="Please enter twitter access token" name="twitterAppId" class="form-control">
                                <%}else{%>
                                <input type="text" id="twitterAppId" placeholder="Please enter twitter access token" name="twitterAppId" class="form-control" >
                                <%}%>
                                
                            </div>
                            <label class="col-sm-2 control-label" >Access token secret</label>
                            <div class="col-sm-4"> 
                                <%if(socialMediaSetting != null && socialMediaSetting.getTwitterAccessTokenSecret() != null){%>
                                <input type="text" id="twitterAppkey" value="<%=socialMediaSetting.getTwitterAccessTokenSecret()%>"  name="twitterAppkey" placeholder="Please enter twitter access token secret" class="form-control"> 
                                <%}else{%>
                                <input type="text" id="twitterAppkey"  name="twitterAppkey" placeholder="Please enter twitter access token secret" class="form-control"> 
                                <%}%>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Consumer key </label>
                            <div class="col-sm-4">  
                                <%if(socialMediaSetting != null && socialMediaSetting.getTwitterConsumerKey() != null){%>
                                <input type="text" id="twitterClientSecret" name="twitterClientSecret" value="<%=socialMediaSetting.getTwitterConsumerKey()%>" placeholder="Please enter twitter client secret" class="form-control">
                                <%}else{%>
                                <input type="text" id="twitterClientSecret" name="twitterClientSecret" placeholder="Please enter twitter client secret" class="form-control" >
                                <%}%>                                
                            </div>
                            
                            <label class="col-sm-2 control-label" >Consumer secret </label>
                            <div class="col-sm-4">  
                                <%if(socialMediaSetting != null && socialMediaSetting.getTwitterConsumerSecret() != null){%>
                                <input type="text" id="twitterConsumerSecret" name="twitterConsumerSecret" value="<%=socialMediaSetting.getTwitterConsumerSecret()%>" placeholder="Please enter twitter consumer secret" class="form-control">
                                <%}else{%>
                                <input type="text" id="twitterConsumerSecret" name="twitterConsumerSecret" placeholder="Please enter twitter consumer secret" class="form-control" >
                                <%}%>                                
                            </div>
                        </div>
                        <div class="form-group">                                    
                            
                        </div>
                        <h6><i class="fa fa-instagram fa-2x text-info"> Instagram</i></h6><br/>    
                        <div class="form-group">                                                                        
                            <label class="col-sm-2 control-label">App Id </label>
                            <div class="col-sm-4">                 
                                <%if(socialMediaSetting != null  && socialMediaSetting.getInstagramAppId() != null){%>
                                <input type="text" id="instaAppid" name="instaAppid" value="<%=socialMediaSetting.getInstagramAppId()%>"  placeholder="Please enter instagram App id" class="form-control">
                                <%}else{%>
                                <input type="text" id="instaAppid" name="instaAppid" placeholder="Please enter instagram App id" class="form-control">
                                <%}%>
                                
                            </div>
                            <label class="col-sm-2 control-label" >App key </label>
                            <div class="col-sm-4"> 
                                 <%if(socialMediaSetting != null && socialMediaSetting.getInstagramAppkey() != null){%>
                                 <input type="text" id="instaAppkey" name="instaAppkey" value="<%=socialMediaSetting.getInstagramAppkey()%>" placeholder="Please enter instagram App key" class="form-control">
                                 <%}else{%>
                                 <input type="text" id="instaAppkey" name="instaAppkey" placeholder="Please enter instagram App key" class="form-control">
                                 <%}%>
                                
                            </div>                                
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Client secret </label>
                            <div class="col-sm-4">
                                <%if (socialMediaSetting != null && socialMediaSetting.getInstagramClientsecret() != null) {%>
                                <input type="text" id="instaClientsec" name="instaClientsec" value="<%=socialMediaSetting.getInstagramClientsecret()%>" placeholder="Please enter instagram client secret" class="form-control">
                                <%} else {%>
                                <input type="text" id="instaClientsec" name="instaClientsec" placeholder="Please enter instagram client secret" class="form-control">
                                <%}%>

                            </div>
                        </div>         
                        <%if(socialMediaSetting != null){%>    
                        <a class="btn btn-primary ladda-button validateSocialMediaForm" data-style="zoom-in" onclick="validateSocilaFormV2('<%=socialMediaSetting.getSocialmediaId()%>')" style="display: none">Save changes</a>    
                        <%}else{%>
                        <a class="btn btn-primary ladda-button validateSocialMediaForm" data-style="zoom-in" onclick="validateSocilaForm()" style="display: none">Save changes</a>    
                        <%}%>
<!--                        <button class="btn btn-primary ladda-button validateSocialMediaForm" data-style="zoom-in" onclick="validateSocilaForm()" style="display: none">Save changes</button>    -->
                    </form>
                    <br><br>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-8" style="margin-left: 16%">
                            <a class="btn btn-default" href="./header.jsp">Cancel</a>                            
                            <%if(socialMediaSetting != null){%>
                            <a class="btn btn-info ladda-button" id="partUpdate" data-style="zoom-in"  onclick="callValidateSocialMediaFormV2()">Save changes</a>
                            <%}else{%>
                            <a class="btn btn-info ladda-button" id="partUpdate" data-style="zoom-in"  onclick="callValidateSocialMediaForm()">Save changes</a>
                            <%}%>
<!--                            <button class="btn btn-primary ladda-button validateProfileForm" data-style="zoom-in" id="addsetting"  onclick="saveSocialSetting()" >Save changes</button>    -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
<div class="col-lg-12" style="margin-top: 90px"></div>
    </div>
</div>


