<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.BrandOwnerManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.BrandOwnerManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmBrandownerdetails"%>
<%@page import="org.json.JSONObject"%>
<script src="./scripts/profile.js" type="text/javascript"></script>
<%
    
    RmBrandownerdetails partnerObjEdit = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
    BrandOwnerManagement partnerDetails = new BrandOwnerManagement();
    if (partnerObjEdit==null) {
        response.sendRedirect("logout.jsp");
        return;
    }  
    RmBrandownerdetails partnerData = partnerDetails.getBrandOwnerDetails(partnerObjEdit.getOwnerId());
    request.getSession().setAttribute("_brandownerDetails", partnerData);   
%>
<div class="small-header transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <h2 class="font-light m-b-xs">
                My Profile
            </h2>
            <small>View/Update your information </small>
        </div>
    </div>
</div>


<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">                                
                    <form id="personalInfo" class="form-horizontal">
                        <input type="hidden" id="partnerId" name="partnerId" value="<%=partnerData.getOwnerId()%>">
                        <h6><i class="fa fa-user fa-2x text-info">  Personal Information</i></h6><br/>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Name </label>
                            <div class="col-sm-3">
                                <%if (partnerData != null && partnerData.getBrandName() != null && !partnerData.getBrandName().isEmpty()) {%>
                                <input type="text" id="partnerName"  name="partnerName" value="<%=partnerData.getBrandName()%>" class="form-control" readonly>
                                <%} else {%>
                                <input type="text" id="partnerName"  name="partnerName" class="form-control" readonly>
                                <%}%>
                            </div>
                            <label class="col-sm-2 control-label">Mobile No. </label>
                            <div class="col-sm-3">
                                <%if (partnerData != null && partnerData.getPhone() != null && !partnerData.getPhone().isEmpty() && !partnerData.getPhone().equals("0")) {%>
                                <input type="text" value="<%=partnerData.getPhone()%>"  id="mobileNo" name="mobileNo" class="form-control" readonly>
                                <%} else {%>
                                <input type="text" id="mobileNo" name="mobileNo" class="form-control" readonly>
                                <%}%>
                            </div>                                                                        
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email </label>
                            <div class="col-sm-3">
                                <%if (partnerData != null && partnerData.getEmail() != null && !partnerData.getEmail().isEmpty()) {%>
                                <input type="text" value="<%=partnerData.getEmail()%>"  id="emailId" name="emailId" class="form-control" readonly>
                                <%} else {%>
                                <input type="text" id="emailId" name="emailId" class="form-control" readonly>
                                <%}%>
                            </div>
                        </div>
                        <h6><i class="fa fa-home fa-2x text-info">  Company Information</i></h6><br/>    
                        <div class="form-group">                                    
                            <label class="col-sm-2 control-label" >Company Name <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if (partnerData.getCompanyName() != null) {%>
                                <input type="text" id="cmpname" name="cmpname"  value="<%=partnerData.getCompanyName()%>" class="form-control" required>
                                <%} else {%>
                                <input type="text" id="cmpname" name="cmpname"  class="form-control" required>
                                <%}%>
                            </div>

                            <label class="col-sm-2 control-label" >Registration Number </label>
                            <div class="col-sm-3">
                                <%if (partnerData.getRegistrationNumber() != null) {%>
                                <input type="text" id="cmpreg" name="cmpreg"  value="<%=partnerData.getRegistrationNumber()%>" class="form-control" readonly>
                                <%} else {%>
                                <input type="text" id="cmpreg" name="cmpreg"  class="form-control" readonly>
                                <%}%>
                            </div>
                        </div>
                        <div class="form-group">                                    
                            <label class="col-sm-2 control-label">Landline Number <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if (partnerData.getLandlineNumber() != null) {%>
                                <input type="text" id="landline_number" name="landline_number"  value="<%=partnerData.getLandlineNumber()%>" class="form-control" required  >
                                <%} else {%>
                                <input type="text" id="landline_number" name="landline_number"  class="form-control" required >
                                <%}%>
                            </div>
                            
                            <label class="col-sm-2 control-label" >Address Line 1 <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if (partnerData.getComapanyAddrLine1() != null) {%>
                                <input type="text" id="addr1" name="addr1"  value="<%=partnerData.getComapanyAddrLine1()%>" class="form-control" required >
                                <%} else {%>
                                <input type="text" id="addr1" name="addr1"  class="form-control" required >
                                <%}%>
                            </div>
                            
                        </div>

                        <div class="form-group">
                            
                            <label class="col-sm-2 control-label" >Address Line 2 <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if (partnerData.getComapanyAddrLine2() != null) {%>
                                <input type="text" id="addr2" name="addr2"  value="<%=partnerData.getComapanyAddrLine2()%>" class="form-control" required >
                                <%} else {%>
                                <input type="text" id="addr2" name="addr2"  class="form-control" required >
                                <%}%>
                            </div>
                            
                            <label class="col-sm-2 control-label" >Address Line 3 <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if (partnerData.getComapanyAddrLine3() != null) {%>
                                <input type="text" id="addr3" name="addr3" value="<%=partnerData.getComapanyAddrLine3()%>" class="form-control" required >
                                <%} else {%>
                                <input type="text" id="addr3" name="addr3" class="form-control" required >
                                <%}%>
                            </div>
                                                                                                               
                        </div>

                        <div class="form-group">                                                                        
                            <label class="col-sm-2 control-label"> City <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if (partnerData.getCity() != null) {%>
                                <input type="text" id="city" name="city" value="<%=partnerData.getCity()%>" class="form-control" required >
                                <%} else {%>
                                <input type="text" id="city" name="city"  class="form-control">
                                <%}%>
                            </div> 
                            
                            <label class="col-sm-2 control-label" > State <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if (partnerData.getCompanyState() != null) {%>
                                <input type="text" id="cmpstate" name="cmpstate"  value="<%=partnerData.getCompanyState()%>" class="form-control" required >
                                <%} else {%>
                                <input type="text" id="cmpstate" name="cmpstate" class="form-control" required >
                                <%}%>
                            </div>                            
                                                         
                        </div>                                
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Country <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if (partnerData.getCountry() != null) {%>
                                <input type="text" id="country" name="country"  value="<%=partnerData.getCountry()%>" class="form-control" required  >
                                <%} else {%>
                                <input type="text" id="country" name="country"  class="form-control" required >
                                <%}%>
                            </div>                                     
                            <label class="col-sm-2 control-label" >Pin Code <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if (partnerData.getPincode() != null) {%>
                                <input type="text" id="pinCode"  value="<%=partnerData.getPincode()%>" name="pinCode" class="form-control" required >
                                <%} else {%>
                                <input type="text" id="pinCode"  name="pinCode" class="form-control" required >
                                <%}%>
                            </div>
                        </div>
                        <button class="btn btn-primary ladda-button validateOwnerForm" data-style="zoom-in" onclick="validateOwnerDetailsForm('<%=partnerData.getOwnerId()%>')" style="display: none">Save changes</button>    
                    </form>

                    <h6><i class="fa fa-image fa-2x text-info"> Profile Picture</h6></i> <br/>
                    <div style="background: #fff; height: 200px">
                        <div class="col-sm-8 col-lg-8 col-md-8" style="width: 68%;margin-left: 16%">
                            <form action="UploadEmailAttachme" method="post" class="dropzone" id="my-dropzone" name="my-dropzone"></form>	
                            <small>Upload Profile pic. Support Image File size limit 1mb</small>
                        </div>
                    </div>
                    <br><br>            
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-8" style="margin-left: 16%">
                            <a class="btn btn-default" href="./header.jsp">Cancel</a>                            
                            <a class="btn btn-info ladda-button" id="partUpdate" data-style="zoom-in"  onclick="callValidateOwnerDetailsForm('<%=partnerData.getOwnerId()%>')">Save changes</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>                             
    </div>
</div>
<script>
    $(document).ready(function () {
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("#my-dropzone", {
            url: "UploadProfilePic",
            uploadMultiple: false,
            maxFilesize: 1,
            maxFiles: 1,
            acceptedFiles: "image/*",
            dictInvalidFileType: "You can't upload files of this type, only Image file",
            autoProcessQueue: true,
            parallelUploads: 1,
            addRemoveLinks: true,
            dictDefaultMessage: "Drop profile pic here to upload",
            removedfile: function (file) {
                removeImageFromSession(file.name);
                var _ref;
                if (file.previewElement) {
                    if ((_ref = file.previewElement) != null) {
                        _ref.parentNode.removeChild(file.previewElement);
                    }
                }
                return this._updateMaxFilesReachedClass();
            }
        });
    });
</script>

