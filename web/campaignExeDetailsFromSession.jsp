<%@page import="com.mollatech.rewardme.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.CampaignManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmCampaigndetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.CampaignExeNameManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmCampaignExecutionName"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmBrandownerdetails"%>
<%@page import="java.text.SimpleDateFormat"%>
<script src="scripts/billingReport.js" type="text/javascript"></script>
<%
    RmBrandownerdetails parObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails"); 
    String SessionId = (String) request.getSession().getAttribute("_brandownerSessionId");     
    if (SessionId == null || parObj==null) {
        response.sendRedirect("logout.jsp");
        return;
    }  
    String startDate = (String)session.getAttribute("startDateRequest");
    String endDate = (String)session.getAttribute("endDateRequest");
    String stime = (String)session.getAttribute("stimeRequest");
    String etime = (String)session.getAttribute("etimeRequest");
    String campaignDetails = (String)session.getAttribute("campaignDetailsRequest");
    SimpleDateFormat tmDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
    String[] listCampaignDetails = null;
    if (campaignDetails != null && !campaignDetails.equalsIgnoreCase("null")) {
        listCampaignDetails = campaignDetails.split(",");
    }
    RmCampaignExecutionName[] campaignExeTracking = (RmCampaignExecutionName[]) session.getAttribute("campaignExeNameDetails");
    int count = 0;
    
%>
<div class="col-lg-12 col-md-12 col-sm-12">
    <form class="form-horizontal " method="get">       
        <div class="dataTable_wrapper table-responsive">
            <table class="table table-striped table-bordered table-hover dataTables-example">
                <thead id="apiUsageHeader">
                    <tr>
                        <th style="text-align: center">Sr.No.</th>                        
                        <th style="text-align: center">Campaign</th>                        
                        <th style="text-align: center">Campaign Execution Name</th> 
                        <th style="text-align: center">Status</th> 
                        <th style="text-align: center">Date</th>
                        <th style="text-align: center">View User Find</th>
                    </tr>
                </thead>
                <tbody>
                    <%  
                        if (campaignExeTracking != null) {
                            for (RmCampaignExecutionName transcationdetailse : campaignExeTracking) {
                                try {
                                        String campName = "NA"; String campaignStatus = "NA";                                        
                                        RmCampaigndetails campaignObject = new CampaignManagement().getCampaignByIdDetails(transcationdetailse.getCampaignId());
                                        if(campaignObject != null){
                                            campName = campaignObject.getRewardTitle();
                                        }
                                        if(campaignDetails != null && !campaignDetails.equalsIgnoreCase("null")){
                                            if(!campaignDetails.contains(campName))
                                            continue;
                                        }
                                        if(transcationdetailse != null){
                                            if(transcationdetailse.getStatus() == GlobalStatus.START_PROCESS){
                                                campaignStatus = "Running";
                                            }
                                            if(transcationdetailse.getStatus() == GlobalStatus.ACTIVE){
                                                campaignStatus = "Stop";
                                            }
                                        }
                                        count ++;                                                                     
                    %>
                    <tr>
                        <td ><font><%=count%></font></td>                        
                        <td ><font ><%=campName%></font></td>                         
                        <td ><font><%=transcationdetailse.getCampaignExeName()%> </font></td>
                        <td ><font><%=campaignStatus%> </font></td>
                        <td ><font><%=tmDateFormat.format(transcationdetailse.getCreationDate())%></font></td>
                        <td><a class="btn btn-success btn-xs ladda-button"  data-style="zoom-in" id="generateButton<%=count%>" onclick="generatereCampaignFindUser('<%=startDate%>', '<%=endDate%>','<%=transcationdetailse.getCampaignExeName()%>','<%=transcationdetailse.getCampaignId()%>','<%=count%>')"> View </a></td>
                    </tr>
                    <%                        
                                }catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }                        
                        }
                        if (count < 1){                        
                    %>
                        <img src="images/no_record_found.png" alt="No record found" width="400px" height="300px"/>
                        <script>
                            document.getElementById("apiUsageHeader").style.display = 'none';
                        </script>
                    <%}%>                                        
                </tbody>
            </table>                
        </div>
        
<!--         <%if (count != 0) {%>
<!--        <a  style="float: left"class="btn btn-success btn-xs"  onclick="downloadBillingPDF('<%=startDate%>', '<%=endDate%>', '<%=stime%>', '<%=etime%>', '<%=campaignDetails%>')">Download PDF <i class="fa fa-file-pdf-o"></i> </a>-->
        <%
            if(listCampaignDetails != null && listCampaignDetails.length > 1){
        %>    
        <p style="float:left"> You run the campaigns, <%=count%> times</p>
        <%
            }else if(listCampaignDetails != null && listCampaignDetails.length == 1){
        %>    
            <p style="float:left"> You run the campaign, <%=count%> times</p>
        <%}else{%>
            <p style="float:left"> You run the campaigns, <%=count%> times</p>
        <%}}%>
        
    </form>
</div>
<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });

</script>

