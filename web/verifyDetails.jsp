<!DOCTYPE html>
<html>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.bouncycastle.util.encoders.Base64"%>    
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>Reward Me | Reset Password</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
   <link rel="shortcut icon" type="images/front-logo.png" href="images/front-logo.png" />

    <!-- Vendor styles -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
     <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
    <link rel="stylesheet" href="vendor/ladda/dist/ladda-themeless.min.css" />
    <link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
    <script src="scripts/utilityFunction.js" type="text/javascript"></script>
    <script src="scripts/partnerRequest.js" type="text/javascript"></script>

    <!-- App styles -->
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="styles/style.css">

</head>
<%
    String username = request.getParameter("username");
    username = new String(Base64.decode(username));
    String email = request.getParameter("email");
    String expiryTime = request.getParameter("expiryTime");
    expiryTime = new String(Base64.decode(expiryTime));
    String userId = request.getParameter("ref");
    userId  = new String(Base64.decode(userId));
    SimpleDateFormat formatter = new SimpleDateFormat("EEEE, MMM dd, yyyy HH:mm:ss a");
    Date expiry = formatter.parse(expiryTime);
    if (expiry.before(new Date())) {
        response.sendRedirect("linkExpire.jsp");
        return;
    }
%>
<body class="blank" style="background: rgba(43,48,131,1);
background: -moz-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
background: -webkit-gradient(left top, right top, color-stop(0%, rgba(43,48,131,1)), color-stop(50%, rgba(35,129,196,1)), color-stop(100%, rgba(43,48,131,1)));
background: -webkit-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
background: -o-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
background: -ms-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
background: linear-gradient(to right, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2b3083', endColorstr='#2b3083', GradientType=1 );">

<!-- Simple splash screen-->
<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Loading...</h1><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="color-line"></div>

<div class="login-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <h3 style="color:#fff">Reset Password</h3>
                <small></small>
            </div>
            <div class="hpanel">
                <div class="panel-body">
                    <form id="forgetPasswordForm">
                         <div class="form-group">
                            <label class="control-label" for="email">Username</label>
                            <input type="text" placeholder="User name" title="Please enter you user name" required="" value="<%=username%>" name="fpUsername" id="fpUsername" class="form-control" disabled>
                            
                        </div>
                         <div class="form-group">
                            <label class="control-label" for="email">Email Id</label>
                            <input type="text" placeholder="abc@noemail.com" title="Please enter you email id" required="" value="<%=email%>" name="fpEmailID" id="fpEmailID" class="form-control" disabled>
                            
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="email">New Password</label>
                            <input type="password" placeholder="New Password"  value="" name="newPassword" id="newPassword" class="form-control">
                            
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="email">Confirm </label>
                            <input type="password" placeholder="Confirm your new password" value="" name="confirmPassword" id="confirmPassword" class="form-control">
                           
                        </div>
                        <button class="btn btn-success ladda-button btn-block" data-style="zoom-in" id="forgetPasswordButton" onclick="forgetPassword('<%=userId%>')"> Reset</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center" style="color:#fff">
             <strong>Powered by Axiom Protect 2.0</strong>
             <%
                    java.util.Date dFooter = new java.util.Date();
                    SimpleDateFormat sdfFooter = new SimpleDateFormat("yyyy");
                    String strYYYY = sdfFooter.format(dFooter);
                    SimpleDateFormat tz = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
                    String completeTimewithLocalTZ = tz.format(dFooter);
                    long LongTime = dFooter.getTime() / 1000;

                %>
            <div align="center"  class="col-md-12 text-center" style="color:#fff">
                <p>&copy; BlueBricks Technologies 2009-<%=strYYYY%> (<a href="http://www.blue-bricks.com" target="_blank">www.blue-bricks.com</a>)</p>
            </div>
        </div>
    </div>
</div>


<!-- Vendor scripts -->
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="vendor/iCheck/icheck.min.js"></script>
<script src="vendor/sparkline/index.js"></script>
<script src="vendor/ladda/dist/spin.min.js"></script>
<script src="vendor/ladda/dist/ladda.min.js"></script>
<script src="vendor/ladda/dist/ladda.jquery.min.js"></script>
<script src="vendor/jquery-validation/jquery.validate.min.js"></script>
<script src="vendor/sweetalert/lib/sweet-alert.min.js"></script>
<script src="vendor/toastr/build/toastr.min.js"></script>
<!-- App scripts -->
<script src="scripts/homer.js"></script>

</body>
</html>