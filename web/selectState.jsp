
<%@page import="com.mollatech.rewardme.nucleus.db.RmStates"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.AddressManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    String stateId = request.getParameter("stateId");
    System.out.println("stateId >> "+stateId);
%>    
    <div class="control-group">
        <label class="control-label"  for="username"></label>
        <div class="controls">
            <select id="_state12" name="_state12"  class="form-control span2" onchange="showcities(this.value)" style="width: 100%">
                <option value="-1" id="-1">Select State</option>
                <%
                    String SessionId = (String) request.getSession().getAttribute("_brandownerSessionId");
                    String _countryID = request.getParameter("_countryID");
                    int countryid = Integer.parseInt(_countryID);
                    if (!_countryID.equals("-1")) {                        
                        AddressManagement addObj = new AddressManagement();
                        RmStates[] stateObj = addObj.getAllstateByCountryid(SessionId, countryid);
                        if(stateObj != null){           
                            for(int i = 0; i < stateObj.length; i++){  
                                if(stateId != null && !stateId.equalsIgnoreCase("null") && stateId.equalsIgnoreCase(String.valueOf(stateObj[i].getId()))){
                %>
                <option value="<%=stateObj[i].getId()%>" id="<%=stateObj[i].getId()%>" selected><%=stateObj[i].getName()%></option>               
                <%
                    }else{
                %>
                <option value="<%=stateObj[i].getId()%>" id="<%=stateObj[i].getId()%>"><%=stateObj[i].getName()%></option>     
                <%    }
                      }
                        }} else {%>
                <option value="-1">Select State</option>  
                <% }%>
                </select>           
        </div>
        
    </div>
                