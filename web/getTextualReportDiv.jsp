
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.CampaignManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmCampaigndetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmBrandownerdetails"%>
<script src="scripts/billingReport.js" type="text/javascript"></script>
<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel" style="height: 50%">

                <div class="panel-body" style="text-align: center;">
                    <form id="api_usageV2" class="form-horizontal">
                        <div class="col-md-12">
                            <div style="display: none">
                                <select class="form-control m-b" name="apiUsageEnvironment" id="apiUsageEnvironment">
                                    <!--<option value="0">All</option>-->
                                    <option value="2">Production</option>
                                </select>
                            </div>						
                            <div class="col-sm-3">
                                <select class="js-source-states-2" id="campaignDetails" name="campaignDetails" multiple="multiple" style="width: 100%">                                                        
                                    <%                                        
                                        RmBrandownerdetails parObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");                                       
                                        RmCampaigndetails[] campaignDetail = 
                                        campaignDetail = new CampaignManagement().getCampaignByOwnerId(parObj.getOwnerId());
                                        if (campaignDetail != null) {
                                            for (int i = 0; i < campaignDetail.length; i++) {                                                                                                   
                                    %>
                                    <option value="<%=campaignDetail[i].getRewardTitle()%>" selected><%=campaignDetail[i].getRewardTitle()%></option>                                                                        
                                    <%                                                                                                            
                                                }
                                            }                                        
                                    %>
                                </select>
                            </div>
                            <div class="col-sm-3"><input id="_startdate" name="_startdate" type="text" class="form-control" placeholder="from"></div>
                            <div class="col-sm-3"><input id="_enddate" name="_enddate" type="text" class="form-control" placeholder="to"></div>
                            <div class="col-sm-3">
                                <a class="btn btn-success ladda-button btn-sm btn-block" data-style="zoom-in" id="generateButton" onclick="generatereAPIreport()" type="submit"><i class="fa fa-bar-chart"></i> Generate</a> 
                            </div>
                        </div>
                        <div class="col-lg-12" id="fillBlankSpace" style="margin-bottom: 33%">
                        </div>
                    </form>
                    <hr class="m-b-xl"/>
                    </br>
                    <div id="report_data"></div>
                </div>
            </div>
        </div>
       <div class="col-md-12" style="margin-bottom: 22%"></div>
    </div>
</div>
<script>
    $(function () {
        $('#_startdate').datepicker();
        $('#_enddate').datepicker();        
        $("#_enddate").datepicker('setDate', new Date().getDay()+1);
        $("#_startdate").datepicker('setDate', new Date().getDay() - 10);
        $(".js-source-states-2").select2();
        //$('#datapicker1').datepicker();
//        $('#datapicker2').datepicker();
//       $('#_startdate').datepicker({
//            "setDate": new Date().getDay()-15,
//            "autoclose": true
//        });
//        $('#_enddate').datepicker({
//            "setDate": new Date(),
//            "autoclose": true
//        });
//        $("#datapicker2").datepicker('setDate', new Date());
//        $("#datapicker1").datepicker('setDate', new Date().getDate() - 10);

    });
    generatereAPIreport();
</script>
