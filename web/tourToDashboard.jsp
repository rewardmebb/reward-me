

<%@page import="com.mollatech.rewardme.nucleus.db.RmSocialmediadetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.SocialMediaManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmBrandownerdetails"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.rewardme.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<!DOCTYPE html>

<html>
    <%response.addHeader("X-FRAME-OPTIONS", "DENY");%>    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendor/c3/c3.min.css" />
        <script src="vendor/c3/c3.min.js"></script>
        <!--                 Page title -->
        <title>Reward Me</title>        
    </head>

    <body class="fixed-navbar">
        <style>
            .flot-chart-pie-content {
                width: 200px;
                height: 200px;
                margin: auto;
            }
            .flot-pie-chart {
                display: block;
                padding-top: 50px;
                height: 300px;
            }

            <%
                String mailId = LoadSettings.g_sSettings.getProperty("support.email.id");
            %>
            span.capitalize {
                text-transform: capitalize;
            }
        </style>    
        <!--Home page-->
        <div id="wrapper1">
            <div id="wrapper">
                <div id="homePage">
                    <div class="content animate-panel">
                        <div class="row">
                            <div class="col-lg-3 tour-79">
                                <div class="hpanel stats">
                                    <div class="panel-heading">

                                        Today's total credit used
                                    </div>
                                    <div class="panel-body h-200">
                                        <div class="stats-icon text-center ">
                                            <i class="pe-7s-share fa-4x "></i>
                                        </div>
                                        <div class="m-t-xl">
                                            <div class="progress m-t-xs full progress-small">
                                                <div id="percentage" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" role="progressbar" class=" progress-bar progress-bar-info">
                                                    <span class="sr-only">100%</span>
                                                </div>
                                            </div> 
                                            <div class="row" style="height: 60px">
                                                <div class="col-xs-6">
                                                    <small class="stats-label">Credit used</small>
                                                    <h4  id='creditUsed'>10</h4>
                                                </div>
                                                <div class="col-xs-6">
                                                    <small class="stats-label">% Credit used</small>
                                                    <h4 id='perCreditUsed'>0.57%</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 tour-80">
                                <div class="hpanel">
                                    <div class="panel-heading">
                                        Last week total credit used
                                    </div>
                                    <div class="panel-body text-center h-200">
                                        <i class="pe-7s-graph1 fa-4x"></i>

                                        <h1 class="m-b-xs text-info" id="lastWeekAmount">100</h1>
                                        <small id="weekMsg">You have used total 100 credits in last week for all campaign</small>
                                        <h3 class="font-bold no-margins lead text-info row">
                                            Weekly Usage
                                        </h3> <!--<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit</small>-->
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 tour-81">
                                <div class="hpanel">
                                    <div class="panel-heading">

                                        Last month total credit used
                                    </div>
                                    <div class="panel-body text-center h-200">
                                        <i class="pe-7s-global fa-4x"></i>
                                        <h1 class="m-b-xs text-info" id="monthCredit">300</h1>

                                        <small id="monthMsg">You have used total 300 credits in last month for all services</small>
                                        <h3 class="font-bold no-margins lead text-info row">
                                            Monthly Usage
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-lg-3 col-sm-3 tour-82" id="topmostServices" data-child="hpanel" data-effect="fadeInLeft">
                                <div class="hpanel">
                                    <div class="panel-heading">
                                        <span id="mostlyFive">Your mostly used campaign</span>
                                    </div>
                                    <div class="panel-body" style="height: 213px">
                                        <div class="flot-chart text-center" style="width: 200;height:250">
                                            <div class="flot-chart-content"  id="flotpiechart"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 tour-83" data-child="hpanel" data-effect="fadeInLeft">
                                <div class="hpanel">
                                    <div class="panel-heading">
                                        Campaign Information And Statistics
                                    </div>
                                    <div class="panel-body">
                                        <div class="text-left">
                                            <i class="fa fa-bar-chart-o fa-fw"></i><span id="homeReportLable">  Today's Expense</span>
                                        </div>
                                        <div class="btn-group" style="float: right;">
<!--                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="generateTopServiceV2()">Top Five</button>-->
                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="creditperSer()">Today's Expense</button>
                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="toptrndingServices()">Today's Trending</button>
                                        </div>
                                        <div style="clear: both"></div>
                                        <div  id="topFiveSerTour" style="height: 215px" class="c3"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
                <%@include file="footer.jsp"%>
            </div>
        </div>
        <!--End home page-->         
        <div class="content animate-panel" >
            <div class="row">
                <div class="col-lg-12  tour-55">
                    <div class="hpanel">
                        <div class="panel-body">                                
                            <form id="socialinfo" class="form-horizontal">
                                <h6><i class="fa fa-facebook fa-2x text-info"> Facebook</i></h6><br/>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" >App Id </label>
                                    <div class="col-sm-4">
                                        <input type="text" id="fbappid" placeholder="Please enter application id" name="fbappid" class="form-control">

                                    </div>
                                    <label class="col-sm-2 control-label">App Key</label>
                                    <div class="col-sm-4">
                                        <input type="text" id="fbappkey" placeholder="Please enter application key" name="fbappkey" class="form-control">                                                               
                                    </div>                             
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" >Client Secret </label>
                                    <div class="col-sm-4">
                                        <input type="text" id="fbclientsecret" name="fbclientsecret" placeholder="Please enter facebook client secret" class="form-control" >

                                    </div>
                                </div>        

                                <h6><i class="fa fa-twitter fa-2x text-info">  Twitter</i></h6><br/>    

                                <div class="form-group">
                                    <label class="col-sm-2 control-label" >Access token</label>
                                    <div class="col-sm-4">  

                                        <input type="text" id="twitterAppId" placeholder="Please enter twitter access token" name="twitterAppId" class="form-control" >


                                    </div>
                                    <label class="col-sm-2 control-label" >Access token secret</label>
                                    <div class="col-sm-4"> 

                                        <input type="text" id="twitterAppkey"  name="twitterAppkey" placeholder="Please enter twitter access token secret" class="form-control"> 


                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" >Consumer key </label>
                                    <div class="col-sm-4">  

                                        <input type="text" id="twitterClientSecret" name="twitterClientSecret" placeholder="Please enter twitter client secret" class="form-control" >

                                    </div>

                                    <label class="col-sm-2 control-label" >Consumer secret </label>
                                    <div class="col-sm-4">  
                                        <input type="text" id="twitterConsumerSecret" name="twitterConsumerSecret" placeholder="Please enter twitter consumer secret" class="form-control" >

                                    </div>
                                </div>
                                <div class="form-group">                                    

                                </div>
                                <h6><i class="fa fa-instagram fa-2x text-info"> Instagram</i></h6><br/>    
                                <div class="form-group">                                                                        
                                    <label class="col-sm-2 control-label">App Id </label>
                                    <div class="col-sm-4">                 
                                        <input type="text" id="instaAppid" name="instaAppid" placeholder="Please enter instagram App id" class="form-control">
                                    </div>
                                    <label class="col-sm-2 control-label" >App key </label>
                                    <div class="col-sm-4"> 
                                        <input type="text" id="instaAppkey" name="instaAppkey" placeholder="Please enter instagram App key" class="form-control">

                                    </div>                                
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" >Client secret </label>
                                    <div class="col-sm-4">

                                        <input type="text" id="instaClientsec" name="instaClientsec" placeholder="Please enter instagram client secret" class="form-control">


                                    </div>
                                </div>         
                                <a class="btn btn-primary ladda-button validateSocialMediaForm" data-style="zoom-in" style="display: none">Save changes</a>    
                            </form>
                            <br><br>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-8" style="margin-left: 16%">
                                    <a class="btn btn-default">Cancel</a>                            
                                    <a class="btn btn-info ladda-button" id="partUpdate" data-style="zoom-in" >Save changes</a>                            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12" style="margin-top: 90px"></div>
            </div>
        </div>

        <div id="wrapper">
            <div class="col-lg-6">
                <div class="modal fade hmodal-success" id="todayExpenditure" tabindex="-1" role="dialog"  aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="color-line"></div>
                            <div class="modal-header" style="padding: 10px 0px !important">
                                <h4 id="todayCreditForAPILable" style="margin-left: 2%">Today Credit Used
                                    <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
                            </div>
                            <div class="modal-body" style="padding-bottom: 50px !important;text-align: center;">                                
                                <div id="todayCreditForAPI">                        
                                </div>
                                <div id="noRecordFoundData" style="display: none;text-align: center;" style="margin-bottom: 30%">
                                    <img src="images/no_record_found.png" alt="No record found" width="300px" height="200px"/>
                                </div> 
                            </div>                                
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="modal fade hmodal-success" id="todayPerformance" tabindex="-1" role="dialog"  aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="color-line"></div>
                            <div class="modal-header" style="padding: 10px 0px !important">
                                <h4 id="todayPerformanceForAPILable" style="margin-left: 2%">Today Performance
                                    <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
                            </div>
                            <div class="modal-body" style="padding-bottom: 50px !important">                                
                                <div id="todayPerformanceForAPI">                        
                                </div>
                                <div id="noRecordFoundDataPerforamce" style="display: none; text-align: center" style="margin-bottom: 30%">
                                    <img src="images/no_record_found.png" alt="No record found" width="300px" height="200px"/>
                                </div>                         
                            </div>                                
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="modal fade hmodal-success" id="monthlyPerformance" tabindex="-1" role="dialog"  aria-hidden="true">
                    <div class="modal-dialog modal-lg" >
                        <div class="modal-content" >
                            <div class="color-line"></div>
                            <div class="modal-header" style="padding: 10px 0px !important">
                                <h4  style="margin-left: 2%"> Monthly Performance <font id="monthlyPerformanceForAPILable"></font>
                                    <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
                            </div>
                            <div class="modal-body">
                                <div style="margin-bottom: 20px;">
                                    <input type="hidden" id="adIdForMothlyPerformance" name="adIdForMothlyPerformance">
                                    <input type="hidden" id="adTypeForMothlyPerformance" name="adTypeForMothlyPerformance">
                                    <div class="col-sm-3">
                                        <select class="form-control" id="_apiCallMonth" name="_apiCallMonth" onchange="generatePDFADPerformanceByMonth()">                                                                        
                                            <%
                                                SimpleDateFormat format = new SimpleDateFormat("MMM");
                                                Calendar cal = Calendar.getInstance();
                                                cal.setTime(new Date());
                                                cal.add(Calendar.MONTH, -6);
                                                Calendar updatedCal = Calendar.getInstance();
                                                Date sixMonthPerformaceBack = cal.getTime();
                                                updatedCal.setTime(sixMonthPerformaceBack);
                                                for (int month = 1; month <= 6; month++) {
                                                    int calMonth = cal.get(Calendar.MONTH) + month;
                                                    updatedCal.set(Calendar.MONTH, calMonth);
                                                    Date upDate = updatedCal.getTime();
                                                    String newMonth = format.format(upDate);
                                                    if (newMonth.equalsIgnoreCase("Jan")) {
                                                        calMonth = 0;
                                                    }
                                            %>
                                            <%if (month == 6) {%>
                                            <option value="<%=calMonth + 1%>" selected><%=newMonth%></option> 
                                            <%} else {%>
                                            <option value="<%=calMonth + 1%>"><%=newMonth%></option> 
                                            <%
                                                    }
                                                }
                                            %>

                                        </select>
                                    </div>

                                    <!--<div class="col-sm-3"><button id="generatePerformanceByMonth" class="btn btn-success btn-sm ladda-button btn-block" data-style="zoom-in" onclick="generatePerformanceByMonth()"><i class="fa fa-bar-chart"></i> Generate</button></div>-->
                                </div>
                                <br/>
                                <div id="monthlyPerformanceForAPI">                        
                                </div>
                                <div id="noRecordFoundDataPerformance" style="display: none;text-align: center" style="margin-bottom: 27%">
                                    <img src="images/no_record_found.png" alt="No record found" width="300px" height="200px"/>
                                </div> 
                            </div>                                
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="modal fade hmodal-success" id="monthlyCredit" tabindex="-1" role="dialog"  aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="color-line"></div>
                            <div class="modal-header" style="padding: 10px 0px !important">
                                <h4 style="margin-left: 2%"> Monthly Credit Used<font id="monthlyCreditForAPILable" ></font>
                                    <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
                            </div>
                            <div class="modal-body" style="padding-bottom: 50px !important">                                
                                <div style="margin-bottom: 20px;" class="col-centered">
                                    <input type="hidden" id="advertiserAdIdForMothlyCredit" name="advertiserAdIdForMothlyCredit">
                                    <input type="hidden" id="adType" name="adType">
                                    <div class="col-sm-3">
                                        <select class="form-control" id="_apiCallMonthMonthlyCredit" name="_apiCallMonthMonthlyCredit" onchange="generateAdvertiserCreditByMonth()">                                                                        

                                            <%
                                                cal.setTime(new Date());
                                                cal.add(Calendar.MONTH, -6);
                                                Date sixMonthBack = cal.getTime();
                                                updatedCal.setTime(sixMonthBack);
                                                for (int month = 1; month <= 6; month++) {
                                                    int calMonth = cal.get(Calendar.MONTH) + month;
                                                    updatedCal.set(Calendar.MONTH, calMonth);

                                                    Date upDate = updatedCal.getTime();
                                                    String newMonth = format.format(upDate);
                                                    if (newMonth.equalsIgnoreCase("Jan")) {
                                                        calMonth = 0;
                                                    }
                                            %>
                                            <%if (month == 6) {%>
                                            <option value="<%=calMonth + 1%>" selected><%=newMonth%></option> 
                                            <%} else {%>
                                            <option value="<%=calMonth + 1%>"><%=newMonth%></option> 
                                            <%
                                                    }
                                                }
                                            %>
                                        </select>
                                    </div>
                                </div>
                                <br><br>
                                <div id="monthlyCreditForAPI">                        
                                </div>
                                <div id="noRecordFoundDataMonthlyCredit" style="display: none ;text-align: center;" style="margin-bottom: 30%">
                                    <img src="images/no_record_found.png" alt="No record found" width="300px" height="200px"/>
                                </div> 


                            </div>                                
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        <div class="content tour-56">
            <div class="row">
                <div class="col-md-12 tour-13">
                    <div class="hpanel">
                        <div class="panel-body">
                            <input type="text" class="form-control input-sm m-b-md" id="filter" placeholder="Search in table">                                        
                            <div class="table-responsive">
                                <table id="api" class="footable table table-stripped table-responsive" valign="middle" data-page-size="25" data-filter=#filter>    
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th style="text-align: center" class="tour-15">Social Media</th>
                                            <th style="text-align: center" class="tour-15">Access Token</th>
                                            <th style="text-align: center" class="tour-15">Access Token Secret</th>                                                                                                            
                                            <th data-hide="phone,tablet" style="text-align: center">Consumer Key</th>                                    
                                            <th data-hide="phone,tablet" style="text-align: center">Consumer Secret</th>                                    
                                            <th style="text-align: center" class="tour-17">Update</th>
                                        </tr>
                                    </thead>
                                    <tbody>                               
                                        <tr>                                                                                                        
                                            <td>

                                            </td>
                                            <td>
                                                <p>Twitter</p>
                                            </td>
                                            <td>
                                                <p>Your Twitter Access Token</p>
                                            </td>
                                            <td>
                                                <p>Your Twitter Access Token Secret</p>
                                            </td>
                                            <td>
                                                <p>Your Twitter Consumer Key</p>
                                            </td>
                                            <td>
                                                <p>Your Twitter Consumer Key Secret</p>
                                            </td>
                                            <td style="" class="tour-iconAPIConsoletwit" align="center" valign="middle">   
                                                <a onclick = "#" class="text-warning" data-toggle="tooltip" data-placement="right" title="Update Campaign" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                            </td>
                                        </tr>  
                                        <tr>      
                                            <td>

                                            </td>
                                            <td>
                                                <p>Facebook</p>
                                            </td>
                                            <td>
                                                <p>Your Facebook Access Token</p>
                                            </td>
                                            <td>
                                                <p>Your Facebook Access Key</p>
                                            </td>
                                            <td>
                                                <p>Your Facebook Client Token</p>
                                            </td>
                                            <td>
                                                <p>Not Needed</p>
                                            </td>
                                            <td style="" class="tour-iconAPIConsoleface" align="center" valign="middle">   
                                                <a onclick = "#" class="text-warning" data-toggle="tooltip" data-placement="right" title="Update Campaign" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                            </td>
                                        </tr>  
                                        <tr>      
                                            <td>

                                            </td>
                                            <td>
                                                <p>Instagram</p>
                                            </td>
                                            <td>
                                                <p>Your Instagram Access Token</p>
                                            </td>
                                            <td>
                                                <p>Your Instagram Access Key</p>
                                            </td>
                                            <td>
                                                <p>Your Instagram Client Token</p>
                                            </td>
                                            <td>
                                                <p>Not Needed</p>
                                            </td>
                                            <td style="" class="tour-63" align="center" valign="middle">   
                                                <a onclick = "#" class="text-warning" data-toggle="tooltip" data-placement="right" title="Update Campaign" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                            </td>

                                        </tr>  

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="10">
                                                <ul class="pagination pull-right"></ul>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>                        
                            </div>                   
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-bottom: 28%"></div>
            </div>
        </div>                                              
        <div id="wrapper" style="min-height:0%!important">
            <div class="content animate-panel">
                <div class="row tour-invoiceTour">
                    <div class="col-lg-12">
                        <div class="hpanel">
                            <div class="panel-heading">                                
                                Invoices
                            </div>
                            <div class="panel-body table-responsive">
                                <table id="apiInvoiceTable" class="table table-striped table-bordered table-hover">
                                    <thead id="invoiceHeader">
                                        <tr>
                                            <th>Sr.No.</th>
                                            <th>Package Name</th>
                                            <th>Date</th>
                                            <th>Time</th>
                                            <th>Invoice Number</th>
                                            <th>Amount</th>
                                            <th>Status</th>
                                            <th>Download</th>   
                                        </tr>
                                    </thead>
                                    <tbody>                                
                                        <tr>
                                            <td>1</td>                                    
                                            <td>Basic</td> 
                                            <td>12/09/2017</td>
                                            <td>05:02 PM</td>                                            
                                            <td>17220893124</td>
                                            <td>AUD 60.00</td>                                    
                                            <td>Paid</td>  
                                            <td><a class=""><i class="fa fa-file-pdf-o fa-2x"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>2</td>                                    
                                            <td>Basic</td> 
                                            <td>12/10/2017</td>
                                            <td>05:02 PM</td>                                            
                                            <td>1722089675</td>
                                            <td>AUD 60.00</td>                                    
                                            <td>Paid</td>  
                                            <td><a class=""><i class="fa fa-file-pdf-o fa-2x"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>3</td>                                    
                                            <td>Basic</td> 
                                            <td>12/11/2017</td>
                                            <td>05:02 PM</td>                                            
                                            <td>1722087652</td>
                                            <td>AUD 60.00</td>                                    
                                            <td>Paid</td>  
                                            <td><a class=""><i class="fa fa-file-pdf-o fa-2x"></i></a></td>
                                        </tr>

                                    </tbody>
                                </table>
                                <script>
                                    $(function () {
                                        $('#apiInvoiceTable').dataTable();
                                    });
                                </script> 
                            </div>
                        </div>
                    </div>                           
                </div>        
            </div>
        </div>        
        
        <!--///////////Create campaign -->
        <div class="content animate-panel tour-60">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">                                
                    <form id="personalInfo" class="form-horizontal">
                        <input type="hidden" id="partnerId" name="partnerId" value="">
                        <h6><i class="fa fa-user fa-2x text-info">  Campaign Information</i></h6><br/>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Title <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <input type="text" id="campaignTitle" title="Please enter campaign title" name="campaignTitle" class="form-control" required>
                            </div>
                            <label class="col-sm-2 control-label">Coupon Code. <span style="color: red">*</span></label>
                            <div class="col-sm-3">                               
                                <input type="text" id="couponCode" name="couponCode" class="form-control" required>                               
                            </div>                                                                        
                        </div>                        
                        <div class="form-group"> 
                            <label class="col-sm-2 control-label" >Promotion URL <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <input type="text" id="promotionURL" name="promotionURL" title="Please enter promotion URL" class="form-control">
                            </div>
                            <label class="col-sm-2 control-label" >Reward to no of user <span style="color: red">*</span></label>
                            <div class="col-sm-3">                               
                                <input type="text" id="noOfUser" name="noOfUser" title="Please enter no of user" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">                                                                
                            <label class="col-sm-2 control-label" >Campaign phrase </label>
                            <div class="col-sm-8">
                                <textarea id="campaignPhrase" name="campaignPhrase" title="Please enter campaign phrase" class="form-control"></textarea>
                                <small>You can add multiple meaningful pharse by put comma at the end</small>
                            </div>
                        </div>
                        <div class="form-group">                                                                
                            <label class="col-sm-2 control-label" >Reward message <span style="color: red">*</span></label>
                            <div class="col-sm-8">                                
                                <textarea rows="4" cols="80" class="form-control" name="message" id="message" title="Please enter message" placeholder="Your message" ></textarea>
                                <br>
                                You have
                                <input readonly type="text" name="countdown" size="3" value="<%=520%>"> chars left                                
                            </div>
                        </div>
                        <h6><i class="fa fa-clock-o fa-2x text-info">  Campaign Period</i></h6><br/>    
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Start Date</label>
                            <div class="col-sm-3">                               
                                <input type="text" id="datapicker1" name="datapicker1" class="form-control">
                            </div>
                            <label class="col-sm-2 control-label" >End Date</label>
                            <div class="col-sm-3">
                                <input type="text" id="datapicker2" name="datapicker2" class="form-control">  
                            </div>                            
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Start Time</label>
                            <div class="col-sm-3">                               
                                <input type="text" id="datapicker3" name="datapicker3" class="form-control clockpicker" >
                            </div>
                            <label class="col-sm-2 control-label" >End Time</label>
                            <div class="col-sm-3">                                
                                <input type="text" id="datapicker4" name="datapicker4" class="form-control clockpicker"> 
                            </div>
                        </div>
                        <h6><i class="fa fa-map-marker fa-2x text-info"> Campaign For</i></h6><br/>    
                        <div class="form-group">                                                                        
                            <label class="col-sm-2 control-label">Country </label>
                            <div class="col-sm-3">                                                                                                
                                <select  id="country" name="country" class="form-control">
                                    <option id="-1" value="-1">Select Country</option>
                                </select>
                            </div>
                            <label class="col-sm-2 control-label" >State </label>
                            <div class="col-sm-3">                                
                                <select name="state1" id="state1" class="form-control"> 
                                    <option id="-1">Select State </option>
                                </select>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >City </label>
                            <div class="col-sm-3">                                
<!--                                <select name="state" id="state" class="form-control" onchange="print_cities('cities',this.selectedIndex);">  -->
                                <select name="city1" id="city1" class="form-control"> 
                                    <option id="-1">Select City </option>
                                </select>
                            </div> 
                        </div>                            
                        <h6><i class="fa fa-users fa-2x text-info">  Target User have</i></h6><br/>    
                        <div class="form-group">                                                                        
                            <label class="col-sm-2 control-label">No of follower </label>
                            <div class="col-sm-3">                                
                                <input type="text" id="noOfFollower" name="noOfFollower" title="Please enter no of friends" class="form-control">
                            </div>
                            <label class="col-sm-2 control-label" >No of friends </label>
                            <div class="col-sm-3">                                
                                <input type="text" id="noOfFriends" name="noOfFriends" title="Please enter no of friends" class="form-control">
                            </div>                                    
                        </div>
                        <h6><i class="fa fa-street-view fa-2x text-info">  Social Settings</i></h6><br/>
                        <div class="form-group">    
                            <label class="col-sm-2 control-label">Twitter Setting </label>
                            <div class="col-sm-3">  
                                <select id="twitterSetting" name="twitterSetting" class="form-control">
                                    <option id="-1" value="-1">Select Twitter Setting</option>
                                 </select>
                            </div>
                        </div>
                        <br>  
                        <button class="btn btn-primary ladda-button validateProfileForm" data-style="zoom-in" style="display: none">Save changes</button>    
                    </form>                                                                                                                              
                    <h6><i class="fa fa-image fa-2x text-info"> Upload Logo</h6></i> <br/>
                    <div style="background: #fff; height: 200px">
                        <div class="col-sm-8 col-lg-8 col-md-8" style="width: 68%;margin-left: 16%">
                            <form action="UploadEmailAttachme" method="post" class="dropzone" id="my-dropzone"></form>	
                            <small>Upload Logo pic. You can upload mutiple logo for campaign</small>
                        </div>
                    </div>
                    <br><br>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-8" style="margin-left: 16%">
                            <a class="btn btn-default" href="./header.jsp">Cancel</a>                            
                            <a class="btn btn-info ladda-button" id="partUpdate" data-style="zoom-in" >Save changes</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>                             
    </div>
</div>
        <!--Update campaign -->
        <div class="content">
            <div class="row">
                <div class="col-md-12 tour-61">
                    <div class="hpanel">
                        <div class="panel-body">
                            <input type="text" class="form-control input-sm m-b-md" id="filter" placeholder="Search in table">                                        
                            <div class="table-responsive">
                                <table id="api" class="footable table table-stripped table-responsive" valign="middle" data-page-size="25" data-filter=#filter>    
                                    <thead>
                                        <tr>                                    
                                            <!--                                    <th style="text-align: center">Email Ad Logo</th> -->
                                            <th style="text-align: center" class="tour-15">Title</th>
                                            <th style="text-align: center" class="tour-15">Social Setting</th>                                    
                                            <th style="text-align: center" class="tour-15">Credits Used</th>
                                            <th style="text-align: center" class="tour-16">Performance</th>
                                            <th style="text-align: center">Status</th>
                                            <th style="text-align: center">Start Campaign</th>
                                            <th style="text-align: center">Stop Campaign</th>
                                            <th style="text-align: center" class="tour-17">Update</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>                                                                                                                        
                                            <td>
                                                <p>Republic Day</p>
                                            </td>
                                            <td>
                                                <p>abhishek_twitter</p>
                                            </td>                                   
                                            <td style="" align="center" valign="middle">
                                                <a class="text-error ladda-button" data-style="zoom-in" id="todayCredit"  type="button" href="#" data-toggle="tooltip" data-placement="right" title="Today Credit Used for"><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                                <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyCredit"  type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Credit Used for" style="margin-left: 10%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                            </td>

                                            <td style="" align="center" valign="middle">
                                                <a class="text-error ladda-button" data-style="zoom-in" id="todayPerformance"  type="button" href="#" data-toggle="tooltip" data-placement="right" title="Today Performance of "><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                                <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyPerformance"  type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Performace of " style="margin-left: 10%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                            </td>
                                            <td style="" align="center" valign="middle">
                                                <p style="font-size: 15px" data-toggle="tooltip" data-placement="right" title="Your Campaign is in active state"><i class="fa fa-play-circle fa-2x text-info"></i> </p>                                       
                                            </td>
                                            <td style="" class="tour-70" align="center" valign="middle">                                        
                                                <a class="text-info" data-toggle="tooltip" data-placement="right" title="Start Campaign" ><i class="pe pe-7s-volume1 pe-3x"></i></a>
                                            </td>
                                            <td style="" class="tour-71" align="center" valign="middle">                                        
                                                <a  class="text-danger" data-toggle="tooltip" data-placement="right" title="Stop Campaign" ><i class="pe pe-7s-volume2 pe-3x"></i></a>
                                            </td>
                                            <td style="" class="tour-iconAPIConsole tour-62" align="center" valign="middle">   
                                                <a  class="text-warning" data-toggle="tooltip" data-placement="right" title="Update Campaign" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                            </td>                                    
                                        </tr>  
                                        <tr>                                                                                                                        
                                            <td>
                                                <p>Christmas</p>
                                            </td>
                                            <td>
                                                <p>Shailendra_twitter</p>
                                            </td>                                   
                                            <td style="" align="center" valign="middle">
                                                <a class="text-error ladda-button" data-style="zoom-in" id="todayCredit"  type="button" href="#" data-toggle="tooltip" data-placement="right" title="Today Credit Used for"><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                                <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyCredit"  type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Credit Used for" style="margin-left: 10%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                            </td>

                                            <td style="" align="center" valign="middle">
                                                <a class="text-error ladda-button" data-style="zoom-in" id="todayPerformance"  type="button" href="#" data-toggle="tooltip" data-placement="right" title="Today Performance of "><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                                <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyPerformance"  type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Performace of " style="margin-left: 10%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                            </td>
                                            <td style="" align="center" valign="middle">
                                                <p style="font-size: 15px" data-toggle="tooltip" data-placement="right" title="Your Campaign is in active state"><i class="fa fa-play-circle fa-2x text-info"></i> </p>                                       
                                            </td>
                                            <td style="" align="center" valign="middle">                                        
                                                <a class="text-info" data-toggle="tooltip" data-placement="right" title="Start Campaign" ><i class="pe pe-7s-volume1 pe-3x"></i></a>
                                            </td>
                                            <td style="" align="center" valign="middle">                                        
                                                <a  class="text-danger" data-toggle="tooltip" data-placement="right" title="Stop Campaign" ><i class="pe pe-7s-volume2 pe-3x"></i></a>
                                            </td>
                                            <td style="" class="tour-iconAPIConsole" align="center" valign="middle">   
                                                <a  class="text-warning" data-toggle="tooltip" data-placement="right" title="Update Campaign" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                            </td>                                    
                                        </tr>  
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="10">
                                                <ul class="pagination pull-right"></ul>
                                            </td>
                                        </tr>
                                    </tfoot>

                                </table>
                                <a class="btn btn-info btn-sm" data-style="zoom-in" id="todayPerformance" href="#">Create Campaign</a>                                                        
                            </div>                   
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-bottom: 28%"></div>
            </div>
        </div>
        <!--Unsubscribe package-->
        <div class="small-header transition animated fadeIn" id="subscriptionPackageHeader">
            <div class="hpanel">
                <div class="panel-body">
                    <h2 class="font-light m-b-xs">
                        Subscription Package
                    </h2>
                    <small>You can Un-subscribe recurring and subscribe with other package from here.</small>
                </div>
            </div>
        </div>
        <div class="content animate-panel" data-effect="rotateInDownRight" data-child="element">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 ">
                    <div class="row">
                        <div class="col-sm-5 col-lg-5 col-md-5 text-center tour-65">
                            <div class="hpanel plan-box hblue active">
                                <div class="panel-heading hbuilt text-center">
                                    <h4 class="font-bold">Basic</h4>
                                </div>
                                <div class="panel-body">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>
                                                    Features
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>                                    
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Response Time (5 days)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Resolution Time (14 days)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Support Available (Email)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>  Special designed Campaign Console
                                                </td>
                                            </tr>                                                                                                      
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>  Reports
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>  All In One Solution
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-square-o"></i> Webex Remote Resolution
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-square-o"></i> One on One Discussion
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-square-o"></i> Uptime
                                                </td>            
                                            </tr>                                    
                                            <tr>
                                                <td>
                                                    <i class="fa fa-square-o"></i> Support Hours
                                                </td>            
                                            </tr>                                    
                                        </tbody>
                                    </table>
                                    <h3 class="font-bold">
                                        A$  1.0/Month
                                    </h3>

                                    <a class="btn btn-info btn-sm ladda-button element" data-style="zoom-in" id="unSubscripbePackage" onclick=""> Un-Subscribe Recurring</a>                                                                                                                                                                      
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--        <div class="col-md-12" style="margin-bottom: 40%" id="marginFooter"></div>-->
            </div>
        </div>
        <!--Reports Inner-->
        <div class="col-lg-12 col-md-12 col-sm-12">
            <form class="form-horizontal " method="get">       
                <div class="dataTable_wrapper table-responsive tour-74">
                    <div class="col-lg-12">
                        <div class="hpanel" style="height: 50%">
                            <div class="panel-body" style="text-align: center;">
                                <form id="api_usageV2" class="form-horizontal">
                                    <div class="col-md-12">                           						
                                        <div class="col-sm-3">
                                            <select class="js-source-states-33" id="campaignDetailsTour" name="campaignDetailsTour" multiple="multiple" style="width: 100%">                                                        
                                                <option value="New Year" selected>New Year</option> 
                                            </select>
                                        </div>
                                        <script>
                                            $(function () {
                                                $(".js-source-states-33").select2();

                                            });
                                        </script>
                                        <div class="col-sm-3"><input  value="08/26/2017" type="text" class="form-control" placeholder="from"></div>
                                        <div class="col-sm-3"><input value="09/10/2017" type="text" class="form-control" placeholder="to"></div>                                        
                                        <div class="col-sm-3">
                                            <a class="btn btn-success ladda-button btn-sm btn-block" data-style="zoom-in" id="generateButton"><i class="fa fa-bar-chart"></i> Generate</a>                                                                                                                                     
                                        </div>                                    
                                        <br/><br/><br/><br/>
                                        <table class="table table-striped table-bordered table-hover dataTables-example">
                                            <thead id="apiUsageHeader">
                                                <tr>
                                                    <th style="text-align: center">Sr.No.</th>                        
                                                    <th style="text-align: center">Campaign</th>                        
                                                    <th style="text-align: center">Campaign Execution Name</th>                                                            
                                                    <th style="text-align: center">Date</th>
                                                    <th style="text-align: center" >View User Find</th>
                                                </tr>
                                            </thead>
                                        <tbody>
                                            <tr>
                                                <td ><font><%=1%></font></td>                        
                                                <td ><font >New Year</font></td>                         
                                                <td ><font>Campaign for Australia</font></td>
                                                <td ><font>22/1/2018 12:08PM</font></td>
                                                <td class="tour-reportViewUserFind"><a class="btn btn-success btn-xs"> View </a></td>
                                            </tr>
                                            <tr>
                                                <td ><font><%=2%></font></td>                        
                                                <td ><font >New Year</font></td>                         
                                                <td ><font>Campaign for India</font></td>
                                                <td ><font>22/2/2018 04:00PM</font></td>
                                                <td><a class="btn btn-success btn-xs"> View </a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <p style="float:left">You have run the campaigns 2 time</p>
                                </div>
                            </div>
                        </div>                       
                    </div>
                </div>
            </form>
        </div>
        <script>
            var chart1 = c3.generate({
                bindto: '#flotpiechart',
                data: {
                    // iris data from R
                    columns: [
                        ["Campaign One", 60],
                        ["Campaign Two", 30],
                        ["Campaign Three", 10],
                        ["data4", 0],
                        ["data5", 0]
                    ],
                    type: 'pie'
                },
                colors: {
                    data1: '#F39C12',
                    data2: '#F8C471',
                    data3: '#F5B041',
                    data4: '#F39C12',
                    data5: '#D68910'
                },
                legend: {
                    show: false
                },
                size: {
                    width: 150,
                    height: 175
                },
                label: {
                    format: function (value, ratio, id) {
                        return d3.format('$')(value);
                    }
                }
            });

            var apiname = ["api name","Christmas","New Year","Republic Day","14 Feb"];
            var obj =  ["Credit used",100,80,30,130];
            console.log("apiname tour== "+apiname);
            console.log("obj tour== "+obj);
            var chart = c3.generate({
                bindto: '#topFiveSerTour',
                size: {
                    height: 240,
                },
                data: {
                    x: 'api name',
                    columns: [
                        apiname,
                        obj,
                    ],
                    colors: {
                        otherUserCount: '#62cb31',
                        partnerCount: '#FF7F50'
//                  callCountArr: '#62cb31',
//                  TransactionAmount: '#FF7F50'
                    },
                    types: {
                        //CallCount: 'bar',
                        CallCount: 'bar'
                    },
                    groups: [
                        ['Credit Count']
                    ]
                },
                subchart: {
                    show: false
                },
                axis: {
                    x: {
                        type: 'category',
                        categories: apiname
                    }
                }
            });
            chart.resize();
        </script>
    </body>
</html>