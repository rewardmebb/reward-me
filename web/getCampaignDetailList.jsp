<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.SocialMediaManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmSocialmediadetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.SubscriptionManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmSubscriptionpackagedetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.CampaignManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmCampaigndetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmBrandownerdetails"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.rewardme.nucleus.commons.GlobalStatus"%>
<%@page import="org.json.JSONObject"%>
<script src="scripts/profile.js" type="text/javascript"></script>
<style>
    td {
  text-align: center;
  vertical-align: middle;
}
</style>
<%
    String SessionId = (String) request.getSession().getAttribute("_brandownerSessionId");
    if (SessionId == null) {
        response.sendRedirect("logout.jsp");
        return;
    }  
%>    
<div id="wrapper">
    <div class="col-lg-6">        
        <div class="modal fade hmodal-success" id="campaignExecutionModal" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header" style="padding: 10px 0px !important">
                        <h4 id="todayCreditForAPILable" style="margin-left: 2%">Campaign Execution Name
                            <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
                    </div>
                    <div class="modal-body" style="padding-bottom: 50px !important;">                                                        
                        <input type="hidden" id="campaignUniqueID" name="campaignUniqueID">
                        <input type="text" id="campaignExecutionName" placeholder="Please enter campaign execution name" name="campaignExecutionName" class="form-control" onblur="checkCampaignExecutionName()">                        
                        <small id="checkAvailability-result"></small>
                        <br>
                        <small>Please enter meaningful name which is used to differentiate between campaign runs</small>
                        <br><br>
                        <a class="btn btn-info btn-sm ladda-button" data-style="zoom-in" id="startCampaignButton" onclick="startCampaignWithExeName()" href="#">Start Campaign</a>                                                        
                    </div>                                
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-lg-6">
        <div class="modal fade hmodal-success" id="todayExpenditure" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header" style="padding: 10px 0px !important">
                        <h4 id="todayCreditForAPILable" style="margin-left: 2%">Today Credit Used
                            <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
                    </div>
                    <div class="modal-body" style="padding-bottom: 50px !important;text-align: center;">                                
                        <div id="todayCreditForAPI">                        
                        </div>
                        <div id="noRecordFoundData" style="display: none;text-align: center;" style="margin-bottom: 30%">
                            <img src="images/no_record_found.png" alt="No record found" width="300px" height="200px"/>
                        </div> 
                    </div>                                
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="modal fade hmodal-success" id="todayPerformance" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header" style="padding: 10px 0px !important">
                        <h4 id="todayPerformanceForAPILable" style="margin-left: 2%">Today Performance
                            <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
                    </div>
                    <div class="modal-body" style="padding-bottom: 50px !important">                                
                        <div id="todayPerformanceForAPI">                        
                        </div>
                        <div id="noRecordFoundDataPerforamce" style="display: none; text-align: center" style="margin-bottom: 30%">
                            <img src="images/no_record_found.png" alt="No record found" width="300px" height="200px"/>
                        </div>                         
                    </div>                                
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="modal fade hmodal-success" id="monthlyPerformance" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg" >
                <div class="modal-content" >
                    <div class="color-line"></div>
                    <div class="modal-header" style="padding: 10px 0px !important">
                        <h4  style="margin-left: 2%"> Monthly Performance <font id="monthlyPerformanceForAPILable"></font>
                            <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
                    </div>
                    <div class="modal-body">
                        <div style="margin-bottom: 20px;">
                            <input type="hidden" id="adIdForMothlyPerformance" name="adIdForMothlyPerformance">
                            <input type="hidden" id="adTypeForMothlyPerformance" name="adTypeForMothlyPerformance">
                            <div class="col-sm-3">
                                <select class="form-control" id="_apiCallMonth" name="_apiCallMonth" onchange="generatePDFADPerformanceByMonth()">                                                                        
                                    <%                                        
                                        SimpleDateFormat format = new SimpleDateFormat("MMM");
                                        Calendar cal = Calendar.getInstance();
                                        cal.setTime(new Date());
                                        cal.add(Calendar.MONTH, -6);
                                        Calendar updatedCal = Calendar.getInstance();
                                        Date sixMonthPerformaceBack = cal.getTime();
                                        updatedCal.setTime(sixMonthPerformaceBack);                                        
                                        for (int month = 1; month <= 6; month++) {
                                            int calMonth = cal.get(Calendar.MONTH) + month;
                                            updatedCal.set(Calendar.MONTH, calMonth);
                                            Date upDate = updatedCal.getTime();
                                            String newMonth = format.format(upDate);
                                            if(newMonth.equalsIgnoreCase("Jan")){
                                                calMonth = 0;
                                            }
                                    %>
                                    <%if (month == 6) {%>
                                    <option value="<%=calMonth + 1%>" selected><%=newMonth%></option> 
                                    <%} else {%>
                                    <option value="<%=calMonth + 1%>"><%=newMonth%></option> 
                                    <%
                                            }
                                        }
                                    %>

                                </select>
                            </div>

                            <!--<div class="col-sm-3"><button id="generatePerformanceByMonth" class="btn btn-success btn-sm ladda-button btn-block" data-style="zoom-in" onclick="generatePerformanceByMonth()"><i class="fa fa-bar-chart"></i> Generate</button></div>-->
                        </div>
                        <br/>
                        <div id="monthlyPerformanceForAPI">                        
                        </div>
                        <div id="noRecordFoundDataPerformance" style="display: none;text-align: center" style="margin-bottom: 27%">
                            <img src="images/no_record_found.png" alt="No record found" width="300px" height="200px"/>
                        </div> 
                    </div>                                
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="modal fade hmodal-success" id="monthlyCredit" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header" style="padding: 10px 0px !important">
                        <h4 style="margin-left: 2%"> Monthly Credit Used<font id="monthlyCreditForAPILable" ></font>
                            <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>                                    
                    </div>
                    <div class="modal-body" style="padding-bottom: 50px !important">                                
                        <div style="margin-bottom: 20px;" class="col-centered">
                            <input type="hidden" id="advertiserAdIdForMothlyCredit" name="advertiserAdIdForMothlyCredit">
                            <input type="hidden" id="adType" name="adType">
                            <div class="col-sm-3">
                                <select class="form-control" id="_apiCallMonthMonthlyCredit" name="_apiCallMonthMonthlyCredit" onchange="generateAdvertiserCreditByMonth()">                                                                        

                                    <%
                                        cal.setTime(new Date());
                                        cal.add(Calendar.MONTH, -6);
                                        Date sixMonthBack = cal.getTime();
                                        updatedCal.setTime(sixMonthBack);
                                        for (int month = 1; month <= 6; month++) {
                                            int calMonth = cal.get(Calendar.MONTH) + month;
                                            updatedCal.set(Calendar.MONTH, calMonth);
                                           
                                            Date upDate = updatedCal.getTime();
                                            String newMonth = format.format(upDate);
                                            if(newMonth.equalsIgnoreCase("Jan")){
                                                calMonth = 0;
                                            }
                                    %>
                                    <%if (month == 6) {%>
                                    <option value="<%=calMonth + 1%>" selected><%=newMonth%></option> 
                                    <%} else {%>
                                    <option value="<%=calMonth + 1%>"><%=newMonth%></option> 
                                    <%
                                            }
                                        }
                                    %>
                                </select>
                            </div>
                        </div>
                        <br><br>
                        <div id="monthlyCreditForAPI">                        
                        </div>
                        <div id="noRecordFoundDataMonthlyCredit" style="display: none ;text-align: center;" style="margin-bottom: 30%">
                            <img src="images/no_record_found.png" alt="No record found" width="300px" height="200px"/>
                        </div> 


                    </div>                                
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content">
    <div class="row">
        <div class="col-md-12 tour-13">
            <div class="hpanel">
                <div class="panel-body">
                    <input type="text" class="form-control input-sm m-b-md" id="filter" placeholder="Search in table">                                        
                    <div class="table-responsive">
                        <table id="api" class="footable table table-stripped table-responsive" valign="middle" data-page-size="25" data-filter=#filter>    
                            <thead>
                                <tr>                                    
<!--                                    <th style="text-align: center">Email Ad Logo</th>                                    -->
                                    <th style="text-align: center" class="tour-15">Title</th>
                                    <th style="text-align: center" class="tour-15">Social Setting</th>                                    
                                    <th style="text-align: center" class="tour-15">Credits Used</th>
                                    <th style="text-align: center" class="tour-16">Performance</th>
                                    <th data-hide="phone,tablet" style="text-align: center">Credits</th>                                    
                                    <th style="text-align: center">Status</th>
                                    <th style="text-align: center">Start Campaign</th>
                                    <th style="text-align: center">Stop Campaign</th>
                                    <th style="text-align: center" class="tour-17">Update</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    RmBrandownerdetails usrObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
                                    RmCampaigndetails[] campaignDetails = new CampaignManagement().getCampaignByOwnerId(usrObj.getOwnerId());
                                    //String base64PDFImage = adDetails.getEmailAdImage(); 
                                    String creditDeductionPerAd="NA"; String campaignTitle = "NA";
                                    RmSubscriptionpackagedetails subscriObject1 = new SubscriptionManagement().getSubscriptionbyOwnerId(usrObj.getOwnerId());                            
                                    if(campaignDetails != null){
                                        for (int i = 0; i < campaignDetails.length; i++) {
                                            byte[] logo = campaignDetails[i].getLogoImages();
                                            String logoString = null;
                                            if(logo != null){
                                                logoString=new String(logo);
                                            }
                                            if(subscriObject1 != null){
                                                String creditDeduction = subscriObject1.getCreditDeductionConfiguration();
                                                if(creditDeduction != null){
                                                    JSONObject creditJson = new JSONObject(creditDeduction);
                                                    creditDeductionPerAd  = creditJson.getString("perCampaign");
                                                }
                                            }
                                            campaignTitle = campaignDetails[i].getRewardTitle();
                                           // System.out.println("logoString >> "+logoString);
                                           String socialSettingDetails = "NA";
                                           RmSocialmediadetails socialMedia = new SocialMediaManagement().getSocialMediaDetailsByUniqueId(campaignDetails[i].getTwitterSocialSetting());
                                           if(socialMedia != null){
                                               socialSettingDetails = socialMedia.getTwitterHandlerName();
                                           }
                                %>
                                <tr>                                   
<!--                                    <td  style="font-size: 15px" style="" align="center" valign="middle">
                                        <image src="data:image/jpg;base64," class="img-small" alt="PDF Ad Image">
                                    </td>                                                                                                                        -->
                                    <td>
                                        <p><%=campaignTitle%></p>
                                    </td>
                                    <td>
                                        <p><%=socialSettingDetails%></p>
                                    </td>                                   
                                    <td style="" align="center" valign="middle">
                                        <a class="text-error ladda-button" data-style="zoom-in" id="todayCredit" onclick="todayCreditPDFAd('<%=campaignDetails[i].getCampaignId()%>')" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Today Credit Used for"><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                        <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyCredit" onclick="monthlyPDFAdCreditChart('<%=campaignDetails[i].getCampaignId()%>')" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Credit Used for" style="margin-left: 10%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                    </td>
                                   
                                    <td style="" align="center" valign="middle">
                                        <a class="text-error ladda-button" data-style="zoom-in" id="todayPerformance" onclick="todayPDFAdPerformance('<%=campaignDetails[i].getCampaignId()%>')" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Today Performance of "><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                        <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyPerformance" onclick="monthlyPDFAdPerformance('<%=campaignDetails[i].getCampaignId()%>')" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Monthly Performace of " style="margin-left: 10%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                    </td>
                                    <td style="" align="center" valign="middle">                                        
                                        <p style="font-size: 15px"><%=creditDeductionPerAd%> Per Campaign</p>
                                    </td>
                                    <td style="" align="center" valign="middle">
                                        <%if(campaignDetails[i].getStatus() == GlobalStatus.ACTIVE){%>
                                        <p style="font-size: 15px" data-toggle="tooltip" data-placement="right" title="Your Campaign is in active state"><i class="fa fa-play-circle fa-2x text-info"></i> </p>                                       
                                        <%}else if(campaignDetails[i].getStatus() == GlobalStatus.START_PROCESS){%>
                                        <p style="font-size: 15px" data-toggle="tooltip" data-placement="right" title="Your Campaign is in running state"><i class="fa fa-stop-circle fa-2x text-info"></i> </p>                                       
                                        <%}%>
                                    </td>
                                    <td style="" align="center" valign="middle">                                        
                                        <%if(campaignDetails[i].getStatus() == GlobalStatus.ACTIVE){%>
                                        <a onclick = "campaignExecutionModal('<%=campaignDetails[i].getCampaignUniqueId()%>','<%=i%>')" class="text-info" data-toggle="tooltip" data-placement="right" title="Start Campaign" id="startCampaign<%=i%>"><i class="pe pe-7s-volume1 pe-3x"></i></a>
                                        <%}else if(campaignDetails[i].getStatus() == GlobalStatus.START_PROCESS){%>
                                        <a onclick = "campaignExecutionModal('<%=campaignDetails[i].getCampaignUniqueId()%>','<%=i%>')" class="text-info" data-toggle="tooltip" data-placement="right" title="Start Campaign" id="startCampaign<%=i%>"><i class="pe pe-7s-volume1 pe-3x"></i></a>
                                        <%}%>
                                    </td>
                                    <td style="" align="center" valign="middle">                                        
                                        <a onclick = "stopCampign('<%=campaignDetails[i].getCampaignUniqueId()%>')" class="text-danger" data-toggle="tooltip" data-placement="right" title="Stop Campaign" id="stopCampaign<%=i%>"><i class="pe pe-7s-volume2 pe-3x"></i></a>
                                    </td>
                                    <td style="" class="tour-iconAPIConsole" align="center" valign="middle">   
                                        <a onclick = "editCampaign('<%=campaignDetails[i].getCampaignId()%>')" class="text-warning" data-toggle="tooltip" data-placement="right" title="Update Campaign" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                    </td>                                    
                                </tr>  
                                <%}}%>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="10">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                            </tfoot>

                        </table>
                        <a class="btn btn-info btn-sm" data-style="zoom-in" id="todayPerformance" onclick="addNewCampaign()" href="#">Create Campaign</a>                                                        
                    </div>                   
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-bottom: 28%"></div>
    </div>
</div>
<script>
    $(function () {
        $('#api').footable();
    });
</script>