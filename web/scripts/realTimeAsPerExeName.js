var updateInterval = 5;
var upinter = 5000;
var v1 = 0;
var settingName;
var type;
var starttime = new Date();
var endtime = new Date();

function loadCountsAsPerExeName() {
    var s = './RealTimeAsPerExeName';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#realtime").serialize(),
        success: function(data) {
            v1 = data._count;
            //console.log("Count >> "+data._count);
            updateInterval = 5;
            upinter = updateInterval * 1000;
//            if(data._count > 0){
//                document.querySelector('.refreshCampaignReport').click();
//            }
            
        }
    });
}
//// area chart
// msg 1
$(function() {
    var data1 = new RealTimeData1(1);
    var chart1 = $('#real-time-line').epoch({
        type: 'time.line',
        //type: 'time.heatmap',
        data: data1.history(),
        axes: ['left', 'bottom']
    });
    setInterval(function() {
        chart1.push(data1.next());
        //console.log("v1 >> "+v1);                            
        
    }, upinter);
    chart1.push(data1.next());
});

// bar chart

// msg 1
$(function() {
    var data1 = new RealTimeData1(1);
    var chart1 = $('#real-time-line').epoch({
        type: 'time.bar',
        data: data1.history(),
        axes: ['left', 'bottom']
    });
    charts = $('#real-time-line').epoch;
    setInterval(function() {
        chart1.push(data1.next());
        //console.log("v1 >> "+v1);        
    }, upinter);
    chart1.push(data1.next());
});

(function() {
    var RealTimeData1 = function(layers) {
        this.layers = layers;
        var t = new Date();
        var mili = t.getMilliseconds();
        t.setMilliseconds(mili - 30000);
//        alert(t.getTime());
        this.timestamp = (t.getTime() / 1000) | 0;
//        alert(this.timestamp);
    };
//     history for initial data

    RealTimeData1.prototype.history = function(entries)
    {
        if (typeof (entries) != 'number' || !entries) {
            entries = 1;
        }
        var history = [];
        for (var k = 0; k < this.layers; k++) {
            history.push({values: []});
        }
        for (var i = 0; i < entries; i++) {
            history[0].values.push({time: this.timestamp, y: (v1)});
            this.timestamp = this.timestamp + 1;            
        }
        return history;
    };
    RealTimeData1.prototype.next = function() {     // push data for the graph    
        var entry = [];
        loadCountsAsPerExeName();
        entry.push({time: this.timestamp, y: (v1)});
        this.timestamp = this.timestamp + updateInterval;        
        return entry;        
    };
    window.RealTimeData1 = RealTimeData1;
})();

function loadRealTimeAsPerExeName(){    
    loadCountsAsPerExeName();    
}
