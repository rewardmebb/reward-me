/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var tour;
function startTour() {
    // Instance the tour

    tour = new Tour({
        backdrop: true,
        reflex: false,
        orphan: true,
        backdropContainer: 'body',
        onShown: function (tour) {

            // ISSUE    - https://github.com/sorich87/bootstrap-tour/issues/189
            // FIX      - https://github.com/sorich87/bootstrap-tour/issues/189#issuecomment-49007822

            // You have to write your used animated effect class
            // Standard animated class
            $('.animated').removeClass('fadeIn');
            // Animate class from animate-panel plugin
            $('.animated-panel').removeClass('zoomIn');

        },
        onEnd: function (tour) {
            clearPanels();
            document.getElementById("tourDiv").style.display = 'none';
            if (typeof (test) != "undefined") {
                if (enable === true) {
                    test.classList.add("show-sidebar");
                    test.classList.remove("hide-sidebar");
                } else {
                    test.classList.remove("show-sidebar");
                    test.classList.add("hide-sidebar");
                }
            }
        },
        steps: [
            {
                //element: ".tourWelcome",
                title: "Welcome to Reward Me",
                content: "We're thankful you've joined with us. Please take a brief tour to dashboard to introduce each menu and how it works.",
                placement: "right",
                orphan: true,
                onPrev: function (tour) {
                    clearPanels();
                    document.getElementById("footerPage").style.display = 'block';
                    document.getElementById("tourDiv").style.display = 'none';
                }

            },
//            {
//                element: ".tour-twitterSocial",
//                title: "Twitter Social setting (1.a/12)",
//                content: "You can get service details with your APIToken key,.",
//                placement: "left",
//                onNext: function (tour) {
//                    if ((test.className).indexOf("show-sidebar") > -1) {
//                        test.classList.remove("show-sidebar");
//                        test.classList.add("hide-sidebar");
//                    }
////                    test.className.remove("show-sidebar");
////                    test.classList.add("hide-sidebar");
//                }, onPrev: function (tour) {
//                    if ((test.className).indexOf("hide-sidebar") > -1) {
//                        test.classList.remove("hide-sidebar");
//                        test.classList.add("show-sidebar");
//                    }
//                }
//
//            },
            
            {
                element: ".tour-1",
                title: "15 days credit used (1/12)",
                content: "From here you get information of your credit usage from last 15 days.",
                placement: "right",
            },
            {
                element: ".tour-2",
                title: "Remaining Credit (2/12)",
                content: "You get information about the remaining credit and the package expiry date and time.",
                placement: "right",
                onNext: function (tour) {
                    if ((test.className).indexOf("show-sidebar") > -1) {
                        test.classList.remove("show-sidebar");
                        test.classList.add("hide-sidebar");
                    }
//                    test.className.remove("show-sidebar");
//                    test.classList.add("hide-sidebar");
                }, onPrev: function (tour) {
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    }
                }

            },
            {
                element: ".tour-3",
                title: "Social Setting (3/12)",
                content: "For run the campaign you need to add your social API keys. We also provide the guideline for how to get API keys from top right corner. We recommend you please go through this step first.",
                placement: "right",
                onNext: function (tour) {
                    if ((test.className).indexOf("show-sidebar") > -1) {
                        test.classList.remove("show-sidebar");
                        test.classList.add("hide-sidebar");
                    }
//                    test.className.remove("show-sidebar");
//                    test.classList.add("hide-sidebar");
                }, onPrev: function (tour) {
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    }
                }

            },
            {
                element: ".tour-55",
                title: "Create Social setting (3.a/12)",
                content: "You can add your API keys for each Social media seperately.",
                placement: "top",
                onNext: function (tour) {
                    if ((test.className).indexOf("show-sidebar") > -1) {
                        test.classList.remove("show-sidebar");
                        test.classList.add("hide-sidebar");
                    }
//                    test.className.remove("show-sidebar");
//                    test.classList.add("hide-sidebar");
                }, onPrev: function (tour) {
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    }
                }

            },
            {
                element: ".tour-56",
                title: "Social Setting  (3.b/12)",
                content: "You get list for all social settings from here also you can add more social settings to run multiple campaigns at one time.",
                placement: "top",
                onNext: function (tour) {
                    if ((test.className).indexOf("show-sidebar") > -1) {
                        test.classList.remove("show-sidebar");
                        test.classList.add("hide-sidebar");
                    }
//                    test.className.remove("show-sidebar");
//                    test.classList.add("hide-sidebar");
                }, onPrev: function (tour) {
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    }
                }

            },
            {
                element: ".tour-63",
                title: "Edit Setting (3.c/12)",
                content: "Here you get the information about your social media API keys which you have added also you can update the keys.",
                placement: "left",
            },
            {
                element: ".tour-58",
                title: "Campaign  (4/12)",
                content: "You can create a campaign, view the list for campagin created, also you can start, stop the campaign from here.",
                placement: "right",
                onNext: function (tour) {
                    if ((test.className).indexOf("show-sidebar") > -1) {
                        test.classList.remove("show-sidebar");
                        test.classList.add("hide-sidebar");
                    }
//                    test.className.remove("show-sidebar");
//                    test.classList.add("hide-sidebar");
                }, onPrev: function (tour) {
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    }
                }

            },
            
            {
                element: ".tour-60",
                title: "Create Campaign (4.a/12)",
                content: "You create the campaign from here with Reward message, No of person you are going to give reward, Start and End date for campaign, Region in which you want to run the campaigns, Upload your campaign logo which is used for find person on the basis of logo detection in photos of that person over social networking.",
                placement: "top",
            },
            {
                element: ".tour-61",
                title: "Campaign List (4.b/12)",
                content: "Here you get the list for all campaign details with social setting which you have configured, Daily and Monthly campaign runs and credit used, start and stop campaigns and update the campagin.",
                placement: "top",
            },
            {
                element: ".tour-70",
                title: "Start Campaign (4.c/12)",
                content: "From here you can start the campaign.",
                placement: "left",
            },
            {
                element: ".tour-71",
                title: "Stop Campaign (4.d/12)",
                content: "From here you can stop the campaign.",
                placement: "left",
            },
            {
                element: ".tour-62",
                title: "Edit Campaign (4.e/12)",
                content: "From here you can update the campaign details.",
                placement: "left",
            },
            
            {
                element: ".tour-59",
                title: "Report  (5/12)",
                content: "You can view the textual report for how many campaign you have run on the basis of specific campaign, for all campaign, start and end date of the campaign",
                placement: "right",
                onNext: function (tour) {
                    if ((test.className).indexOf("show-sidebar") > -1) {
                        test.classList.remove("show-sidebar");
                        test.classList.add("hide-sidebar");
                    }
//                    test.className.remove("show-sidebar");
//                    test.classList.add("hide-sidebar");
                }, onPrev: function (tour) {
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    }
                }

            },
            {
                element: ".tour-74",
                title: "Report (5.a/12)",
                content: "Here you get the API details as per the services you selected.",
                placement: "top",
            },
            {
                element: ".tour-reportViewUserFind",
                title: "Report (5.b/12)",
                content: "From here you get details of user find over the social networking as per the campaign execution name. You can give the execution name as per you want during before start the campaign.",
                placement: "top",
            },
            {
                element: ".tour-57",
                title: "Invoices  (6/12)",
                content: "You can download your payment invoices also get overview of your payment history.",
                placement: "right",
                onNext: function (tour) {
                    if ((test.className).indexOf("show-sidebar") > -1) {
                        test.classList.remove("show-sidebar");
                        test.classList.add("hide-sidebar");
                    }
//                    test.className.remove("show-sidebar");
//                    test.classList.add("hide-sidebar");
                }, onPrev: function (tour) {
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    }
                }

            },
            {
                element: ".tour-invoiceTour",
                title: "Payment History(6.a/12)",
                content: "From here you can view your payment history also you can download your payment invoices from here.",
                placement: "top",
            },
            {
                element: ".tour-64",
                title: "Subscription  (6/12)",
                content: "You can unsubscribe or subscribe the package from here.",
                placement: "right",
                onNext: function (tour) {
                    if ((test.className).indexOf("show-sidebar") > -1) {
                        test.classList.remove("show-sidebar");
                        test.classList.add("hide-sidebar");
                    }
//                    test.className.remove("show-sidebar");
//                    test.classList.add("hide-sidebar");
                }, onPrev: function (tour) {
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    }
                }

            },
            {
                element: ".tour-65",
                title: "Subscribe packages (6.a/12)",
                content: "From here you get the details of the package you have subscribe also you can unsubscribe and subscibe for new package from here.",
                placement: "top",
            },
            {
                element: ".tour-79",
                title: "Today's total credit used (7.a/12)",
                content: "From here you can view the todays total credit used and percentage for it with the remaining credit.",
                placement: "top",
            },
            {
                element: ".tour-80",
                title: "Last week total credit used (7.b/12)",
                content: "From here you can view the total credit used in last week.",
                placement: "top",
            },
            {
                element: ".tour-81",
                title: "Last month total credit used (7.c/12)",
                content: "From here you can view the total credit used in last month.",
                placement: "top",
            },
            {
                element: ".tour-82",
                title: "Your mostly run campaign (7.d/12)",
                content: "From here you can view the graphical pie chart for the top five campaign run.",
                placement: "top",
            },
            {
                element: ".tour-83",
                title: "Campaign Information And Statistics (7.e/12)",
                content: "From here you get the information about the todays credit used as per the campaign and the how many times you have run the campaigns.",
                placement: "top",
                onNext: function (tour) {
                    clearPanels();
                    document.getElementById("tourDiv").style.display = 'none';
                }
            },
        ]});
//    tour.init();

}
var test;
var enable;
function firstTimeTour() {
    $('.mobile-menu-toggle').click();
    test = document.getElementById("Ashish");
    if ((test.className).indexOf("page-small") > -1) {
        enable = false;
    } else {
        enable = true;
    }
    if ((test.className).indexOf("hide-sidebar") > -1) {
        test.classList.add("show-sidebar");
    } else {
        test.classList.add("show-sidebar");
    }

    setTimeout(function () {
        tour.restart();
        dashboardTour();
    }, 1000);

    document.getElementById("tourDiv").style.display = 'block';
}

function generateTopServiceTour() {
    var apiname = ['api name', 'DocumentUtility', 'ValueAddedServices', 'InstantWebChat', 'GoogleAuthToken', 'ValueAddedServices'];
    var partnerCount = ['Your Count', 130, 190, 250, 90, 160];
    var otherUserCount = ['Others Count', 90, 230, 170, 70, 140];
    console.log("otherUserCount :: " + JSON.stringify(otherUserCount));
    console.log("partnerCount :: " + partnerCount);
    console.log("apiname :: " + apiname);
    var chart = c3.generate({
        bindto: '#topFiveSerTour',
        size: {
            height: 240,
        },
        data: {
            x: 'api name',
            columns: [
                apiname,
                otherUserCount,
                partnerCount
            ],
            colors: {
                otherUserCount: '#62cb31',
                partnerCount: '#FF7F50'
            },
            types: {
                CallCount: 'bar'
            },
            groups: [
                ['Your Count', 'Others Count']
            ]
        },
        subchart: {
            show: false
        },
        axis: {
            x: {
                type: 'category',
                categories: apiname
            },
            y: {
                padding: {top: 200, bottom: 0}
            },
        }
    });
    document.getElementById("homeReportLable").innerHTML = "Todays top five services count";
    setTimeout(function () {
        generateTopServiceTourV2();
    }, 1000);
}


function generateTopServiceTourV2() {
    var apiname = ['api name', 'DocumentUtility', 'ValueAddedServices', 'InstantWebChat', 'GoogleAuthToken', 'ValueAddedServices'];
    var partnerCount = ['Your Count', 290, 230, 170, 270, 140];
    var otherUserCount = ['Others Count', 130, 190, 250, 290, 160];

    console.log("otherUserCount :: " + JSON.stringify(otherUserCount));
    console.log("partnerCount :: " + partnerCount);
    console.log("apiname :: " + apiname);

    var chart = c3.generate({
        bindto: '#topFiveSerTour',
        size: {
            height: 240,
        },
        data: {
            x: 'api name',
            columns: [
                apiname,
                otherUserCount,
                partnerCount
            ],
            colors: {
                otherUserCount: '#62cb31',
                partnerCount: '#FF7F50'
//                  callCountArr: '#62cb31',
//                  TransactionAmount: '#FF7F50'
            },
            types: {
                //CallCount: 'bar',
                CallCount: 'bar'
            },
            groups: [
                ['Your Count', 'Others Count']
            ]
        },
        subchart: {
            show: false
        },
        axis: {
            x: {
                type: 'category',
                categories: apiname
            },
            y: {
                padding: {top: 200, bottom: 0}
            },
        }
    });
    document.getElementById("homeReportLable").innerHTML = "Todays top five services";
}

