function showPackageChange(packageName, paymentMode, idNum) {
    var l = $('#subscriptionErrorButton' + idNum).ladda();
    l.ladda('start');
    initializeToastr();
    setTimeout(function () {
        toastr.error('Error - ' + 'First pay the previous package bill');
        l.ladda('stop');
    }, 3000);
    setTimeout(function () {
        var s = './tminvoice.jsp?_packageName=' + packageName + '&_paymentMode=' + paymentMode;
        window.location.href = s;
    }, 5000);
    return;
}

function unSubscribePackage(packageId) {
    var l = $('#unSubscripbePackage').ladda();
    l.ladda('start');
    initializeToastr();
    var s = './UnSubscribePackage?packageId=' + packageId;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                toastr.error('Error - ' + data._message);
                l.ladda('stop');
            } else if (strCompare(data._result, "success") === 0) {
                toastr.success('Success - ' + data._message);
                l.ladda('stop');
                setTimeout(function () {
//                   window.location="./ala-carte-package.jsp";
                    getPackageList();
                }, 3000);
            }
        }
    });
}

function getPackageList() {
    var s = "packageListToSubscribe.jsp";
    //var s="unsubscribePackage.jsp";
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
        },
        error: function (data) {
        },
        complete: function () {
            document.getElementById("wrapper3").style.display = "none";
            $('#wrapper2').show();
        }
    });
}
function manualPaymentEnable(idNum) {
    var l = $('#manualPaymentErrorButton' + idNum).ladda();
    l.ladda('start');
    initializeToastr();
    setTimeout(function () {
        toastr.error('Error - ' + 'Previous Package bill send on your email, Please pay it first manually at TM Point');
        l.ladda('stop');
    }, 3000);
    return;
}
function slabPriceDiv(value) {
    if (value === 'Enable') {
        document.getElementById('apSlabrate').style.display = 'block';
    } else {
        document.getElementById('apSlabrate').style.display = 'none';
    }
}

function loanPriceDiv(value) {
    if (value === 'Enable') {
        document.getElementById('loanDiv').style.display = 'block';
    } else {
        document.getElementById('loanDiv').style.display = 'none';
    }
}

function creditPointDiv(value) {
    if (value === 'Enable') {
        document.getElementById('creditDiv').style.display = 'block';
    } else {
        document.getElementById('creditDiv').style.display = 'none';
    }
}

function payLoanFirst(packageID) {
    showAlert1("First pay the borrowed loan", "danger", 3000);
    var s = './loaninvoiceDetail.jsp?_packageID=' + packageID;
    setInterval(function () {
        window.location = s;
    }, 3000);
}

function accesspointChangeAPIPrice(value) {
    var s = './accessPointWiseAPI.jsp?_apId=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_APIForSlabPricing2').html(data);
        }
    });
}

function acesspointChangeAPI(value) {
    var s = './accessPointChangeAPI.jsp?_apId=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_APIForSlabPricing2').html(data);
            showSlabPricing(value);
        }
    });
}

function accesspointChangePrice(value, versionData, _resourceId, packageId) {
    var s = './accessPointChangeBilling.jsp?_apId=' + value + '&versionData=' + versionData + '&_resourceId=' + _resourceId + '&_bucketId=' + packageId;
    if (value != -2) {
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                $('#listOfAPI').html(data);
            }
        });
    } else {
        $('#addPackageTab a[href="#apRate"]').tab('show');
    }
}

function showSlabPricing(apName, _resourceName, versionData, apiName, packageName) {
    var s = './getSlabPriceWindow.jsp?apName=' + apName + '&_resourceName=' + _resourceName + '&versionData=' + versionData + '&apiName=' + apiName + '&packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_slabPriceWindow').html(data);
            // acesspointChangeAPIPrice(value);
        }
    });
}

function showSlabAPI(versionData, ap, _resourceId, packageName) {
    var s = './showReqAPI.jsp?versionData=' + versionData + '&_apName=' + ap + '&_resourceName=' + _resourceId + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_APIForSlabPricing2').html(data);
            var apiName = document.getElementById('_APIForSlabPricing2').value;
            // showSlabAPI(versionData,ap);
            showSlabPricing(ap, _resourceId, versionData, apiName, packageName);
        }
    });
}

function showSlabVersion(value, ap, packageName) {
    var s = './getSlabVersion.jsp?_resourceName=' + value + '&_apname=' + ap + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_VersionForSlabPricing2').html(data);
            var versionData = document.getElementById('_VersionForSlabPricing2').value;
            showSlabAPI(versionData, ap, value, packageName);
        }
    });
}

function acesspointChangeResource(value, packageName) {
    var s = './getSlabResource.jsp?_apName=' + value + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_ResourceForSlabPricing2').html(data);
            var res = document.getElementById('_ResourceForSlabPricing2').value;
            //alert(res+"res");
            var ap = document.getElementById('_Accesspoint2').value;
            //alert(ap+"ap");
            showSlabVersion(res, ap, packageName);
        }
    });
}

function showAPAPI(versionData, ap, _resourceId, packageId) {
    var s = './slabAPI.jsp?versionData=' + versionData + '&_apId=' + ap + '&_resourceId=' + _resourceId;
    accesspointChangePrice(ap, versionData, _resourceId, packageId);
}

function accesspointPrice(packageName) {
    var _apname = document.getElementById('_Accesspoint12').value;
    var versionData = document.getElementById('_VersionForAPricing2').value;
    var _resourceId = document.getElementById('_ResourceForAPPricing2').value;
    var s = './accessPointPriceBilling.jsp?_apname=' + _apname + '&versionData=' + versionData + '&_resourceName=' + _resourceId + '&_packageName=' + packageName;

    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#listOfAPI').html(data);
        }
    });

}

function showAPVersion(value, packageName) {
    var ap = document.getElementById('_Accesspoint12').value;
    var s = './showReqVersion.jsp?_resourceName=' + value + '&_apname=' + ap + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_VersionForAPricing2').html(data);
            var versionData = document.getElementById('_VersionForAPricing2').value;
            //showAPAPI(versionData,ap,value,packageId);
            accesspointPrice(packageName);
        }
    });
}

function acesspointResource(value, packageName) {
    var s = './showReqResource.jsp?_apName=' + value + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_ResourceForAPPricing2').html(data);
            var res = document.getElementById('_ResourceForAPPricing2').value;
            //alert(res+"res");
            var ap = document.getElementById('_Accesspoint12').value;
            //alert(ap+"ap");
            showAPVersion(res, packageName);
        }
    });
}

function showAlert1(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        $("body")
                .append($('<div id="alerts-container" style="position: fixed;' +
                        'width: 50%; left: 25%; top: 10%;">'));
    }
    type = type || "info";
    // create the alert div
    message = '<i class="fa fa-info-circle"></i> ' + message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);
    $("#alerts-container").prepend(alert);
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);
}

function acessChangeResForTiering(accesspoint, packageName) {
    var value = document.getElementById("_Accesspoint3").value;
    var s = './tieringResource.jsp?_apName=' + value + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_ResourceForTieringPricing2').html(data);
            var res = document.getElementById('_ResourceForTieringPricing2').value;
            var ap = document.getElementById('_Accesspoint3').value;
            showTieringVersion(res, packageName);
        }
    });
}

function showTieringVersion(value, packageName) {
    var ap = document.getElementById("_Accesspoint3").value;
    var s = './tieringVersion.jsp?_resourceId=' + value + '&_apId=' + ap;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_VersionForTieringPricing2').html(data);
            var versionData = document.getElementById('_VersionForTieringPricing2').value;
            showTieringAPI(versionData, ap, value, packageName);
        }
    });
}

function showTieringAPI(versionData, ap, value, packageName) {
    var ap = document.getElementById("_Accesspoint3").value;
    var _resourceId = document.getElementById("_ResourceForTieringPricing2").value;
    var packageName = document.getElementById("packageName").value;
    var s = './tieringAPI.jsp?versionData=' + versionData + '&_apId=' + ap + '&_resourceId=' + _resourceId + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_APIForTieringPricing2').html(data);
            var apiName = document.getElementById('_APIForTieringPricing2').value;
            // showSlabAPI(versionData,ap);
            showTieringPricing(apiName);
        }
    });
}

function showTieringPricing(value) {

    var accesspointName = document.getElementById("_Accesspoint3").value;
    var resourceName = document.getElementById("_ResourceForTieringPricing2").value;
    var version = document.getElementById("_VersionForTieringPricing2").value;
    var packageName = document.getElementById("packageName").value;

    var s = './showTieringPriceWindow.jsp?_apId=' + accesspointName + '&_resourceId=' + resourceName + '&version=' + version + '&apiName=' + value + '&packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_tieringPriceWindow').html(data);

        }
    });
}

//function showSubscribeTieringPricing(value) {
//
//    var accesspointName = document.getElementById("_Accesspoint3").value;
//    var resourceName = document.getElementById("_ResourceForTieringPricing2").value;
//    var version = document.getElementById("_VersionForTieringPricing2").value;
//    var packageId = document.getElementById("packageID").value;
//
//    var s = './showSubscribeTieringPriceWindow.jsp?_apId=' + accesspointName + '&_resourceId=' + resourceName + '&version=' + version + '&apiName=' + value + '&packageId=' + packageId;
//    $.ajax({
//        type: 'GET',
//        url: s,
//        success: function (data) {
//            $('#_tieringPriceWindow').html(data);
//
//        }
//    });
//}

function tieringPriceDiv(value) {
    if (value === 'Enable') {
        document.getElementById('apTieringrate').style.display = 'block';
    } else {
        document.getElementById('apTieringrate').style.display = 'none';
    }
}

function throttlingResource(value, packageName) {
    var s = './showRequestedThrottlingResource.jsp?_apName=' + value + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_resourceAPIThrottling').html(data);
            var res = document.getElementById('_resourceAPIThrottling').value;

            throttlingVersion(res, packageName);
        }
    });
}

function throttlingVersion(value, packageName) {
    var ap = document.getElementById('_throttlingAccesspoint').value;
    var s = './showRequestedThrottlingVersion.jsp?_resourceName=' + value + '&_apname=' + ap + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_versionofAPIThrottling').html(data);
            throttlingAPIDetails(packageName);
        }
    });
}

function throttlingAPIDetails(packageName) {
    var _apname = document.getElementById('_throttlingAccesspoint').value;
    var versionData = document.getElementById('_versionofAPIThrottling').value;
    var _resourceId = document.getElementById('_resourceAPIThrottling').value;
    var s = './showAPIThrottlingDetails.jsp?_apname=' + _apname + '&versionData=' + versionData + '&_resourceName=' + _resourceId + '&_packageName=' + packageName;

    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#listOfthrottlingAPI').html(data);
        }
    });
}

function subscribeThrottlingResource(value, packageName) {
    var s = './showSubscribeThrottlingResource.jsp?_apName=' + value + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_resourceAPIThrottling').html(data);
            var res = document.getElementById('_resourceAPIThrottling').value;
            subscribeThrottlingVersion(res, packageName);
        }
    });
}

function subscribeThrottlingVersion(value, packageName) {
    var ap = document.getElementById('_throttlingAccesspoint').value;
    var s = './showSubscribeThrottlingVersion.jsp?_resourceName=' + value + '&_apname=' + ap + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_versionofAPIThrottling').html(data);
            subscribeThrottlingAPIDetails(packageName);
        }
    });
}

function subscribeThrottlingAPIDetails(packageName) {
    var _apname = document.getElementById('_throttlingAccesspoint').value;
    var versionData = document.getElementById('_versionofAPIThrottling').value;
    var _resourceId = document.getElementById('_resourceAPIThrottling').value;
    var s = './showSubscribeAPIThrottlingDetails.jsp?_apname=' + _apname + '&versionData=' + versionData + '&_resourceName=' + _resourceId + '&_packageName=' + packageName;

    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#listOfthrottlingAPI').html(data);
        }
    });
}

function subscribedAcesspointResource(value, packageName) {
    var s = './showSubscribedResource.jsp?_apName=' + value + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_ResourceForAPPricing2').html(data);
            var res = document.getElementById('_ResourceForAPPricing2').value;
            var ap = document.getElementById('_Accesspoint12').value;
            subscribedShowAPVersion(res, packageName);
        }
    });
}
function subscribedShowAPVersion(value, packageName) {
    var ap = document.getElementById('_Accesspoint12').value;
    var s = './showSubscribedVersion.jsp?_resourceName=' + value + '&_apname=' + ap + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_VersionForAPricing2').html(data);
            var versionData = document.getElementById('_VersionForAPricing2').value;
            //showAPAPI(versionData,ap,value,packageId);
            subscribedAccesspointPrice(packageName);
        }
    });
}
function subscribedAccesspointPrice(packageName) {
    var _apname = document.getElementById('_Accesspoint12').value;
    var versionData = document.getElementById('_VersionForAPricing2').value;
    var _resourceId = document.getElementById('_ResourceForAPPricing2').value;
    var s = './subscribedAccessPointPrice.jsp?_apname=' + _apname + '&versionData=' + versionData + '&_resourceName=' + _resourceId + '&_packageName=' + packageName;

    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#listOfAPI').html(data);
        }
    });
}


function flatPriceResource(value, packageName) {
    var s = './showRequestedFlatPriceResource.jsp?_apName=' + value + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_resourceFlatPrice').html(data);
            var res = document.getElementById('_resourceFlatPrice').value;

            flatVersion(res, packageName);
        }
    });
}

function flatVersion(value, packageName) {
    var ap = document.getElementById('_accesspointFlatPrice').value;
    var s = './showRequestedFlatVersion.jsp?_resourceName=' + value + '&_apname=' + ap + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_versionFlatPrice').html(data);
            flatPriceDetails(packageName);
        }
    });
}

function flatPriceDetails(packageName) {
    var _apname = document.getElementById('_accesspointFlatPrice').value;
    var versionData = document.getElementById('_versionFlatPrice').value;
    var _resourceId = document.getElementById('_resourceFlatPrice').value;
    var s = './showFlatPriceDetails.jsp?_apname=' + _apname + '&versionData=' + versionData + '&_resourceName=' + _resourceId + '&_packageName=' + packageName;

    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#listflatPriceDetails').html(data);
        }
    });
}

function subscribeFlatPriceResource(value, packageName) {
    var s = './showSubscribeFlatPriceResource.jsp?_apName=' + value + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_resourceFlatPrice').html(data);
            var res = document.getElementById('_resourceFlatPrice').value;

            subscribeFlatVersion(res, packageName);
        }
    });
}

function subscribeFlatVersion(value, packageName) {
    var ap = document.getElementById('_accesspointFlatPrice').value;
    var s = './showSubscribeFlatVersion.jsp?_resourceName=' + value + '&_apname=' + ap + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_versionFlatPrice').html(data);
            subscribeFlatPriceDetails(packageName);
        }
    });
}

function subscribeFlatPriceDetails(packageName) {
    var _apname = document.getElementById('_accesspointFlatPrice').value;
    var versionData = document.getElementById('_versionFlatPrice').value;
    var _resourceId = document.getElementById('_resourceFlatPrice').value;
    var s = './showSubscribeFlatPriceDetails.jsp?_apname=' + _apname + '&versionData=' + versionData + '&_resourceName=' + _resourceId + '&_packageName=' + packageName;

    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#listflatPriceDetails').html(data);
        }
    });
}

function subscribeSlabResource(value, packageName) {
    var s = './showSubscribeSlabResource.jsp?_apName=' + value + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_ResourceForSlabPricing2').html(data);
            var res = document.getElementById('_ResourceForSlabPricing2').value;
            var ap = document.getElementById('_Accesspoint2').value;
            subscribeSlabVersion(res, ap, packageName);
        }
    });
}
function subscribeSlabVersion(value, ap, packageName) {
    var s = './showSubscribeSlabVersion.jsp?_resourceName=' + value + '&_apname=' + ap + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_VersionForSlabPricing2').html(data);
            var versionData = document.getElementById('_VersionForSlabPricing2').value;
            showSubscribeSlabAPI(versionData, ap, value, packageName);
        }
    });
}
function showSubscribeSlabAPI(versionData, ap, _resourceId, packageName) {
    var s = './showSubscribeSlabAPI.jsp?versionData=' + versionData + '&_apName=' + ap + '&_resourceName=' + _resourceId + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_APIForSlabPricing2').html(data);
            var apiName = document.getElementById('_APIForSlabPricing2').value;
            showSubscribeSlabPricing(ap, _resourceId, versionData, apiName, packageName);
        }
    });
}
function showSubscribeSlabPricing(apName, _resourceName, versionData, apiName, packageName) {
    var s = './showSubscribeSlabPriceWindow.jsp?apName=' + apName + '&_resourceName=' + _resourceName + '&versionData=' + versionData + '&apiName=' + apiName + '&packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_slabPriceWindow').html(data);
            // acesspointChangeAPIPrice(value);
        }
    });
}



// subscribed tier price details
function subscribeResourceTiering() {
    var value = document.getElementById("_Accesspoint3").value;
    var s = './subscribeTieringResource.jsp?_apName=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_ResourceForTieringPricing2').html(data);
            var res = document.getElementById('_ResourceForTieringPricing2').value;
            var ap = document.getElementById('_Accesspoint3').value;
            showSubscribeTieringVersion(res);
        }
    });
}

function showSubscribeTieringVersion(value) {
    var ap = document.getElementById("_Accesspoint3").value;
    var s = './subscribeTieringVersion.jsp?_resourceId=' + value + '&_apId=' + ap;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_VersionForTieringPricing2').html(data);
            var versionData = document.getElementById('_VersionForTieringPricing2').value;
            showSubscriTieringAPI(versionData, ap, value);
        }
    });
}

function showSubscriTieringAPI(versionData) {
    var ap = document.getElementById("_Accesspoint3").value;
    var _resourceId = document.getElementById("_ResourceForTieringPricing2").value;
    var s = './subscribeTieringAPI.jsp?versionData=' + versionData + '&_apId=' + ap + '&_resourceId=' + _resourceId;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_APIForTieringPricing2').html(data);
            var apiName = document.getElementById('_APIForTieringPricing2').value;
            showSubscribeTieringPricing(apiName);
        }
    });
}

function showSubscribeTieringPricing(value) {
    var accesspointName = document.getElementById("_Accesspoint3").value;
    var resourceName = document.getElementById("_ResourceForTieringPricing2").value;
    var version = document.getElementById("_VersionForTieringPricing2").value;

    var s = './showSubscribeTieringPriceWindow.jsp?_apId=' + accesspointName + '&_resourceId=' + resourceName + '&version=' + version + '&apiName=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_tieringPriceWindow').html(data);

        }
    });
}


function confirmUnSubscribePackage(packageId) {
    swal({
        title: "Are you sure?",
        text: "You will not able to do campaign after finish the credit! \n \n Note: \n\n1) You need subscription with same or other package to continue usage for campaigns. \n\n 2) Your remaining credit will be added in your next subscription in case subscribe with new package. \n\n 3) In case you subscribe with other package your expiry apply as \nper new subscription .",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true},
            function (isConfirm) {
                if (isConfirm) {
                    unSubscribePackage(packageId);
                } else {
                    swal("Cancelled", "Your request has been cancelled)", "error");
                }
            });
}