/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Alert4Users(msg) {
    bootboxmodel("<h5>" + msg + "</h5>");
}
function downloadDocments(id) {
    var s = './downloadDoc?id=' + id;
    //alert("id from js "+id);
    window.location.href = s;
    return false;
}
function uploadCertificate() {
    $('#buttonUploadCert').attr("disabled", true);
    var s = './UploadKeyOrCert?type=test';
    $.ajaxFileUpload({
        fileElementId: 'certificatetoupload',
        url: s,
        dataType: 'json',
        success: function (data, status) {
            //alert(data);
            if (strcmpserviceguard(data.result, "error") == 0) {
                Alert4serviceguard("<span><font color=red>" + data.message + "</font></span>");
                $('#buttonUploadCert').attr("disabled", false);
            } else if (strcmpserviceguard(data.result, "success") == 0) {
                Alert4serviceguard("<span><font color=blue>" + data.message + "</font></span>");
                $('#buttonUploadCert').attr("disabled", false);
            }
        },
    });
}
function downloadCert(id, env) {
    if (env === 'test') {
        var l = $('#downTcert').ladda();
        l.ladda('start');
    } else {
        var l = $('#downPcert').ladda();
        l.ladda('start');
    }
    initializeToastr();
    var s = './DownloadCert?pid=' + id + "&env=" + env;
    window.location.href = s;
    l.ladda('stop');
//    $('#dowModal').modal('hide');
    return false;
}

function addNewCampaign(){
    if ((test.className).indexOf("page-small") > -1) {
        test.classList.remove("show-sidebar");
        test.classList.add("hide-sidebar");
    }
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = './createCampaignV2.jsp';
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
        },
        complete: function () {
            document.getElementById("wrapper3").style.display = "none";
            $('#wrapper2').show();
        }

    });
}
function editCampaign(campaignId){
    if ((test.className).indexOf("page-small") > -1) {
        test.classList.remove("show-sidebar");
        test.classList.add("hide-sidebar");
    }
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = './createCampaign.jsp?campaignId='+campaignId;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
        },
        complete: function () {
            document.getElementById("wrapper3").style.display = "none";
            $('#wrapper2').show();
        }

    });
}

function editSocialSetting(socialMediaId){
    if ((test.className).indexOf("page-small") > -1) {
        test.classList.remove("show-sidebar");
        test.classList.add("hide-sidebar");
    }
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = './createSocialSetting.jsp?socialSetting='+socialMediaId;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
        },
        complete: function () {
            document.getElementById("wrapper3").style.display = "none";
            $('#wrapper2').show();
        }

    });
}

function addcert() {
    var env = document.getElementById("selectPEnv").value;
    var sid = $("#_partnerid").val();
    var s = './addCertificate?_sid=' + encodeURIComponent(sid) + "&env=" + env;
    $.ajax({
        fileElementId: 'addcertificate',
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#uploadCertFileForm").serialize(),
        success: function (data) {
            if (strcmpserviceguard(data._result, "error") == 0) {
                $('#upload-partner-certificate').html("<span><font color=red>" + data._message + "</font></span></small>");
            } else if (strcmpserviceguard(data._result, "success") == 0) {
                $('#upload-partner-certificate').html("<span><font color=blue>" + data._message + "</font></span></small>");
                $('#addPartnerButton').attr("disabled", false);
                window.setTimeout(RefreshpartnerList(), 5000);
            }
        }
    });
}
function RefreshpartnerList() {
    window.location.href = "./profile.jsp";
}
function loadCertificate(_pid) {
//   _orpid= encodeURIComponent(_orpid);
    $('#envModal').modal('hide');
    $('#_partnerid').val(_pid);
//    alert(_pid);
    //alert($('#merchantId').val());
    $("#uploadCertificate").modal();
}
function bootboxmodel(content) {
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>' +
            content +
            '</div></div></div></div>');
    popup.modal();
}
var waiting;
function pleasewait() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    waiting.modal();
}

function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        // alerts-container does not exist, create it
        $("body")
                .append($('<div id="alerts-container" style="position: fixed;' +
                        'width: 50%; left: 25%; top: 10%;">'));
    }
    // default to alert-info; other options include success, warning, danger
    type = type || "info";
    // create the alert div
    message = '<i class="fa fa-info-circle"></i> ' + message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);
    // add the alert div to top of alerts-container, use append() to add to bottom
    $("#alerts-container").prepend(alert);
    // if closeDelay was passed - set a timeout to close the alert
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);
}

function updateProfile() {
    var s = './EditProfile';
    var emailID = document.getElementById("pemail").value;
    var contactNo = document.getElementById("pmobNo").value;
    var ipTest = document.getElementById("testIpDe").value;
    var ipLive = document.getElementById("liveIpDe").value;
    if (ipTest.trim() === '' || ipLive.trim() === '') {
        //Alert4Users("<font color=red>"+ "IP's should not be empty."+ "</font>");
        showAlert("IP should not be empty.", "danger", 3000);
        return;
    }
    if (emailID.trim() === '' || contactNo.trim() === '') {
        //Alert4Users("<font color=red>"+ "EmailId or Contact number should not be empty."+ "</font>");
        showAlert("EmailId or Contact number should not be empty.", "danger", 3000);
        return;
    }
    pleasewait();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#update-profile").serialize(),
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                waiting.modal('hide');
                //Alert4Users("<font color=red>"+data._message+"</font>");
                showAlert(data._message, "danger", 3000);
                setTimeout(function () {
                    window.location.href = "./profile.jsp";
                }, 3000);
            } else if (strCompare(data._result, "success") === 0) {
                waiting.modal('hide');
                //Alert4Users("<font color=blue>"+data._message+"</font>");
                showAlert(data._message, "success", 3000);
                setTimeout(function () {
                    window.location.href = "./profile.jsp";
                }, 3000);
            }
        }
    });
}
function cancelProfile() {
    window.setTimeout(function () {
        window.location.href = "home.jsp";
    }, 3000);
}
function strcmpserviceguard(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function viewCrtData(id, env) {
    if (env === 'test') {
        var l = $('#viewTestcert').ladda();
        l.ladda('start');
    } else {
        var l = $('#viewProdcert').ladda();
        l.ladda('start');
    }
    initializeToastr();
    var s = './ViewCertData?pid=' + id + "&env=" + env;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpserviceguard(data._result, "error") === 0) {
                //Alert4Users("<font color=red>" + data._message + "</font>") 
                l.ladda('stop');
//                showAlert(data._message,"danger",3000);
                toastr.error('Error - ' + data._message);
            } else if (strcmpserviceguard(data._result, "success") === 0) {

                $('#live_cert').modal();
                if (env === 'test') {
                    document.getElementById('userDetails').innerHTML = 'Test Environment';
                } else {
                    document.getElementById('userDetails').innerHTML = 'Live Enviornment';
                }
//                $('#_pName').html(data.name);
                $('#_srNo').val(data._srno);
                $('#_algo').val(data._algoname);
                $('#_issuerdn').val(JSON.stringify(data._issuerdn));
                $('#_subjectdn').val(JSON.stringify(data._subjectdn));
                $('#_notafter').val(data._notafter);
                $('#_notbefore').val(data._notbefore);
                $('#_version').val(data._version);
                l.ladda('stop');
            }
        }
    });

}
function showTokenForPartner(msg, msg1) {
    var msgToken = "<p>" + msg + "</p>";
    var type = "Token For Test enviroment is";
    var tokenType = "<h4>" + type + "<h4>";
    document.getElementById('tokenType').innerHTML = tokenType;
    document.getElementById('test1').innerHTML = msgToken;
    $('#tmTermsAndCondition').modal();
}
function showLiveTokenForPartner(msg, msg1) {
    var msgToken = "<p><b>" + msg1 + "</b></p>";
    var type = "Token For Live enviroment is";
    var tokenType = "<h4>" + type + "<h4>";
    //document.getElementById('tokenType').innerHTML = tokenType;
    document.getElementById('test1').innerHTML = msgToken;
    $('#tmTermsAndCondition').modal();
}
function showPartnerId(PartnerId) {
    var msgToken = "<p><b>" + PartnerId + "</b></p>";
    var type = "Token For Live enviroment is";
    var tokenType = "<h4>" + type + "<h4>";
    //document.getElementById('tokenType').innerHTML = tokenType;
    document.getElementById('test12').innerHTML = msgToken;
    $('#showPartnerIdModal').modal();
}

function reGenerateAPToken(partnerId, partnerName, partnerEmail, partnerPhone, env) {
//    var env = "Test";
//    $('#dowModal').modal('hide');
    if (env === 'Test') {
        var l = $('#regTestToken').ladda();
        l.ladda('start');
    } else {
        var l = $('#regProdToken').ladda();
        l.ladda('start');
    }
    var s = './ReAssignAPToken?_partnerid=' + partnerId + '&env=' + env + '&_partnername=' + partnerName + '&_partneremail=' + partnerEmail + '&_partnerphone=' + partnerPhone;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            initializeToastr();
            var msg = "";
            if (strCompare(data._result, "error") === 0) {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.error('Error - ' + 'Token regenerated for ' + env + ' environment failed.');
                }, 3000);
                setTimeout(function () {
                    window.location.href = "./token_manager.jsp";
                }, 5000);
            } else if (strCompare(data._result, "success") === 0) {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.success('success - ' + 'Token regenerated for ' + env + ' environment successfully.');
                }, 3000);
                setTimeout(function () {
                    window.location.href = "./token_manager.jsp";
                }, 5000);
            }
        }
    });
}

function emailAPToken(partnerName, partnerEmail, testToken, productionToken, env) {
    if (env === 'Test') {
        var l = $('#sendTestToken').ladda();
        l.ladda('start');
    } else {
        var l = $('#sendProdToken').ladda();
        l.ladda('start');
    }
    var apToken = testToken;
    if (env !== 'Test') {
        apToken = productionToken;
    }
    var s = './EmailAPToken?partnerName=' + partnerName + '&partnerEmail=' + partnerEmail + '&apToken=' + apToken + '&env=' + env;
    initializeToastr();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                l.ladda('stop');
                toastr.error('Error - ' + 'Token send for ' + env + ' environment failed.');
            } else if (strCompare(data.result, "success") === 0) {
                toastr.success('success - ' + 'Token send for ' + env + ' environment successfully.');
                l.ladda('stop');
            }
        }
    });
}

function assignToken(accesspoint_Name) {
    var accesspointName = accesspoint_Name;
    var resourceName = document.getElementById("apId").value;
    var version = document.getElementById("versionName").value;
    //var api = document.getElementById("_APIForSlabPricing2").value;
    //var packageID = document.getElementById("packageID").value;
    var s = './assignToken.jsp?_apId=' + accesspointName + '&_resourceId=' + resourceName + '&version=' + version;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_slabPriceWindow').html(data);
            // acesspointChangeAPIPrice(value);
        }
    });
}

function updatePartnerName(partnerId) {
    var companyName = document.getElementById("companyName").value;
    var companyRegNo = document.getElementById("companyRegNo").value;
    var companyGstNo = document.getElementById("companyGstNo").value;
    var l = $('#partUpdate').ladda();
    $("#personalInfo").validate({
        rules: {
            partnerName: {
                required: true
            },
            companyFixedNo: {
                required: true
            },
            companyAddress: {
                required: true
            },
            companyState: {
                required: true
            },
            compnyPcode: {
                required: true
            },
            companyCountry: {
                required: true
            },
            bilingAdd: {
                required: true
            },
            billingState: {
                required: true
            },
            bilingPcode: {
                required: true
            },
            bilingCountry: {
                required: true
            }
        },
        submitHandler: function (form) {
            l.ladda('start');
            initializeToastr();
            var partnerName = document.getElementById("partnerName").value;
            var s = './EditPartner?partnerId=' + partnerId + '&companyName=' + companyName + '&companyRegNo=' + companyRegNo + '&companyGstNo=' + companyGstNo;
            $.ajax({
                type: 'POST',
                url: s,
                data: $("#personalInfo").serialize(),
                dataType: 'json',
                success: function (data) {
                    if (strCompare(data._result, "error") === 0) {
                        toastr.error('Error - ' + data._message);
                        l.ladda('stop');
                    } else {
                        toastr.success('Success - ' + data._message);
                        l.ladda('stop');
//                    window.location.href = "./my_profile.jsp";
                        setTimeout(function () {
                            window.location.href = "./my_profile.jsp";
                        }, 3000);
                    }
                }
            });
        }
    });
}
 function validateSocilaForm(){
     updateSocialmedia();
 }
 function validateSocilaFormV2(socialMediaId){
     updateSocialmediaV2(socialMediaId);
 }
 
 function updateSocialmedia() {
    var l = $('#partUpdate').ladda();    
    l.ladda('start');
    initializeToastr();
    var s = './CreateSocialSetting';
    $.ajax({
        type: 'POST',
        url: s,
        data: $("#socialinfo").serialize(),
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                toastr.error('Error - ' + data._message);
                l.ladda('stop');
            } else {
                toastr.success('Success - ' + data._message);
                l.ladda('stop');
//                setTimeout(function () {
//                    window.location.href = "./header.jsp";
//                },5000);
            }
        }
    });
}

 function updateSocialmediaV2(socialMediaId) {
    var l = $('#partUpdate').ladda();    
    l.ladda('start');
    initializeToastr();
    var s = './UpdateSocialSetting?socialMediaId='+socialMediaId;
    $.ajax({
        type: 'POST',
        url: s,
        data: $("#socialinfo").serialize(),
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                toastr.error('Error - ' + data._message);
                l.ladda('stop');
            } else {
                toastr.success('Success - ' + data._message);
                l.ladda('stop');                
            }
        }
    });
}

function validateProfileForm() {
    $("#personalInfo").validate({
        rules: {
            campaignTitle: {
                required: true
            },
            couponCode: {
                required: true
            },
            rewardPoint: {
                required: true
            },
            noOfUser: {
                required: true
            },
            campaignPhrase: {
                required: true
            },
            message:{
                required: true
            },
            promotionURL:{
                required: true
            }
        },
        submitHandler: function (form) {
            updateCampaign();
        }
    });
}

function validateProfileFormV2(campaignId) {
    $("#personalInfo").validate({
        rules: {
            campaignTitle: {
                required: true
            },
            couponCode: {
                required: true
            },
            rewardPoint: {
                required: true
            },
            noOfUser: {
                required: true
            },
            campaignPhrase: {
                required: true
            },
            message:{
                required: true
            },
            promotionURL:{
                required: true
            }
        },
        submitHandler: function (form) {
            updateCampaignV2(campaignId);
        }
    });
}

function validateOwnerDetailsForm() {
    $("#personalInfo").validate({
        rules: {
            cmpname: {
                required: true
            },
            addr1: {
                required: true
            },
            addr2: {
                required: true
            },
            addr3: {
                required: true
            },
            cmpstate: {
                required: true
            },
            city:{
                required: true
            },
            landline_number:{
                required: true
            },
            country:{
                required: true
            },
            pinCode:{
                required: true
            }
        },
        submitHandler: function (form) {
            updateOwnerProfileDetail();
        }
    });
}

function updateOwnerProfileDetail() {
    var l = $('#partUpdate').ladda();    
    l.ladda('start');
    initializeToastr();
    var s = './UploadBrandDetails';
    $.ajax({
        type: 'POST',
        url: s,
        data: $("#personalInfo").serialize(),
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                toastr.error('Error - ' + data._message);
                l.ladda('stop');
            } else {
                toastr.success('Success - ' + data._message);
                l.ladda('stop');
                setTimeout(function () {
                    window.location.href = "./header.jsp";
                }, 3000);
            }
        }
    });
}

function callValidateOwnerDetailsForm() {
    document.querySelector('.validateOwnerForm').click();
}

function callValidateProfileForm() {
    document.querySelector('.validateProfileForm').click();
}

function callValidateProfileFormV2() {
    document.querySelector('.validateProfileForm').click();
}
function callValidateSocialMediaForm() {
    document.querySelector('.validateSocialMediaForm').click();
}

function callValidateSocialMediaFormV2() {
    document.querySelector('.validateSocialMediaForm').click();
}
function updateCampaignV2(campaignId) {
    var l = $('#partUpdate').ladda();    
    l.ladda('start');
    initializeToastr();
    var s = './UpdateCampaign?campaignId='+campaignId;
    $.ajax({
        type: 'POST',
        url: s,
        data: $("#personalInfo").serialize(),
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                toastr.error('Error - ' + data._message);
                l.ladda('stop');
            } else {
                toastr.success('Success - ' + data._message);
                l.ladda('stop');
//                setTimeout(function () {
//                    window.location.href = "./header.jsp";
//                }, 3000);
            }
        }
    });
}

function updateCampaign() {
    var l = $('#partUpdate').ladda();    
    l.ladda('start');
    initializeToastr();
    var s = './CreateCampaign';
    $.ajax({
        type: 'POST',
        url: s,
        data: $("#personalInfo").serialize(),
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                toastr.error('Error - ' + data._message);
                l.ladda('stop');
            } else {
                toastr.success('Success - ' + data._message);
                l.ladda('stop');
//                setTimeout(function () {
//                    window.location.href = "./header.jsp";
//                }, 3000);
            }
        }
    });
}
function getCreditStatus() {
    var s = './GetCreditV2';
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',        
        success: function (data) {
            console.log("data.credits >>> "+data.credits);
            if (data.credits !== 'NA') {
                $('#mainCreditOfDev').html(data.credits);
            }                        
        }
    });
}

function startCampaign(campaignUniqueId,id) {
    var l = $('#startCampaign'+id).ladda();    
    l.ladda('start');
    initializeToastr();
    var s = './StartCampaign?campaignUniqueId='+encodeURIComponent(campaignUniqueId);
    $.ajax({
        type: 'POST',
        url: s,
//        data: $("#personalInfo").serialize(),
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                toastr.error('Error - ' + data._message);
                l.ladda('stop');
            } else {
                toastr.success('Success - ' + data._message);
                l.ladda('stop');
                getCreditStatus();
                setTimeout(function () {
                    window.location.href = "./header.jsp";
                }, 3000);
            }
        }
    });
}

function startCampaignWithExeName(){
    var l = $('#startCampaignButton').ladda();    
    l.ladda('start');
    initializeToastr();
    var campaignUniqueId = document.getElementById("campaignUniqueID").value;
    var campaignExeName = document.getElementById("campaignExecutionName").value;
    var s = './StartCampaign?campaignUniqueId='+encodeURIComponent(campaignUniqueId)+'&campaignExeName='+campaignExeName;
    $.ajax({
        type: 'POST',
        url: s,
//        data: $("#personalInfo").serialize(),
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                toastr.error('Error - ' + data._message);
                l.ladda('stop');
            } else {
                toastr.success('Success - ' + data._message);
                l.ladda('stop');
                getCreditStatus();
                $('#campaignExecutionModal').modal('hide');
//                setTimeout(function () {
//                    window.location.href = "./header.jsp";
//                }, 3000);
            }
        }
    });
}
function stopCampign(campaignUniqueId,id){
    var l = $('#stopCampaign'+id).ladda();    
    l.ladda('start');
    initializeToastr();
    var s = './StopCampaign?campaignUniqueId='+encodeURIComponent(campaignUniqueId);
    $.ajax({
        type: 'POST',
        url: s,
//        data: $("#personalInfo").serialize(),
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                toastr.error('Error - ' + data._message);
                l.ladda('stop');
            } else {
                toastr.success('Success - ' + data._message);
                l.ladda('stop');
                getCreditStatus();
//                setTimeout(function () {
//                    window.location.href = "./header.jsp";
//                }, 3000);
            }
        }
    });
}
function removeImageFromSession(file){
    var s = './RemoveUploadProfileImageFromSession?file='+file;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                
            } else if (strCompare(data._result, "success") === 0) {
              
            }
        }
    });
}

function checkTwitterAvailability() {
    var value = $("#twitterHandlerId").val();
    var s = './CheckTwitterHandlerAvailability?twitterHandlerId=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strCompare(data.result, "error") == 0) {
                $('#checkAvailability-result').html("<span><font color=red>" + data.message + "</font></span></small>");
            } else {
                $('#checkAvailability-result').html("<span><font color=blue>" + data.message + "</font></span></small>");
            }
        }
    });
}

function addNewSocialSettings(){
    if ((test.className).indexOf("page-small") > -1) {
        test.classList.remove("show-sidebar");
        test.classList.add("hide-sidebar");
    }
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    var s = './createSocialSetting.jsp';
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#otherPage').html(data);
        },
        complete: function () {
            document.getElementById("wrapper3").style.display = "none";
            $('#wrapper2').show();
        }

    });
}

function showstate(value){
    var s = './selectState.jsp?_countryID='+value;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#state1').html(data);
            var stateID = document.getElementById("state1").value;            
            showcities(stateID);
        }
    });
}

function showstateV2(value,stateId){
    var s = './selectState.jsp?_countryID='+value+'&stateId='+stateId;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#state1').html(data);
            var stateID = document.getElementById("state1").value;            
            showcities(stateID);
        }
    });
}
function showstateV3(value,stateId,cityId){
    var s = './selectState.jsp?_countryID='+value+'&stateId='+stateId;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#state1').html(data);
            var stateID = document.getElementById("state1").value;            
            showcitiesV2(stateID,cityId);
        }
    });
}

function showcities(value){
    var s = './selectCity.jsp?_stateID='+value;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#city1').html(data);
        }
    });
}

function showcitiesV2(value,CityId){
    var s = './selectCity.jsp?_stateID='+value+'&cityId='+CityId;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#city1').html(data);
        }
    });
}
