/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var waiting;
function pleasewait() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    //waiting = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"></div></div></div>');
    waiting.modal();
}
//function copyToClipboard(elem) {
//    // create hidden text element, if it doesn't already exist
//    var targetId = "_hiddenCopyText_";
//    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
//    var origSelectionStart, origSelectionEnd;
//    if (isInput) {
//        // can just use the original source element for the selection and copy
//        target = elem;
//        origSelectionStart = elem.selectionStart;
//        origSelectionEnd = elem.selectionEnd;
//    } else {
//        // must use a temporary form element for the selection and copy
//        target = document.getElementById(targetId);
//        if (!target) {
//            var target = document.createElement("textarea");
//            target.style.position = "absolute";
//            target.style.left = "-9999px";
//            target.style.top = "0";
//            target.id = targetId;
//            document.body.appendChild(target);
//        }
//        target.textContent = elem.textContent;
//    }
//    // select the content
//    var currentFocus = document.activeElement;
//    target.focus();
//    target.setSelectionRange(0, target.value.length);
//
//    // copy the selection
//    var succeed;
//    try {
//        succeed = document.execCommand("copy");
//    } catch (e) {
//        succeed = false;
//    }
//    // restore original focus
//    if (currentFocus && typeof currentFocus.focus === "function") {
//        currentFocus.focus();
//    }
//
//    if (isInput) {
//        // restore prior selection
//        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
//    } else {
//        // clear temporary content
//        target.textContent = "";
//    }
//    return succeed;
//}

// new copy function
function selectElementContents(el)
{
    // Copy textarea, pre, div, etc.
    if (document.body.createTextRange) {
        // IE 
        var textRange = document.body.createTextRange();
        textRange.moveToElementText(el);
        textRange.select();
        textRange.execCommand("Copy");
    } else if (window.getSelection && document.createRange) {
        // non-IE
        var range = document.createRange();
        range.selectNodeContents(el);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copy command was ' + msg);
        } catch (err) {
            console.log('Oops, unable to copy');
        }
    }
} // end function selectElementContents(el) 

function make_copy_button(el)
{
    var copy_btn = document.createElement('input');
    copy_btn.type = "button";
    copy_btn.className = "btn btn-success btn-xs";
    el.parentNode.insertBefore(copy_btn, el.nextSibling);
    copy_btn.onclick = function () {
        selectElementContents(el);
    };

    if (document.queryCommandSupported("copy") || parseInt(navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./)[2]) >= 42)
    {
        // Copy works with IE 4+, Chrome 42+, Firefox 41+, Opera 29+
        copy_btn.value = "Copy to Clipboard";
    } else
    {
        // Select only for Safari and older Chrome, Firefox and Opera
        copy_btn.value = "Select All (then press CTRL+C to Copy)";
    }
}
/* Note: document.queryCommandSupported("copy") should return "true" on browsers that support copy
 but there was a bug in Chrome versions 42 to 47 that makes it return "false".  So in those
 versions of Chrome feature detection does not work!
 See https://code.google.com/p/chromium/issues/detail?id=476508
 */



function bootboxmodelAl(content) {
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body" id=tokenDetail>' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>' +
            '<div >' +
            content +
            '</div>' +
            '<a class="btn btn-success btn-xs" data-dismiss="modal" class="close"><i class="fa fa-thumbs-o-up"></i> OK</a>' +
            '</div></div></div></div>');
    popup.modal();
    //copyToClipboard(document.getElementById("tokenDetail"));
    // make_copy_button(document.getElementById("tokenDetail"));
}
function Alert4Users(msg) {
    bootboxmodelAl("<h4>" + msg + "</h4>");
// myApp.alert("EPIN Copied","SUCCESS");
}
function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        // alerts-container does not exist, create it
        $("body")
                .append($('<div id="alerts-container" style="position: fixed;' +
                        'width: 50%; left: 25%; top: 10%;">'));
    }
    // default to alert-info; other options include success, warning, danger
    type = type || "info";
    // create the alert div
    message = '<i class="fa fa-info-circle"></i> ' + message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);
    // add the alert div to top of alerts-container, use append() to add to bottom
    $("#alerts-container").prepend(alert);
    // if closeDelay was passed - set a timeout to close the alert
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);
}

function assignAPIToken(apiName, idNo) {
    var l = $('#reAssignToken' + idNo).ladda();
    l.ladda('start');
    initializeToastr();
    var s = './AssignAPIToken?apiName=' + apiName;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#assign_token_form").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.error('Error - ' + data.message);
                }, 3000);
            } else if (strCompare(data.result, "success") === 0) {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.success('Success - ' + data.message);
                }, 3000);
            }
        }
    });
}

function viewAPIToken(apiName, idNo) {
    var l = $('#viewToken' + idNo).ladda();
    l.ladda('start');
    initializeToastr();
    var s = './ViewAPIToken?apiName=' + apiName;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#assign_token_form").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.error('Error - ' + data.message);
                }, 3000);
            } else if (strCompare(data.result, "success") === 0) {
                var msgToken = "<p><b>" + data.message + "</b></p>";
                var type = "Token";
                var tokenType = "<h4>" + type + "<h4>";
                //document.getElementById('tokenType').innerHTML = tokenType;
                document.getElementById('test1').innerHTML = msgToken;
                var inputTag = "<input type='text' id='copyTarget' style='display: none'>";
                //var copyTag = '<a href="#/" style="font-size:15px; margin-left:35%;text-decoration:none" onclick="copy('+ data.encodeTokenDetails +')"><b>Copy</b></a>';
                var copyTag = "<a href='#/' style='font-size:15px; margin-left:15%;text-decoration:none' onclick=copy('"+data.encodeTokenDetails+"','"+idNo+"')><b>Copy</b></a>";
                console.log("copyTag >>> "+copyTag);
                document.getElementById('copyDiv').innerHTML = copyTag;
                document.getElementById('inputTag').innerHTML = inputTag;
                $('#tmTermsAndCondition').modal();
                setTimeout(function () {
                    l.ladda('stop');
                }, 3000);
            }
        }
    });
}

function emailAPIToken(apiName, idNo) {
    var s = './EmailAPIToken?apiName=' + apiName;
    var l = $('#sendApi' + idNo).ladda();
    l.ladda('start');
    initializeToastr();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#assign_token_form").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.error('Error - ' + data.message);
                }, 3000);
            } else if (strCompare(data.result, "success") === 0) {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.success('Success - ' + data.message);
                }, 3000);
            }
        }
    });
}

function autoAssignAPIToken(apiName) {
    var s = './AssignAPIToken?apiName=' + apiName;
    initializeToastr();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#assign_token_form").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                document.getElementById("_myNameRM").value = "APITokenId";
                document.getElementById('_myResType').value = 'NA';

                document.getElementById("_myNameRMRest").value = "APITokenId";
                document.getElementById('_myResTypeRest').value = 'NA';
            } else if (strCompare(data.result, "success") === 0) {
                document.getElementById("_myNameRM").value = "APITokenId";
                document.getElementById('_myResType').value = data.token;

                document.getElementById("_myNameRMRest").value = "APITokenId";
                document.getElementById('_myResTypeRest').value = data.token;
                toastr.success('Congratulations - ' + "Auto API Token Assignment Is Completed And It Take Two Minutes To Get Reflected. Please Try It After Two Minutes.");
                
            }
        }
    });
}

function confirmAssignToken(apiName, idNo) {
    swal({
        title: "Are you sure?",
        text: "\n\n1) If you have an application with " + apiName + " API  used,\n You need to update your code with Header value\n for APITokenId with newly generated token.\n\n 2) You need to wait for 2 minute for getting it reflected to \nuse the API.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true},
            function (isConfirm) {
                if (isConfirm) {
                    assignAPIToken(apiName, idNo);
                } else {
                    swal("Cancelled", "Your request has been cancelled)", "error");
                }
            });
}