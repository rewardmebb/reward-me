/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
	$( ".accordion" ).accordion({
		header: "h3",
		heightStyle: "content",
		collapsible: true,
		active: 'none'
	});
});
$(document).on('accordion:lazy',function(){
	$( ".accordion" ).accordion({
		header: "h3",
		heightStyle: "content",
		collapsible: true,
		active: 'none',
		icons: false
	});
});
