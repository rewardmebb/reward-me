//update the method defination
function subscribePostpaid(packageName, idNum) {
    var s = './SubscribePostpaid?_packageName=' + packageName;
    var l;
    l = $('#subscriptionButton' + idNum).ladda();
    l.ladda('start');
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                setTimeout(function () {
                    toastr.error('Error - ' + data.message);
                    l.ladda('stop');
                }, 3000);
            } else if (strCompare(data.result, "success") === 0) {
                setTimeout(function () {
                    toastr.success('Success - ' + data.message);
                    l.ladda('stop');
                }, 3000);
                setTimeout(function () {
                    window.location = "./home.jsp";
                }, 4000);
            } else if (strCompare(data.result, "redirect") === 0) {
                setTimeout(function () {
                    toastr.error('Error - ' + data.message);
                    l.ladda('stop');
                }, 3000);
                setTimeout(function () {
                    window.location = "./invoiceDetail.jsp";
                }, 4000);
            }
        }
    });
}

//updation end

//check for pleasewait() if not present add the following
var waiting;
function pleasewait() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    //waiting = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"></div></div></div>');
    waiting.modal();
}
// end

// check for showAlert(), if not present add the following
function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        // alerts-container does not exist, create it
        $("body")
                .append($('<div id="container" style="' +
                        'width: %; margin-left: 65%; margin-top: 10%;">'));
    }
    // default to alert-info; other options include success, warning, danger
    type = type || "info";
    message = '<i class="fa fa-info-circle"></i> ' + message;
    // create the alert div
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);

    // add the alert div to top of alerts-container, use append() to add to bottom
    $("#alerts-container").prepend(alert);

    // if closeDelay was passed - set a timeout to close the alert
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);

}

function showAPRate() {
    $('#addPackageTab a[href="#apRate"]').tab('show');
}
function showAPIThrottling() {
    $('#addPackageTab a[href="#apiThrottling"]').tab('show');
}
function showRate() {
    $('#addPackageTab a[href="#rate"]').tab('show');
}
function showSlabPrice() {
    $('#addPackageTab a[href="#promoCode"]').tab('show');
}
function showTierPrice() {
    $('#addPackageTab a[href="#tieringPrice"]').tab('show');
}
function showSecurityAndAlerts() {
    $('#addPackageTab a[href="#alerts"]').tab('show');
}
function showfeature() {
    $('#addPackageTab a[href="#feature"]').tab('show');
}
//end

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode !== 46 && charCode > 31
            && (charCode < 48 || charCode > 57)) {
        var showMessage = 'Enter Numeric Or Decimal Values Only';
        Alert4Users("<span><font color=red>" + showMessage + "</font></span>");
        return false;
    }
    return true;
}
function isNumericKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if ((charCode < 48 || charCode > 57)) {
        var showMessage = 'Enter Numeric Values Only';
        Alert4Users("<span><font color=red>" + showMessage + "</font></span>");
        return false;
    }
    return true;
}

function showTierPrice() {
    $('#addPackageTab a[href="#tieringPrice"]').tab('show');
}
function packagedetails(packageName){
    var s="my_subscription_package_detail_live.jsp?_name="+packageName;  
    $('#otherPage').empty();
    $('#wrapper1').hide();
    document.getElementById("wrapper3").style.display = "block";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
             $('#otherPage').html(data);
        },complete: function () {
            document.getElementById("wrapper3").style.display = "none";
            $('#wrapper2').show();
        }
    });
}

function subscribePrepaid(packageName, _paymentMode) {
    var form = $('<form></form>');

    form.attr("method", "post");
//    form.attr("action", "./tminvoice.jsp");
    form.attr("action","./invoice.jsp");
    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "_paymentMode");
    field.attr("value", _paymentMode);
    form.append(field);

    var field = $('<input></input>');
    field.attr("type", "hidden");
    field.attr("name", "_packageName");
    field.attr("value", packageName);
    form.append(field);
    $(document.body).append(form);
    form.submit();
}