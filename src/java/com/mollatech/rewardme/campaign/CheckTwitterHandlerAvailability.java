/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.campaign;

import com.mollatech.rewardme.nucleus.db.RmSocialmediadetails;
import com.mollatech.rewardme.nucleus.db.connector.management.SocialMediaManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "CheckTwitterHandlerAvailability", urlPatterns = {"/CheckTwitterHandlerAvailability"})
public class CheckTwitterHandlerAvailability extends HttpServlet {

    static final Logger logger = Logger.getLogger(CheckTwitterHandlerAvailability.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(logger.getLevel() == null){
            logger.setLevel(Level.ALL);
        }
        logger.info("Requested Servlet is CheckTwitterHandlerAvailability at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Handler name is available";                
        String twitterHandlerId = request.getParameter("twitterHandlerId");
        logger.info("twitterHandlerId " + twitterHandlerId);
        String strtwitterHandlerId = twitterHandlerId.trim();                                       
        try {
            if (strtwitterHandlerId.trim().length() == 0) {
                result = "error";
                message = "Please Enter Your account handler Name.";
                json.put("result", result);
                logger.info("Response of CheckTwitterHandlerAvailability Servlet's from Parameter   result is " + result);
                json.put("message", message);
                logger.info("Response of CheckTwitterHandlerAvailability Servlet's from Parameter   message is " + message);
                return;
            } 
            RmSocialmediadetails bucketObj = new SocialMediaManagement().getSocialMediaDetailsByHandlerName(strtwitterHandlerId);
            if (bucketObj != null) {
                result = "error";
                message = "Handler name already in use";
                json.put("result", result);
                logger.info("Response of CheckTwitterHandlerAvailability Servlet's from  Parameter   result is " + result);
                json.put("message", message);
                logger.info("Response of CheckTwitterHandlerAvailability Servlet's from Parameter   message is " + message);
                return;
            } else {
                result = "success";
                message = "Handler name is available";
                json.put("result", result);
                logger.info("Response of CheckTwitterHandlerAvailability Servlet's from Parameter   result is " + result);
                json.put("message", message);
                logger.info("Response of CheckTwitterHandlerAvailability Servlet's from Parameter   message is " + message);
                return;
            }
        } catch (Exception e) {
            logger.error("Exception at CheckTwitterHandlerAvailability ", e);
            result = "error";
            message = "Error in Checking Availability";
            json.put("result", result);
            logger.info("Response of CheckTwitterHandlerAvailability Servlet's from Parameter   result is " + result);
            json.put("message", message);
            logger.info("Response of CheckTwitterHandlerAvailability Servlet's from Parameter   message is " + message);
        } finally {
            logger.info("Response of CheckTwitterHandlerAvailability from" + json.toString());
            logger.info("Response of CheckTwitterHandlerAvailability Servlet from at " + new Date());
            out.print(json);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
