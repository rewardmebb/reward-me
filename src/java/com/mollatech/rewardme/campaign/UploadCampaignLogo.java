/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.campaign;

import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "UploadCampaignLogo", urlPatterns = {"/UploadCampaignLogo"})
public class UploadCampaignLogo extends HttpServlet {

    static final Logger logger = Logger.getLogger(UploadCampaignLogo.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Request servlet is #UploadCampaignLogo from #RewardMePortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("text/html;charset=UTF-8");
        RmBrandownerdetails usrObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
        Integer logoCount = (Integer) request.getSession().getAttribute("logoImagesCount");
        logoCount++;
        PrintWriter out = response.getWriter();
        String strError = "";
        String saveFile = "";
        String savepath = "";
        String result = "success";
        String message = "File uploaded sucessfully";
        JSONObject json = new JSONObject();
        savepath = System.getProperty("catalina.home");
        if (savepath == null) {
            savepath = System.getenv("catalina.home");
        }
        savepath += System.getProperty("file.separator");
        savepath += "rewardme-settings";
        savepath += System.getProperty("file.separator");
        savepath += "uploads";
        savepath += System.getProperty("file.separator");
        String optionalFileName = "";
        FileItem fileItem = null;
        String[] files = new String[1];
        String dirName = savepath;
        int retValue = 0;
        int i = 0;
        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);
                Iterator it = fileItemsList.iterator();
                while (it.hasNext()) {
                    FileItem fileItemTemp = (FileItem) it.next();
                    if (fileItemTemp.isFormField()) {
                        if (fileItemTemp.getFieldName().equals("filename")) {
                            optionalFileName = fileItemTemp.getString();
                        } else {
                            System.out.println("Additional fields in file upload form is  " + fileItemTemp.getFieldName());
                        }
                    } else {
                        fileItem = fileItemTemp;
                    }
                    if (fileItem != null) {
                        String fileName = fileItem.getName();
                        if (fileItem.getSize() == 0) {
                            strError = "Please Select File To Upload";
                            result = "error";
                            try {
                                json.put("result", result);
                                json.put("message", strError);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            out.print("{result:'" + result + "',message:'" + strError + "'}");
                            out.flush();
                            logger.info("Response of #UploadCampaignLogo from #RewardMePortal " + json.toString());
                            logger.info("Response of #UploadCampaignLogo from #RewardMePortal at " + new Date());
                            return;
                        }
                        if (fileItem.getSize() > 0 && fileItem.getSize() < 1024000 * 5) {
                            if (optionalFileName.trim().equals("")) {
                                fileName = FilenameUtils.getName(fileName);
                            } else {
                                fileName = optionalFileName;
                            }
                            files[i++] = dirName + fileName;
                            File saveTo = new File(dirName + fileName);
                            saveFile = fileName;
                            try {
                                fileItem.write(saveTo);
                                HttpSession session = request.getSession(true);                                                                
                                session.setAttribute("_logoImageUploaded"+logoCount, saveTo.getAbsolutePath());                                
                                session.setAttribute("_logofileName"+logoCount, fileName);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            strError = "Error: " + fileName + " Size is more than 5MB. Please upload correct file size.";
                            result = "error";
                            try {
                                json.put("result", result);
                                json.put("message", strError);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            out.print("{result:'" + result + "',message:'" + strError + "'}");
                            out.flush();
                            logger.info("Response of #UploadCampaignLogo from #RewardMePortal " + json.toString());
                            logger.info("Response of #UploadCampaignLogo from #RewardMePortal at " + new Date());
                            return;
                        }
                    } else {
                        result = "error";
                        message = "Error: No File Present";
                        try {
                            json.put("result", result);
                            json.put("message", message);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        out.print("{result:'" + result + "',message:'" + message + "'}");
                        out.flush();
                        return;
                    }
                }
            } catch (FileUploadException ex) {
            }
        } else {
            result = "error";
            message = "Error: Form post is invalid";
            try {
                json.put("result", result);
                json.put("message", message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print("{result:'" + result + "',message:'" + message + "'}");
            out.flush();
            logger.info("Response of #UploadCampaignLogo from #RewardMePortal " + json.toString());
            logger.info("Response of #UploadCampaignLogo from #RewardMePortal at " + new Date());
            return;
        }
        String _logoImageUploaded = (String) request.getSession().getAttribute("_logoImageUploaded"+logoCount);
        if (_logoImageUploaded != null) {
            strError = "File Uploaded Successfully";
            result = "success";            
            request.getSession().setAttribute("logoImagesCount",logoCount);
            try {
                json.put("result", result);
                json.put("message", strError);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print("{result:'" + result + "',message:'" + strError + "'}");
            out.flush();
            logger.info("Response of #UploadCampaignLogo from #RewardMePortal " + json.toString());
            logger.info("Response of #UploadCampaignLogo from #RewardMePortal at " + new Date());
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
