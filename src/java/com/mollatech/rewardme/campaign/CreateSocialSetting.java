/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.campaign;

import static com.mollatech.rewardme.nucleus.commons.CommonUtility.SHA1;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.RmSocialmediadetails;
import com.mollatech.rewardme.nucleus.db.connector.management.SocialMediaManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "CreateSocialSetting", urlPatterns = {"/CreateSocialSetting"})
public class CreateSocialSetting extends HttpServlet {

    static final Logger logger = Logger.getLogger(CreateSocialSetting.class);
    public static final int PENDING = 2;
    public static final int SEND = 0;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        if(logger.getLevel() == null){
            logger.setLevel(Level.ALL);
        }
        logger.info("Request servlet is #CreateSocialSetting called at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String SessionId = (String) request.getSession().getAttribute("_brandownerSessionId");
        RmBrandownerdetails user  = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
        String result = "success";
        String message = "Your Social setting added successfully.";
        String fbappid = request.getParameter("fbappid");
        String fbappkey = request.getParameter("fbappkey");
        String fbclientSec = request.getParameter("fbclientsecret");
        String twitterAppId = request.getParameter("twitterAppId");
        String twitterAppKey = request.getParameter("twitterAppkey");
        String twitterClientSec = request.getParameter("twitterClientSecret");
        String twitterConsumerSecret = request.getParameter("twitterConsumerSecret");
        String instaAppId = request.getParameter("instaAppid");
        String instaAppkey = request.getParameter("instaAppkey");
        String instaClientSec = request.getParameter("instaClientsec");
        String twitterHandlerId = request.getParameter("twitterHandlerId");
        logger.info("fbappid : " + fbappid);
        logger.info("fbappkey : " + fbappkey);
        logger.info("fbclientSec : " + fbclientSec);
        logger.info("twitterAppId : " + twitterAppId);
        logger.info("twitterAppKey : " + twitterAppKey);
        logger.info("twitterClientSec : " + twitterClientSec);
        logger.info("twitterConsumerSecret : " + twitterConsumerSecret);
        logger.info("twitterHandlerId : " + twitterHandlerId);
        logger.info("instaAppId : " + instaAppId);
        logger.info("instaAppkey : " + instaAppkey);
        logger.info("instaClientSec : " + instaClientSec);        
        SocialMediaManagement socialSetting = new SocialMediaManagement();
        RmSocialmediadetails socialMedia = socialSetting.getSocialMediaDetailsByHandlerName(twitterHandlerId);
        if(socialMedia!=null){
            try {
                message = "Your Twitter handler name is already associated with other settings.";
                json.put("_result", "error");
                logger.info("Response of CreateSocialSetting Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of CreateSocialSetting Servlet's Parameter message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        Date d = new Date();
        byte[] SHA1hash = SHA1(user.getBrandName() + user.getPassword() + user.getEmail() + user.getPhone() + d.toString());
        String socialUniqueId = new String(org.bouncycastle.util.encoders.Base64.encode(SHA1hash));
        String rmSpecialChar = socialUniqueId.replaceAll("[^a-zA-Z0-9]", "s");
        RmSocialmediadetails socialObj = new RmSocialmediadetails();
        socialObj.setOwnerId(user.getOwnerId());
        socialObj.setFacebookAppId(fbappid);
        socialObj.setFacebookAppkey(fbappkey);
        socialObj.setFacebookClientsecret(fbclientSec);
        socialObj.setTwitterAccessToken(twitterAppId);
        socialObj.setTwitterAccessTokenSecret(twitterAppKey);
        socialObj.setTwitterConsumerKey(twitterClientSec);
        socialObj.setTwitterConsumerSecret(twitterConsumerSecret);
        socialObj.setInstagramAppId(instaAppId);
        socialObj.setInstagramAppkey(instaAppkey);
        socialObj.setInstagramClientsecret(instaClientSec);
        socialObj.setCreationDate(new Date());
        socialObj.setTwitterHandlerName(twitterHandlerId);
        socialObj.setUniqueSocialMediaId(rmSpecialChar);
        int res = socialSetting.CreateSocialMediaDetails(SessionId, socialObj);
        if (res == -1) {
          message = "Failed to add social media setting.";
           result = "error";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            logger.info("Response of #CreateSocialSetting from Servlet's Parameter  result is " + result);
            logger.info("Response of #CreateSocialSetting from Servlet's Parameter  message is " + message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }  
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
