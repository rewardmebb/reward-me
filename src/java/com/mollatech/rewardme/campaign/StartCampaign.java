/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.campaign;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmAiTrackingCampaignDetails;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.RmCampaignExecutionName;
import com.mollatech.rewardme.nucleus.db.RmCampaigndetails;
import com.mollatech.rewardme.nucleus.db.RmCreditinfo;
import com.mollatech.rewardme.nucleus.db.RmSocialmediadetails;
import com.mollatech.rewardme.nucleus.db.RmSubscriptionpackagedetails;
import com.mollatech.rewardme.nucleus.db.connector.management.AITrackingManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.CampaignExeNameManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.CampaignManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.CreditManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.SocialMediaManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.SubscriptionManagement;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "StartCampaign", urlPatterns = {"/StartCampaign"})
public class StartCampaign extends HttpServlet {

    static final Logger logger = Logger.getLogger(StartCampaign.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(logger.getLevel() == null){
            logger.setLevel(Level.ALL);
        }
        logger.info("Request servlet is #StartCampaign called at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "campaign started successfully.";
        PrintWriter out = response.getWriter();
        RmBrandownerdetails usrObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
        String SessionId = (String) request.getSession().getAttribute("_brandownerSessionId"); 
        String campaignUniqueId = request.getParameter("campaignUniqueId");
        String campaignExeName = request.getParameter("campaignExeName");
        logger.info("campaignUniqueId " + campaignUniqueId);
        logger.info("campaignExeName " + campaignExeName);
        RmCampaigndetails campaignDetails = new CampaignManagement().getCampaignByUniqueIdDetails(campaignUniqueId);
        if (campaignDetails == null) {
            try {
                message = "Campign details not found to start.";
                json.put("_result", "error");
                logger.info("Response of StartCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of StartCampaign Servlet's Parameter message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        if (campaignDetails.getStatus() == GlobalStatus.START_PROCESS) {
            try {
                message = "Campign already in running state.";
                json.put("_result", "error");
                logger.info("Response of StartCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of StartCampaign Servlet's Parameter message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        RmSubscriptionpackagedetails subscriptionObj = new SubscriptionManagement().getSubscriptionbyOwnerId(usrObj.getOwnerId());
        if (subscriptionObj == null) {
            try {
                message = "Package subscription details not found.";
                json.put("_result", "error");
                logger.info("Response of StartCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of StartCampaign Servlet's Parameter message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        if(subscriptionObj.getExpiryDate().before(new Date())){
            try {
                message = "Your package is expired, Please renew it with new subscription.";
                json.put("_result", "error");
                logger.info("Response of StartCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of StartCampaign Servlet's Parameter message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        RmSocialmediadetails socialDetails = new SocialMediaManagement().getSocialMediaDetailsByUniqueId(campaignDetails.getTwitterSocialSetting());
        if(socialDetails == null){
            try {
                message = "Social setting not configured, Please add social setting first.";
                json.put("_result", "error");
                logger.info("Response of StartCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of StartCampaign Servlet's Parameter message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        RmCampaigndetails[] listOfCampaignBySocialId = new CampaignManagement().getAllCampaignBySocialMediaUniqueId(campaignDetails.getTwitterSocialSetting());
        if(listOfCampaignBySocialId != null){
            try {
                message = "Other campaign already running with social setting "+socialDetails.getTwitterHandlerName();
                json.put("_result", "error");
                logger.info("Response of StartCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of StartCampaign Servlet's Parameter message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        RmCreditinfo creditDetails = new CreditManagement().getDetailsByOwnerId(usrObj.getOwnerId());
        Float balanceCredit = creditDetails.getMainCredit();
        String creditDeduction = subscriptionObj.getCreditDeductionConfiguration();
        try{
            JSONObject jsonObj = new JSONObject(creditDeduction);
            String perUserCredit = null; String perCampaignCredit = null;
            if(jsonObj.has("userSearched")){
                perUserCredit = jsonObj.getString("userSearched");                
            }
            if(jsonObj.has("perCampaign")){
                perCampaignCredit = jsonObj.getString("perCampaign");                
            }
            if(perUserCredit == null && perCampaignCredit ==null){
                try {
                message = "Credit setting not found";
                json.put("_result", "error");
                logger.info("Response of StartCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of StartCampaign Servlet's Parameter message is " + message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                out.print(json);
                out.flush();
                return;
            }            
            Float userSearchedCredit = Float.parseFloat(perUserCredit);
            Float campaignCredit = Float.parseFloat(perCampaignCredit);
            int targetUser = campaignDetails.getTargetUser();
            Float estimatedPerUserCredit = targetUser * userSearchedCredit;
            Float estimatedTotalCredit = campaignCredit + estimatedPerUserCredit;
            if(balanceCredit-estimatedTotalCredit < 0){
                try {
                message = "Credit not enough to run campaign";
                json.put("_result", "error");
                logger.info("Response of StartCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of StartCampaign Servlet's Parameter message is " + message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                out.print(json);
                out.flush();
                return;
            }
            RmCampaigndetails[] campaignObj = new CampaignManagement().getCampaignByOwnerId(usrObj.getOwnerId());            
            if (campaignObj != null) {
                for(int i=0;i<campaignObj.length;i++){
                    String camName = campaignObj[i].getCampaignExecutedIdDetails();
                    if(camName!=null){
                        List<String> al = Arrays.asList(camName.split(","));                        
                        if(al.stream().anyMatch(campaignExeName::equalsIgnoreCase)){
                            result = "error";
                            message = "Campaign execution name already in use";
                            json.put("_result", result);
                            logger.info("Response of StartCampaign Servlet's from  Parameter   result is " + result);
                            json.put("_message", message);
                            logger.info("Response of StartCampaign Servlet's from Parameter   message is " + message);
                            return;
                        }
                    }
                }                
            }
            // Campaign Start
            boolean aiServiceResponse = startCampaign(campaignDetails.getCampaignUniqueId());
            //boolean aiServiceResponse = true;
            if(aiServiceResponse){
            RmCampaignExecutionName campaignExe = new RmCampaignExecutionName();
            campaignExe.setOwnerId(usrObj.getOwnerId());
            campaignExe.setCampaignExeName(campaignExeName);
            campaignExe.setCreationDate(new Date());
            campaignExe.setCampaignId(campaignDetails.getCampaignId());
            campaignExe.setStatus(GlobalStatus.START_PROCESS);
            new CampaignExeNameManagement().CreateCampaignExeName(SessionId, campaignExe);
            Float excreditdeduction = balanceCredit-estimatedTotalCredit;            
            Float creditdeduction = balanceCredit-campaignCredit;            
            campaignDetails.setStatus(GlobalStatus.START_PROCESS);
            String campaignExeIds = campaignDetails.getCampaignExecutedIdDetails();
            if(campaignExeIds != null && !campaignExeIds.isEmpty())
                campaignExeIds += "," + campaignExeName;
            else
                campaignExeIds = campaignExeName;
            campaignDetails.setCampaignExecutedIdDetails(campaignExeIds);
            campaignDetails.setCurrentCampaignExecution(campaignExeName);
            new CampaignManagement().updateDetails(campaignDetails);
            creditDetails.setMainCredit(creditdeduction);
            creditDetails.setExpectedCreditDeduction(excreditdeduction);
            new CreditManagement().updateDetails(creditDetails);
            
            RmAiTrackingCampaignDetails aiTracking = new RmAiTrackingCampaignDetails();
            aiTracking.setCampaignId(campaignDetails.getCampaignId());
            aiTracking.setCreationDate(new Date());
            aiTracking.setOwnerId(usrObj.getOwnerId());
            aiTracking.setCreditDeducted(campaignCredit);
            aiTracking.setCampaignRunFlag(GlobalStatus.ACTIVE);
            new AITrackingManagement().addTracking(aiTracking);
            message = "Campaign started successfully.";
            json.put("_result", "success");
            logger.info("Response of StartCampaign Servlet's Parameter result is error");
            json.put("_message", message);
            logger.info("Response of StartCampaign Servlet's Parameter message is " + message);
            }else{
                message = "Campaign failed to start.";
                json.put("_result", "error");
                logger.info("Response of StartCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of StartCampaign Servlet's Parameter message is " + message);
            }
        }catch(Exception e){
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static boolean startCampaign(String campaignId){
        boolean response = false;
        try {
            String aiServiceURL = LoadSettings.g_sSettings.getProperty("ai.start.campaign");
            //System.out.println("aiServiceURL >> "+aiServiceURL);
		URL url = new URL(aiServiceURL);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");

                JSONObject requestJson = new JSONObject();
                requestJson.put("compainid", campaignId);
                requestJson.put("operation_name", "start_compain");
		String input = requestJson.toString();
                //System.out.println("input >> "+input);
                logger.info("Request "+input);
		OutputStream os = conn.getOutputStream();
		os.write(input.getBytes());
		os.flush();

		if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
			throw new RuntimeException("Failed : HTTP error code : "
				+ conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));

		String output;
                StringBuffer stringBuf = new StringBuffer("");
                
		System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {
			//System.out.println(output);
                        stringBuf.append(output);
		}
                if(stringBuf != null){
                    output = stringBuf.toString();
                } 
                logger.info("Response "+output);
                if(output != null && !output.isEmpty()){
                    JSONObject result = new JSONObject(output);
                    if(result.has("task")){
                        String strresult  = result.getString("task");
                        JSONObject obj1 = new JSONObject(strresult);
                        if(obj1.has("errorcode")){
                          //System.out.println("result from ai >> "+obj1.get("errorcode"));  
                          String errorCode = obj1.getString("errorcode");
                          if(errorCode.equalsIgnoreCase("0")){
                              response = true;
                             
                          }
                        }
                    }
                }
                
		conn.disconnect();
                return response;
	  } catch (MalformedURLException e) {
		e.printStackTrace();
	  } catch (IOException e) {
		e.printStackTrace();
	 }catch(Exception e){
             e.printStackTrace();
         }
        return response;
    }
}
