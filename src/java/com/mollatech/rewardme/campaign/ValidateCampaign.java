/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.campaign;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.RmCampaigndetails;
import com.mollatech.rewardme.nucleus.db.RmCreditinfo;
import com.mollatech.rewardme.nucleus.db.RmSocialmediadetails;
import com.mollatech.rewardme.nucleus.db.RmSubscriptionpackagedetails;
import com.mollatech.rewardme.nucleus.db.connector.management.CampaignManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.CreditManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.SocialMediaManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.SubscriptionManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "ValidateCampaign", urlPatterns = {"/ValidateCampaign"})
public class ValidateCampaign extends HttpServlet {

    static final Logger logger = Logger.getLogger(ValidateCampaign.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(logger.getLevel() == null){
            logger.setLevel(Level.ALL);
        }
        logger.info("Request servlet is #ValidateCampaign called at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "campaign ready to start.";
        PrintWriter out = response.getWriter();
        RmBrandownerdetails usrObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");        
        String campaignUniqueId = request.getParameter("campaignUniqueId");
        logger.info("campaignUniqueId " + campaignUniqueId);
        RmCampaigndetails campaignDetails = new CampaignManagement().getCampaignByUniqueIdDetails(campaignUniqueId);
        if (campaignDetails == null) {
            try {
                message = "Campign details not found to start.";
                json.put("_result", "error");
                logger.info("Response of ValidateCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of ValidateCampaign Servlet's Parameter message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        if (campaignDetails.getStatus() == GlobalStatus.START_PROCESS) {
            try {
                message = "Campign already in running state.";
                json.put("_result", "error");
                logger.info("Response of ValidateCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of ValidateCampaign Servlet's Parameter message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        RmSubscriptionpackagedetails subscriptionObj = new SubscriptionManagement().getSubscriptionbyOwnerId(usrObj.getOwnerId());
        if (subscriptionObj == null) {
            try {
                message = "Package subscription details not found.";
                json.put("_result", "error");
                logger.info("Response of ValidateCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of ValidateCampaign Servlet's Parameter message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        if(subscriptionObj.getExpiryDate().before(new Date())){
            try {
                message = "Your package is expired, Please renew it with new subscription.";
                json.put("_result", "error");
                logger.info("Response of ValidateCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of ValidateCampaign Servlet's Parameter message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        RmSocialmediadetails socialDetails = new SocialMediaManagement().getSocialMediaDetailsByUniqueId(campaignDetails.getTwitterSocialSetting());
        if(socialDetails == null){
            try {
                message = "Social setting not configured, Please add social setting first.";
                json.put("_result", "error");
                logger.info("Response of ValidateCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of ValidateCampaign Servlet's Parameter message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        RmCampaigndetails[] listOfCampaignBySocialId = new CampaignManagement().getAllCampaignBySocialMediaUniqueId(campaignDetails.getTwitterSocialSetting());
        if(listOfCampaignBySocialId != null){
            try {
                message = "Other campaign already running with social setting "+socialDetails.getTwitterHandlerName();
                json.put("_result", "error");
                logger.info("Response of ValidateCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of ValidateCampaign Servlet's Parameter message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        RmCreditinfo creditDetails = new CreditManagement().getDetailsByOwnerId(usrObj.getOwnerId());
        Float balanceCredit = creditDetails.getMainCredit();
        String creditDeduction = subscriptionObj.getCreditDeductionConfiguration();
        try{
            JSONObject jsonObj = new JSONObject(creditDeduction);
            String perUserCredit = null; String perCampaignCredit = null;
            if(jsonObj.has("userSearched")){
                perUserCredit = jsonObj.getString("userSearched");                
            }
            if(jsonObj.has("perCampaign")){
                perCampaignCredit = jsonObj.getString("perCampaign");                
            }
            if(perUserCredit == null && perCampaignCredit ==null){
                try {
                message = "Credit setting not found";
                json.put("_result", "error");
                logger.info("Response of ValidateCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of ValidateCampaign Servlet's Parameter message is " + message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                out.print(json);
                out.flush();
                return;
            }            
            Float userSearchedCredit = Float.parseFloat(perUserCredit);
            Float campaignCredit = Float.parseFloat(perCampaignCredit);
            int targetUser = campaignDetails.getTargetUser();
            Float estimatedPerUserCredit = targetUser * userSearchedCredit;
            Float estimatedTotalCredit = campaignCredit + estimatedPerUserCredit;
            if(balanceCredit-estimatedTotalCredit < 0){
                try {
                message = "Credit not enough to run campaign";
                json.put("_result", "error");
                logger.info("Response of ValidateCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of ValidateCampaign Servlet's Parameter message is " + message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                out.print(json);
                out.flush();
                return;
            }
            message = "Campaign ready to start.";
            json.put("_result", "success");
            logger.info("Response of StartCampaign Servlet's Parameter result is error");
            json.put("_message", message);
            logger.info("Response of StartCampaign Servlet's Parameter message is " + message);            
        }catch(Exception e){
            e.printStackTrace();            
        }finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
