/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.campaign;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.RmCampaignExecutionName;
import com.mollatech.rewardme.nucleus.db.RmCampaigndetails;
import com.mollatech.rewardme.nucleus.db.RmCreditinfo;
import com.mollatech.rewardme.nucleus.db.connector.management.CampaignExeNameManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.CampaignManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.CreditManagement;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
@WebServlet(name = "StopCampaign", urlPatterns = {"/StopCampaign"})
public class StopCampaign extends HttpServlet {
static final Logger logger = Logger.getLogger(StopCampaign.class);
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(logger.getLevel() == null){
            logger.setLevel(Level.ALL);
        }
        logger.info("Request servlet is #StopCampaign called at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "campaign stop successfully.";
        PrintWriter out = response.getWriter();
        RmBrandownerdetails usrObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");        
        String campaignUniqueId = request.getParameter("campaignUniqueId");
        logger.info("campaignUniqueId " + campaignUniqueId);
        RmCampaigndetails campaignDetails = new CampaignManagement().getCampaignByUniqueIdDetails(campaignUniqueId);
        if (campaignDetails == null) {
            try {
                message = "Campign details not found to stop.";
                json.put("_result", "error");
                logger.info("Response of StopCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of StopCampaign Servlet's Parameter message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }                                              
        try{                        
            // Campaign Start
            //boolean aiServiceResponse = stopCampaign(campaignDetails.getCampaignUniqueId());
            //if(aiServiceResponse){                         
            campaignDetails.setStatus(GlobalStatus.ACTIVE);
            new CampaignManagement().updateDetails(campaignDetails);
            RmCreditinfo creditDetails = new CreditManagement().getDetailsByOwnerId(usrObj.getOwnerId());
            creditDetails.setExpectedCreditDeduction(creditDetails.getMainCredit());
            new CreditManagement().updateDetails(creditDetails);
            RmCampaignExecutionName camExeObj = new CampaignExeNameManagement().getCampaignDetailsByExename(usrObj.getOwnerId(), campaignDetails.getCurrentCampaignExecution());
            if(camExeObj != null){
                camExeObj.setStatus(GlobalStatus.ACTIVE);
                new CampaignExeNameManagement().updateDetails(camExeObj);
            }
            message = "Campaign stop successfully.";
            json.put("_result", "success");
            logger.info("Response of StopCampaign Servlet's Parameter result is error");
            json.put("_message", message);
            logger.info("Response of StopCampaign Servlet's Parameter message is " + message);
//            }else{
//                message = "Campaign failed to stop.";
//                json.put("_result", "error");
//                logger.info("Response of StopCampaign Servlet's Parameter result is error");
//                json.put("_message", message);
//                logger.info("Response of StopCampaign Servlet's Parameter message is " + message);
//            }
        }catch(Exception e){
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
public static boolean stopCampaign(String campaignId){
        boolean response = false;
        try {
            String aiServiceURL = LoadSettings.g_sSettings.getProperty("ai.start.campaign");
            //System.out.println("aiServiceURL >> "+aiServiceURL);
		URL url = new URL(aiServiceURL);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");

                JSONObject requestJson = new JSONObject();
                requestJson.put("compainid", campaignId);
                requestJson.put("operation_name", "stop_compain");
		String input = requestJson.toString();
                logger.info("Request "+input);
		OutputStream os = conn.getOutputStream();
		os.write(input.getBytes());
		os.flush();

		if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
			throw new RuntimeException("Failed : HTTP error code : "
				+ conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));

		String output;
                StringBuffer stringBuf = new StringBuffer();
                
		//System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {
			//System.out.println(output);
                        stringBuf.append(output);
		}
                if(stringBuf != null){
                    output = stringBuf.toString();
                }
                logger.info("Response "+output);
                if(output != null && !output.isEmpty()){
                    JSONObject result = new JSONObject(output);
                    if(result.has("task")){
                        String strresult  = result.getString("task");
                        JSONObject obj1 = new JSONObject(strresult);
                        if(obj1.has("errorcode")){
                          //System.out.println("result from ai >> "+obj1.get("errorcode"));  
                          String errorCode = obj1.getString("errorcode");
                          if(errorCode.equalsIgnoreCase("0")){
                              response = true;                              
                          }
                        }
                    }
                }
		conn.disconnect();
                return response;
	  } catch (MalformedURLException e) {
		e.printStackTrace();
	  } catch (IOException e) {
		e.printStackTrace();
	 }catch(Exception e){
             e.printStackTrace();
         }
        return response;
    }
}
