/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.campaign;

import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.RmCampaigndetails;
import com.mollatech.rewardme.nucleus.db.connector.management.CampaignManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "CheckCampaignExecutionName", urlPatterns = {"/CheckCampaignExecutionName"})
public class CheckCampaignExecutionName extends HttpServlet {

    static final Logger logger = Logger.getLogger(CheckCampaignExecutionName.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(logger.getLevel() == null){
            logger.setLevel(Level.ALL);
        }
        logger.info("Requested Servlet is CheckCampaignExecutionName at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Handler name is available";                
        String campaignExecutionName = request.getParameter("campaignExecutionName");
        RmBrandownerdetails usrObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
        logger.info("campaignExecutionName " + campaignExecutionName);
        String strcampaignExecutionName = campaignExecutionName.trim();                                       
        try {
            if (strcampaignExecutionName.trim().length() == 0) {
                result = "error";
                message = "Please Enter Your campaign execution Name.";
                json.put("result", result);
                logger.info("Response of CheckCampaignExecutionName Servlet's from Parameter   result is " + result);
                json.put("message", message);
                logger.info("Response of CheckCampaignExecutionName Servlet's from Parameter   message is " + message);
                return;
            } 
            RmCampaigndetails[] campaignObj = new CampaignManagement().getCampaignByOwnerId(usrObj.getOwnerId());            
            if (campaignObj != null) {
                for(int i=0;i<campaignObj.length;i++){
                    String camName = campaignObj[i].getCampaignExecutedIdDetails();
                    if(camName!=null){
                        List<String> al = Arrays.asList(camName.split(","));
                        if(al.stream().anyMatch(campaignExecutionName::equalsIgnoreCase)){
                            result = "error";
                            message = "Campaign execution name already in use";
                            json.put("result", result);
                            logger.info("Response of CheckCampaignExecutionName Servlet's from  Parameter   result is " + result);
                            json.put("message", message);
                            logger.info("Response of CheckCampaignExecutionName Servlet's from Parameter   message is " + message);
                            return;
                        }
                    }
                }
                
            } else {
                result = "success";
                message = "Campaign exection name is available";
                json.put("result", result);
                logger.info("Response of CheckCampaignExecutionName Servlet's from Parameter   result is " + result);
                json.put("message", message);
                logger.info("Response of CheckCampaignExecutionName Servlet's from Parameter   message is " + message);
                return;
            }
            result = "success";
            message = "Campaign exection name is available";
            json.put("result", result);
            logger.info("Response of CheckCampaignExecutionName Servlet's from Parameter   result is " + result);
            json.put("message", message);
            logger.info("Response of CheckCampaignExecutionName Servlet's from Parameter   message is " + message);
            return;
        } catch (Exception e) {
            logger.error("Exception at CheckCampaignExecutionName ", e);
            result = "error";
            message = "Error in Checking Availability";
            json.put("result", result);
            logger.info("Response of CheckCampaignExecutionName Servlet's from Parameter   result is " + result);
            json.put("message", message);
            logger.info("Response of CheckCampaignExecutionName Servlet's from Parameter   message is " + message);
        } finally {
            logger.info("Response of CheckCampaignExecutionName from" + json.toString());
            logger.info("Response of CheckCampaignExecutionName Servlet from at " + new Date());
            out.print(json);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
