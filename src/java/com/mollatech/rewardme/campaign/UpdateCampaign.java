/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.campaign;

import com.mollatech.rewardme.nucleus.commons.GetLocationFromAddress;
import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.RmCampaigndetails;
import com.mollatech.rewardme.nucleus.db.RmCities;
import com.mollatech.rewardme.nucleus.db.RmCountries;
import com.mollatech.rewardme.nucleus.db.RmStates;
import com.mollatech.rewardme.nucleus.db.connector.management.CampaignManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.CityManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.CountryManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.StateManagement;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "UpdateCampaign", urlPatterns = {"/UpdateCampaign"})
public class UpdateCampaign extends HttpServlet {

    static final Logger logger = Logger.getLogger(UpdateCampaign.class);
    public static final int PENDING = 2;
    public static final int SEND = 0;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(logger.getLevel() == null){
            logger.setLevel(Level.ALL);
        }
        logger.info("Request servlet is #UpdateCampaign called at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Your campaign updated successfully.";
        PrintWriter out = response.getWriter();
        RmBrandownerdetails usrObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
        String SessionId = (String) request.getSession().getAttribute("_brandownerSessionId"); 
        String campaignTitle = request.getParameter("campaignTitle");
        String couponCode = request.getParameter("couponCode");
        String rewardPoint = request.getParameter("rewardPoint");
        String promotionURL = request.getParameter("promotionURL");
        String noOfUser = request.getParameter("noOfUser");
        String campaignPhrase = request.getParameter("campaignPhrase");
        String startDate = request.getParameter("datapicker1");
        String endDate = request.getParameter("datapicker2");
        String starttime = request.getParameter("datapicker3");
        String endtime = request.getParameter("datapicker4");
        String state = request.getParameter("state1");
        String city = request.getParameter("city1"); 
        String country = request.getParameter("country");  
        String noOfFollower = request.getParameter("noOfFollower");  
        String noOfFriends = request.getParameter("noOfFriends");                
        Integer imageLogoCount = (Integer) request.getSession().getAttribute("logoImagesCount");   
        String campaignId = request.getParameter("campaignId");
        String campaignMessage = request.getParameter("message");
        String twitterSetting = request.getParameter("twitterSetting");
        logger.info("campaignTitle : " + campaignTitle);
        logger.info("couponCode : " + couponCode);
        logger.info("rewardPoint : " + rewardPoint);
        logger.info("promotionURL : " + promotionURL);
        logger.info("noOfUser : " + noOfUser);
        logger.info("campaignPhrase : " + campaignPhrase);
        logger.info("startDate : " + startDate);
        logger.info("endDate : " + endDate);
        logger.info("starttime : " + starttime);
        logger.info("endtime : " + endtime);
        logger.info("country : " + country);
        logger.info("state : " + state);
         logger.info("city : " + city);
        logger.info("imageLogoCount : " + imageLogoCount);        
        logger.info("campaignId : " + campaignId);       
        logger.info("campaignMessage : " + campaignMessage);
        logger.info("twitterSetting : " + twitterSetting);
        int icampaign = 0;
        if (twitterSetting.equalsIgnoreCase("-1")) {
            try {
                message = "Please select social settings";
                json.put("_result", "error");
                logger.info("Response of UpdateCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of UpdateCampaign Servlet's Parameter message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        try{

        if(campaignId != null && !campaignId.isEmpty()){
            icampaign = Integer.parseInt(campaignId);
        }
            Calendar current = Calendar.getInstance();
            Calendar currenttime = Calendar.getInstance();
            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            DateFormat timeformat = new SimpleDateFormat("HH:mm");
            Date sttime = null;
            if (startDate != null && !startDate.isEmpty()) {
                sttime = (Date) timeformat.parse(starttime);
            }
            Date entime = null;
            if (startDate != null && !startDate.isEmpty()) {
                entime = (Date) timeformat.parse(endtime);
            }
            Date dStartDate = null;
            if (startDate != null && !startDate.isEmpty()) {
                dStartDate = (Date) formatter.parse(startDate);
            }
            Date dEndDate = null;
            if (endDate != null && !endDate.isEmpty()) {
                dEndDate = (Date) formatter.parse(endDate);
            }
            if (startDate != null || endDate != null) {
                current.setTime(dEndDate);
                currenttime.setTime(entime);
                current.set(Calendar.AM_PM, currenttime.get(Calendar.AM_PM));
                current.set(Calendar.HOUR, currenttime.get(Calendar.HOUR));
                current.set(Calendar.MINUTE, currenttime.get(Calendar.MINUTE));
                dEndDate = current.getTime();
                current.setTime(dStartDate);
                currenttime.setTime(sttime);
                current.set(Calendar.AM_PM, currenttime.get(Calendar.AM_PM));
                current.set(Calendar.HOUR, currenttime.get(Calendar.HOUR));
                current.set(Calendar.MINUTE, currenttime.get(Calendar.MINUTE));
                dStartDate = current.getTime();                
            }
                
         RmCampaigndetails campaignDetails = new CampaignManagement().getCampaignByIdDetails(icampaign);
         if (campaignDetails == null) {
            try {
                message = "Campign details not found to update.";
                json.put("_result", "error");
                logger.info("Response of UpdateCampaign Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of UpdateCampaign Servlet's Parameter message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        byte[] byteImage = null;
        JSONObject logoImage = new JSONObject();
        int logoCount = 0;String base64ImageString = "";
        if (imageLogoCount != null && imageLogoCount != 0) {
            for(int i=1; i<=imageLogoCount; i++){
                String logoPath = (String)request.getSession().getAttribute("_logoImageUploaded"+i);
                if(logoPath != null){                
                    logoCount++;
                    Path path = Paths.get(logoPath);
                    byteImage = Files.readAllBytes(path);
                    String imgBase64 = Base64.encode(byteImage);
//                    logoImage.put("logo"+logoCount, imgBase64);
//                    logoImage.put("logoCount", logoCount);
                    if(base64ImageString.isEmpty())
                        base64ImageString += imgBase64;
                    else
                        base64ImageString += "," + imgBase64;

                }
                request.getSession().setAttribute("_logoImageUploaded"+i, null);
                File f = new File(logoPath);
                f.delete();
            }
            //compressed_json = logoImage.toString().getBytes();
            //campaignDetails.setLogoImages(compressed_json);
            campaignDetails.setLogos(base64ImageString);
            
        }
        boolean isGeoBoundarySet = true;
        GetLocationFromAddress getGeo = new GetLocationFromAddress();
            if (country != null && !country.equalsIgnoreCase("-1")) {
                RmCountries countryObj = new CountryManagement().getCountryDetailsById(Integer.parseInt(country));
                if(countryObj != null){
                    String countryLoction = getGeo.getGeoData(countryObj.getName());                    
                    campaignDetails.setState(countryLoction);
                    campaignDetails.setCampaignInCountry(country);
                    isGeoBoundarySet = false; 
                }                
            }else{
                campaignDetails.setCountry(null);
                campaignDetails.setCampaignInCountry(null);
            }            
            if (state != null && !state.equalsIgnoreCase("-1")) {                
                RmStates stateObj = new StateManagement().getStateDetailsById(Integer.parseInt(state));
                if(stateObj != null){  
                    String stateLocation = getGeo.getGeoData(stateObj.getName());
                    campaignDetails.setState(stateLocation);
                    campaignDetails.setCampaignInRegion(state);
                    isGeoBoundarySet = false;
                }
            }else{
//                campaignDetails.setState(null);
                campaignDetails.setCampaignInRegion(null);
            }
            if (city != null && !city.equalsIgnoreCase("-1")) {
                RmCities cityObj = new CityManagement().getCityDetailsById(Integer.parseInt(city));
                if(cityObj != null){
                    String cityLocation = getGeo.getGeoData(cityObj.getName());
                    campaignDetails.setState(cityLocation);
                    campaignDetails.setCampaignInCity(city);
                    isGeoBoundarySet = false;
                }
            }else{
                campaignDetails.setCity(null);
                campaignDetails.setCampaignInCity(null);
            }
            if(isGeoBoundarySet){
                campaignDetails.setState(null);
            }
        int retValue = -1;                       
        int inoOfUser = 0;
       
//        campaignDetails.setCampaignInCountry(country);        
        campaignDetails.setCampaignEndDate(dEndDate);
        campaignDetails.setCampaignStartDate(dStartDate);
//        campaignDetails.setCampaignInRegion(state);
        campaignDetails.setCampaignPhrase(campaignPhrase);
        campaignDetails.setCreationDate(new Date());
        if(noOfFollower != null && !noOfFollower.isEmpty()){
            int iNoOfFollower = Integer.parseInt(noOfFollower);
            campaignDetails.setNoOfFollowers(iNoOfFollower);
        }
        if(noOfFriends != null && !noOfFriends.isEmpty()){
            int iNoOfFriends = Integer.parseInt(noOfFriends);
            campaignDetails.setNoOfFriends(iNoOfFriends);
        }
        if(noOfUser != null && !noOfUser.isEmpty()){
            inoOfUser = Integer.parseInt(noOfUser);
        }
        campaignDetails.setPromotionUrl(promotionURL);
        campaignDetails.setRewardCouponCode(couponCode);
        campaignDetails.setRewardPoint(rewardPoint);
        campaignDetails.setRewardTitle(campaignTitle);
        campaignDetails.setTargetUser(inoOfUser);
        campaignDetails.setOwnerId(usrObj.getOwnerId());
        campaignDetails.setMessage(campaignMessage);        
        campaignDetails.setStatus(GlobalStatus.ACTIVE);
        campaignDetails.setTwitterSocialSetting(twitterSetting);
        retValue = new CampaignManagement().updateDetails(campaignDetails);         
        if (retValue >= 0) {                                   
            result = "success";
            message = "Campaign updated successfully.";
            request.getSession().setAttribute("logoImagesCount",null);
        } else {
            result = "error";
            message = "Error while creating your account.";
        }
        json.put("_result", result);
        json.put("_message", message);
        logger.info("Response of #UpdateCampaign from Servlet's Parameter  result is " + result);
        logger.info("Response of #UpdateCampaign from Servlet's Parameter  message is " + message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
