/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.reports;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.rewardme.nucleus.db.RmAiTrackingCampaignDetails;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.RmCampaigndetails;
import com.mollatech.rewardme.nucleus.db.connector.management.AITrackingManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.CampaignManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "DeveloperFiveService", urlPatterns = {"/DeveloperFiveService"})
public class DeveloperFiveService extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Map<String, Long> map = new HashMap<String, Long>();
        ArrayList<ApiDashboard> sample = new ArrayList<ApiDashboard>();
        try {
            RmBrandownerdetails usrObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");

            Date endDate = new Date();
            endDate.setHours(23);
            endDate.setMinutes(59);
            endDate.setSeconds(59);
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, (-30));
            Date froDate1 = cal.getTime();
            froDate1.setHours(00);
            froDate1.setMinutes(00);
            froDate1.setSeconds(00);

            AITrackingManagement requesTr = new AITrackingManagement();
            RmCampaigndetails[] campaignDetails = new CampaignManagement().getCampaignByOwnerId(usrObj.getOwnerId());
            if(campaignDetails != null){
                if(campaignDetails.length > 5){
                    for(int i=0; i<5; i++){
                    RmAiTrackingCampaignDetails[] campaigntx = requesTr.getTxDetailsByOwnerIdAndCampaignId(usrObj.getOwnerId(), froDate1, endDate,campaignDetails[i].getCampaignId());
                    int campaignLength = 0;
                    if(campaigntx != null){
                        campaignLength = campaigntx.length;
                    }
                    sample.add(new ApiDashboard(campaignDetails[i].getRewardTitle(), campaignLength));                    
                    }
                }else {
                    boolean recordFound = false;
                    for(int i=0; i<campaignDetails.length; i++){
                    RmAiTrackingCampaignDetails[] campaigntx = requesTr.getTxDetailsByOwnerIdAndCampaignId(usrObj.getOwnerId(), froDate1, endDate,campaignDetails[i].getCampaignId());
                    int campaignLength = 0;
                    if(campaigntx != null){
                        campaignLength = campaigntx.length;
                        sample.add(new ApiDashboard(campaignDetails[i].getRewardTitle(), campaignLength));
                        recordFound = true;
                    }else{
                        sample.add(new ApiDashboard("NA", 0));                    
                    }
                }
                    int length = 5 - campaignDetails.length;                    
                    if(!recordFound && length != 0){
                        for(int j=0; j<length; j++){
                            sample.add(new ApiDashboard("NA", 0));
                        }
                    }
                }
            }else{
                sample.add(new ApiDashboard("NA", 0));
                sample.add(new ApiDashboard("NA", 0));
                sample.add(new ApiDashboard("NA", 0));
                sample.add(new ApiDashboard("NA", 0));
                sample.add(new ApiDashboard("NA", 0));
            }
            
            

        } catch (Exception e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree(sample, new TypeToken<List<ApiDashboard>>() {
        }.getType());
        JsonArray jsonArray = element.getAsJsonArray();
        out.print(jsonArray);
        out.flush();
        out.close();
        return;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
