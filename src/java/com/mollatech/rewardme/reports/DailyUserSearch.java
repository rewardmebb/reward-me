///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mollatech.rewardme.reports;
//
//import com.google.gson.Gson;
//import com.google.gson.JsonArray;
//import com.google.gson.JsonElement;
//import com.google.gson.reflect.TypeToken;
//import com.mollatech.rewardme.nucleus.db.RmAiTrackingCampaignDetails;
//import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
//import com.mollatech.rewardme.nucleus.db.connector.management.AITrackingManagement;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.math.BigDecimal;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.Iterator;
//import java.util.LinkedHashMap;
//import java.util.List;
//import java.util.Map;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import org.json.JSONObject;
//
///**
// *
// * @author abhishekingle
// */
//@WebServlet(name = "DailyUserSearch", urlPatterns = {"/DailyUserSearch"})
//public class DailyUserSearch extends HttpServlet {
//
//    /**
//     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
//     * methods.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        response.setContentType("application/json");
//        PrintWriter out = response.getWriter();
//        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
//        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
//        RmBrandownerdetails usrObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
//        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");        
//        int callCount = 0;
//        float amount = 0;
//        DateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//        try {
//            Date cdate = format.parse(format.format(new Date()));
//            //SgHourlytransactiondetails[] shs = new HourlyTxManagement().getTxDetails(ChannelId, partnerId, format.format(new Date()), format.format(new Date()), "00:00 AM", "11:59 PM");
//            List<Object[]> aiTracking = new AITrackingManagement().getCountUsingHQL(usrObj.getOwnerId(), cdate, cdate, true);
//            cdate.setHours(0);
//            cdate.setMinutes(59);
//            Map partnerObj = new LinkedHashMap();
//            for (int i = 0; i < writeFormat.parse(writeFormat.format(new Date())).getHours(); i++) {
//                Date date = new Date();
//                date.setHours(i);
//                date.setMinutes(59);
//                partnerObj.put(writeFormat.format(date), "{}");
//            }
//            if (aiTracking != null) {
//                for (Object[] os : aiTracking) {
//                    {
//                        try {
//                            JSONObject data = new JSONObject(transcationdetailse.getRawData());
//                            cdate = transcationdetailse.getExecuutionDate();
//                            if (!partnerObj.containsKey("" + writeFormat.format(transcationdetailse.getExecuutionDate()))) {
//                                partnerObj.put("" + writeFormat.format(cdate), data.toString());
//                            } else {
//                                Iterator itr = data.keys();
//                                while (itr.hasNext()) {
//                                    Object key = itr.next();
//                                    JSONObject oldData = new JSONObject((String) partnerObj.get("" + writeFormat.format(cdate)));
//                                    if (oldData.isNull((String) key)) {
//                                        oldData.put((String) key, data.getString((String) key));
//                                        partnerObj.put("" + writeFormat.format(cdate), oldData.toString());
//                                    } else {
//                                        String value = data.getString((String) key);
//                                        String value1 = oldData.getString((String) key);
//                                        int count = Integer.parseInt(value.split(":")[0]);
//                                        count = count + Integer.parseInt(value1.split(":")[0]);
//                                        value = count + ":" + value.split(":")[1];
//                                        oldData.put((String) key, value);
//                                        partnerObj.put("" + cdate, oldData.toString());
//                                    }
//                                }
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//                ArrayList<bar> sample = new ArrayList<bar>();
//                BigDecimal inttotalCharge = null;
//                if (!partnerObj.isEmpty()) {
//                    for (Object keyS : partnerObj.keySet()) {
//                        JSONObject data = new JSONObject((String) partnerObj.get((String) keyS));
//                        Iterator itr = data.keys();
//                        inttotalCharge = new BigDecimal("0");
//                        callCount = 0;
//                        BigDecimal calculateCharge = null;
//                        while (itr.hasNext()) {
//                            Object key = itr.next();
//                            String keyData = (String) key;
//                            String value = data.getString((String) key);                                                    
//                            callCount += Integer.parseInt(value.split(":")[0]);
//                            calculateCharge = new BigDecimal(value.split(":")[0]);
//                            amount = Float.valueOf(value.split(":")[1]);
//                            if (amount != -99) {
//                                calculateCharge = calculateCharge.multiply(new BigDecimal(value.split(":")[1]));
//                                inttotalCharge = inttotalCharge.add(calculateCharge);
//                                inttotalCharge = inttotalCharge.setScale(2, BigDecimal.ROUND_HALF_EVEN);
//                            }
//                        }
//                        sample.add(new bar(callCount, inttotalCharge.floatValue(), (String) keyS));
//                    }
//                } else {
//                    sample.add(new bar(0, 0, "NA"));
//                }
//                Gson gson = new Gson();
//                JsonElement element = gson.toJsonTree(sample, new TypeToken<List<bar>>() {
//                }.getType());
//                JsonArray jsonArray = element.getAsJsonArray();
//                out.print(jsonArray);
//                out.flush();
//                out.close();
//                return;
//            } else {
//                ArrayList<bar> sample = new ArrayList<bar>();
//                for (Object obj : partnerObj.keySet()) {
//                    sample.add(new bar(0, 0, (String) obj));
//                }
//                Gson gson = new Gson();
//                JsonElement element = gson.toJsonTree(sample, new TypeToken<List<bar>>() {
//                }.getType());
//                JsonArray jsonArray = element.getAsJsonArray();
//                out.print(jsonArray);
//                out.flush();
//                out.close();
//                return;
//            }
//        } catch (Exception ee) {
//            ee.printStackTrace();
//        } finally {
//            out.close();
//            out.flush();
//        }
//    }
//
//    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
//    /**
//     * Handles the HTTP <code>GET</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Handles the HTTP <code>POST</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Returns a short description of the servlet.
//     *
//     * @return a String containing servlet description
//     */
//    @Override
//    public String getServletInfo() {
//        return "Short description";
//    }// </editor-fold>
//
//}
