///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mollatech.rewardme.reports;
//
//import com.google.gson.Gson;
//import com.google.gson.JsonArray;
//import com.google.gson.JsonElement;
//import com.google.gson.reflect.TypeToken;
//import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Objects;
//import java.util.concurrent.ConcurrentHashMap;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
///**
// *
// * @author abhishekingle
// */
//@WebServlet(name = "TopFiveServices", urlPatterns = {"/TopFiveServices"})
//public class TopFiveServices extends HttpServlet {
//
//    /**
//     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
//     * methods.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        response.setContentType("application/json");
//        PrintWriter out = response.getWriter();
//        Map<String, Long> map = new ConcurrentHashMap<>();
//        ArrayList<ApiDashboard> sample = new ArrayList<ApiDashboard>();        
//        try {
//            String SessionId = (String) request.getSession().getAttribute("_advertisorSessionId");
//            String ChannelId = (String) request.getSession().getAttribute("_advertiserChannelId");
//            //Integer partnerId = (Integer) request.getSession().getAttribute("_partnerID");
//            RmBrandownerdetails usrObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
//            Integer advertisorId = usrObj.getOwnerId();
//
//            Date endDate = new Date();
//            endDate.setHours(23);
//            endDate.setMinutes(59);
//            endDate.setSeconds(59);
//            Calendar cal = Calendar.getInstance();
//            Date froDate1 = new Date();
//            froDate1.setHours(00);
//            froDate1.setMinutes(00);
//            froDate1.setSeconds(00);
//            AdvertiserManagement part = new AdvertiserManagement();
//            SgAdvertiserDetails[] partdetails = part.getAllAdvertiserDetails(SessionId, ChannelId);
//            AdvertisementTrackingManagement requesTr = new AdvertisementTrackingManagement();
//            List<Integer> partList = new ArrayList();
//            Map<Integer, String> resourceMap = new HashMap();
//            Map<String, Long> sortedMap;
//            Map<String, Long> tempPartnerMap = new ConcurrentHashMap<String, Long>();
//
//            if (partdetails != null) {
//                for (SgAdvertiserDetails pdetails : partdetails) {
//                    if (!Objects.equals(pdetails.getAdId(), advertisorId)) {
//                        partList.add(pdetails.getAdId());
//                    }
//                }                
//                resourceMap.put(GlobalStatus.PDF_AD, "PDF Ad");
//                map.put("PDF Ad", 0l);
//                tempPartnerMap.put("PDF Ad", 0l);
//
//                resourceMap.put(GlobalStatus.EMAIL_AD, "Email Ad");
//                map.put("Email Ad", 0l);
//                tempPartnerMap.put("Email Ad", 0l);
//
//                resourceMap.put(GlobalStatus.PUSH_AD, "Push Ad");
//                map.put("Push Ad", 0l);
//                tempPartnerMap.put("Push Ad", 0l);
//
//                List<Object[]> objects = requesTr.getCountUsingHQL(advertisorId, froDate1, endDate, false);
//                if (objects != null) {
//                    int count = 0;
//                    for (Object[] os : objects) {
//                        if (count < 5) {
//                            String resName = resourceMap.get(os[1]);
//                            if (map.get(resName) < (Long) os[0]) {
//                                map.put(resName, (Long) os[0]);
////                                count++;
//                            }
//                        } else {
//                            break;
//                        }
//                    }
//                }
//                if (map.isEmpty()) {
//                    int count = 0;
//                    for (int key : resourceMap.keySet()) {
//                        if (count < 5) {
//                            map.put(resourceMap.get(key), 0l);
////                            count++;
//                        } else {
//                            break;
//                        }
//                    }
//                }
//                map = SortMapByValue.sortByComparator(map, SortMapByValue.DESC);
//                List<Object[]> partnerCount = requesTr.getCountUsingHQL(advertisorId, froDate1, endDate, true);
//                if (partnerCount != null) {
//                    int count = 0;
//                    for (Object[] os : partnerCount) {
//                        if (count < 5) {
//                            String resName = resourceMap.get(os[1]);
//                            if (tempPartnerMap.get(resName) < (Long) os[0]) {
//                                tempPartnerMap.put(resName, (Long) os[0]);
////                                count++;
//                            }
//                        } else {
//                            break;
//                        }
//                    }
//                }
//                if (tempPartnerMap.isEmpty()) {
//                    int count = 0;
//                    for (int key : resourceMap.keySet()) {
//                        if (count < 5) {
//                            tempPartnerMap.put(resourceMap.get(key), 0l);
////                            count++;
//                        } else {
//                            break;
//                        }
//                    }
//                }
//                sortedMap = SortMapByValue.sortByComparator(tempPartnerMap, SortMapByValue.DESC);
//
//                Map<String, Long> TopFive = new HashMap();
//                for (String key : map.keySet()) {
//                    if (sortedMap.get(key) == null || map.get(key) >= sortedMap.get(key)) {
//                        TopFive.put(key, map.get(key));
//                    } else {
//                        TopFive.put(key, sortedMap.get(key));
//                    }
//                }
//                TopFive = SortMapByValue.sortByComparator(TopFive, SortMapByValue.DESC);
//                int i = 0;
//
//                for (Map.Entry<String, Long> entrySet
//                        : TopFive.entrySet()) {
//                    if (i >= 5) {
//                        break;
//                    }
//                    String apiname = entrySet.getKey();
//                    sample.add(new ApiDashboard(apiname, map.get(apiname), (sortedMap.get(apiname) == null) ? 0l : sortedMap.get(apiname)));
//                    i++;
//                }
//            }
//            
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        Gson gson = new Gson();
//        JsonElement element = gson.toJsonTree(sample, new TypeToken<List<ApiDashboard>>() {
//        }.getType());
//        JsonArray jsonArray = element.getAsJsonArray();
//        out.print(jsonArray);
//
//        out.flush();
//        out.close();
//        return;
//    }
//
//    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
//    /**
//     * Handles the HTTP <code>GET</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Handles the HTTP <code>POST</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Returns a short description of the servlet.
//     *
//     * @return a String containing servlet description
//     */
//    @Override
//    public String getServletInfo() {
//        return "Short description";
//    }// </editor-fold>
//
//}
