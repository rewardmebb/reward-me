/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.reports;

/**
 *
 * @author abhishekingle
 */
public class AreaChartGraph {
    String strDate;

    float intTotal;
    Integer callCount;
    
    public AreaChartGraph(String strDate, float intTotal) {
        this.intTotal = intTotal;
        this.strDate = strDate;
    }
    public AreaChartGraph(String strDate, float intTotal,Integer callCount) {
        this.intTotal = intTotal;
        this.strDate = strDate;
        this.callCount = callCount;
    }
    public String getStrDate() {
        return strDate;
    }

    public void setStrDate(String strDate) {
        this.strDate = strDate;
    }

    public float getIntTotal() {
        return intTotal;
    }

    public void setIntTotal(float intTotal) {
        this.intTotal = intTotal;
    }
}
