/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.reports;

import com.mollatech.rewardme.nucleus.db.RmAiTrackingCampaignDetails;
import com.mollatech.rewardme.nucleus.db.connector.management.AITrackingManagement;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "GenerateUserSearched", urlPatterns = {"/GenerateUserSearched"})
public class GenerateUserSearched extends HttpServlet {
    private static int PDF_TYPE = 0;

    private static int CSV_TYPE = 1;

    private static int TEXT_TYPE = 2;

    private static final int BUFSIZE = 4096;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        RmAiTrackingCampaignDetails[] aiTracking = (RmAiTrackingCampaignDetails[]) request.getSession().getAttribute("aiTrackingDetails");
        
        String sdate = request.getParameter("_sdate");
        String stime = request.getParameter("_stime");
        String edate = request.getParameter("_edate");
        String format = request.getParameter("_format");        
        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        DateFormat timeformat = new SimpleDateFormat("hh:mm a");
        String filepath = null;
        try {
            Date startDate = null;
            if (sdate != null && !sdate.isEmpty()) {
                startDate = formatter.parse(sdate);
            }
            Date startTime = null;
            if (stime != null && !stime.isEmpty()) {
                startTime = timeformat.parse(stime);
            }
            Date endDate = null;
            if (edate != null && !edate.isEmpty()) {
                endDate = formatter.parse(edate);
            }
            Date endTime = null;
            if (stime != null && !stime.isEmpty()) {
                endTime = timeformat.parse(stime);
            }
            int iFormat = -9999;
            if (format != null && !format.isEmpty()) {
                iFormat = Integer.parseInt(format);
            }
            if (PDF_TYPE == iFormat) {
                iFormat = PDF_TYPE;
            } else if (CSV_TYPE == iFormat) {
                iFormat = CSV_TYPE;
            } else {
                iFormat = TEXT_TYPE;
            }            
            filepath = new AITrackingManagement().generateReport(iFormat,aiTracking);
        } catch (Exception e) {
            e.printStackTrace();
        }
        File file = new File(filepath);
        int length = 0;
        ServletOutputStream outStream = response.getOutputStream();
        ServletContext context = getServletConfig().getServletContext();
        String mimetype = context.getMimeType(filepath);
        if (mimetype == null) {
            mimetype = "application/octet-stream";
        }
        response.setContentType(mimetype);
        response.setContentLength((int) file.length());
        String fileName = (new File(filepath)).getName();
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        byte[] byteBuffer = new byte[BUFSIZE];
        DataInputStream in = new DataInputStream(new FileInputStream(file));
        while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
            outStream.write(byteBuffer, 0, length);
        }
        in.close();
        outStream.close();
        file.delete();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
