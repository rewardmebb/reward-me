/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.reports;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmAiTrackingCampaignDetails;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.connector.management.AITrackingManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "MonthlyCampaignChart", urlPatterns = {"/MonthlyCampaignChart"})
public class MonthlyCampaignChart extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();        
        String campaignId = request.getParameter("campaignId");        
        String month = request.getParameter("month");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat getCurrentDate = new SimpleDateFormat("yyyy");
        RmBrandownerdetails usrObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
        String currentYear = getCurrentDate.format(new Date());
        try {
            Date currentDate = sdf.parse(currentYear + "-" + month + "-01");
            int iAdvertiserID = 0;            
            if (campaignId != null) {
                iAdvertiserID = Integer.parseInt(campaignId);
            }
            
            Date startDate;
            Date endDate;

            Calendar cal = Calendar.getInstance();
            cal.setTime(currentDate);
            int daysInMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
                      
            ArrayList<AreaChartGraph> sample = new ArrayList<AreaChartGraph>();
            for (int i = 1; i <= daysInMonth; i++) {
                cal = Calendar.getInstance();
                cal.setTime(currentDate);
                //cal.add(Calendar.DATE, 1);
                startDate = cal.getTime();
                //froDate = cal.getTime();
                cal.add(Calendar.DATE, 1);
                endDate = cal.getTime();
                //System.out.println("StartDate >> "+startDate.toString() +" EndDate >> "+endDate.toString());
                currentDate = cal.getTime();
                //sDate = formatter.format(froDate);
                RmAiTrackingCampaignDetails[] shs = new AITrackingManagement().getTxDetailsByOwnerIdAndCampaignId(usrObj.getOwnerId(), startDate, endDate,iAdvertiserID);
                Float creditUsed = 0.0f;
                Integer adShown = 0; 
                String keyDate = formatter.format(startDate);
                if (shs != null) {
                    //adShown = shs.length;
                    for (int j = 0; j < shs.length; j++) {
                        creditUsed += shs[j].getCreditDeducted();
                        if(shs[j].getCampaignRunFlag() != null && shs[j].getCampaignRunFlag() == GlobalStatus.ACTIVE){
                            adShown++;
                        }
                    }
                    sample.add(new AreaChartGraph(keyDate, creditUsed, adShown));
                } else {
                    sample.add(new AreaChartGraph(keyDate, creditUsed, adShown));
                }
            }

            Gson gson = new Gson();
            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<AreaChartGraph>>() {
            }.getType());
            JsonArray jsonArray = element.getAsJsonArray();
            out.print(jsonArray);
            out.flush();
            out.close();
            return;

        } catch (Exception ee) {
            ee.printStackTrace();
        } finally {
            out.close();
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
