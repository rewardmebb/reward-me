/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.reports;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.RmCampaigndetails;
import com.mollatech.rewardme.nucleus.db.connector.management.AITrackingManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.CampaignManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "TopfiveTrendingSer", urlPatterns = {"/TopfiveTrendingSer"})
public class TopfiveTrendingSer extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        int ActiveStatus = 0;
        Map<String, Long> map = new HashMap<String, Long>();
        ArrayList<ApiDashboard> sample = new ArrayList<ApiDashboard>();
        try {
            RmBrandownerdetails usrObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
            Integer advertisorId = usrObj.getOwnerId();
            Date endDate = new Date();
            endDate.setHours(23);
            endDate.setMinutes(59);
            endDate.setSeconds(59);
            Date startDate = new Date();
            startDate.setHours(00);
            startDate.setMinutes(00);
            startDate.setSeconds(00);

            AITrackingManagement requesTr = new AITrackingManagement();
            RmCampaigndetails[] campaignDetails = new CampaignManagement().getCampaignByOwnerId(usrObj.getOwnerId());
            Map<Integer, String> resourceMap = new HashMap();
            if (campaignDetails != null) {
                for (RmCampaigndetails resData : campaignDetails) {
                    resourceMap.put(resData.getCampaignId(), resData.getRewardTitle());
                    map.put(resData.getRewardTitle(), 0l);
                }
            }else{
                resourceMap.put(1, "NA");
            }                          
            //List<Object[]> partnerCount = requesTr.getTrendingCountUsingHQL(advertisorId, startDate, endDate, true);
            List<Object[]> partnerCount = requesTr.getCampaignRunCountUsingHQL(advertisorId, startDate, endDate, true);        
            if (partnerCount != null) {
                int count = 0;
                for (Object[] os : partnerCount) {
                    if (count < 5) {
                        String resName = resourceMap.get(os[1]);
                        if ((Long) os[0] > map.get(resName)) {
                            map.put(resName, (Long) os[0]);
                        }
                    } else {
                        break;
                    }
                }
            }
            if (map.isEmpty()) {
                int count = 0;
                for (int key : resourceMap.keySet()) {
                    if (count < 5) {
                        map.put(resourceMap.get(key), 0l);
                        count++;
                    } else {
                        break;
                    }
                }
            }
            Object[] a = map.entrySet().toArray();
            Arrays.sort(a, new Comparator() {
                public int compare(Object o1, Object o2) {
                    return ((Map.Entry<String, Long>) o2).getValue()
                            .compareTo(((Map.Entry<String, Long>) o1).getValue());
                }
            });
            for (int i = 0; i < a.length; i++) {
                String apiname = ((Map.Entry<String, Long>) a[i]).getKey();
                long apicount = ((Map.Entry<String, Long>) a[i]).getValue();
                sample.add(new ApiDashboard(apiname, apicount));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree(sample, new TypeToken<List<ApiDashboard>>() {
        }.getType());
        JsonArray jsonArray = element.getAsJsonArray();
        System.out.println("JSON Array 5:: " + jsonArray);
        out.print(jsonArray);
        out.flush();
        out.close();
        return;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
