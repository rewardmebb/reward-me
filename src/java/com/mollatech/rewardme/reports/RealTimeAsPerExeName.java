/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.reports;

import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.connector.management.AITrackingManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "RealTimeAsPerExeName", urlPatterns = {"/RealTimeAsPerExeName"})
public class RealTimeAsPerExeName extends HttpServlet {

    private static int counts = 0;

    private Date date = null;

    static final Logger logger = Logger.getLogger(RealTime.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(logger.getLevel() == null){
            logger.setLevel(Level.ALL);
        }
//        logger.info("Requested Servlet is realtime at " + new Date());
//        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();        
        RmBrandownerdetails ownerObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");  
        String exeName = (String) request.getSession().getAttribute("campExeNameDetail");
        logger.debug("Value of exeName  = " + exeName);                                
        Calendar endDate = Calendar.getInstance();
        Date eDate = endDate.getTime();
        String frequency = "1min";
        String f = frequency.replaceAll("min", "");
        int freqmin = Integer.parseInt(f);
        int frequensec = freqmin * 60;
        endDate.add(Calendar.SECOND, -frequensec);
        Date sDate = endDate.getTime();        
        AITrackingManagement requesTr = new AITrackingManagement();
        if (date == null) {
            if(ownerObj != null){
            List<Object[]> partnerCount = requesTr.getCountUsingHQLForRealTimeUsingEXEName(ownerObj.getOwnerId(), sDate, eDate,exeName, true);            
            if (partnerCount != null) {
                long count = 0;
                for (Object[] os : partnerCount) {
                    count += (Long) os[0];
                }
                counts = Integer.parseInt(String.valueOf(count));
            }else{
                counts = 0;
            }
            }else{
                counts = 0;
            }
            date = new Date();
        } else {
            long tinter = System.currentTimeMillis() - date.getTime();
            tinter = (tinter / 1000) % 60;
            if (tinter > 3) {
                if(ownerObj != null){
                    List<Object[]> partnerCount = requesTr.getCountUsingHQLForRealTime(ownerObj.getOwnerId(), sDate, eDate, true);
                    if (partnerCount != null) {
                        long count = 0;
                        for (Object[] os : partnerCount) {
                            count += (Long) os[0];
                        }
                        counts = Integer.parseInt(String.valueOf(count));
                    }else{
                        counts = 0;
                    }
                    date = new Date();
                }else{
                    counts = 0;
                }
            }
        }
        PrintWriter out = response.getWriter();
        try {
            json.put("_count", counts);
        } catch (Exception e) {
            logger.error("Exception at realtime ", e);
        }
        out.print(json);
        out.flush();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
