/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.billing;

import com.mollatech.rewardme.common.SendThreadEmailNotification;
import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.commons.UtilityFunctions;
import com.mollatech.rewardme.nucleus.db.RmApprovedpackagedetails;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.RmCreditinfo;
import com.mollatech.rewardme.nucleus.db.RmPaymentdetails;
import com.mollatech.rewardme.nucleus.db.RmSubscriptionpackagedetails;
import com.mollatech.rewardme.nucleus.db.connector.management.ApprovedPackageManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.CreditManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.PDFInvoiceManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.PaymentManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.SubscriptionManagement;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.stripe.model.Subscription;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "SubscribePackage", urlPatterns = {"/SubscribePackage"})
public class SubscribePackage extends HttpServlet {

    static final Logger logger = Logger.getLogger(SubscribePackage.class);

    public static final int PENDING = 2;
    public static final int SEND = 0;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {        
        if(logger.getLevel() == null){
            logger.setLevel(Level.ALL);
        }
        logger.info("Request servlet is #SubscribePackage at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();       
        String result = "success";
        String message = "Package subscriped successfully";
        
        String SessionId = (String) request.getSession().getAttribute("_brandownerSessionId");
        RmBrandownerdetails ownerObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");        
        String packageName = (String) request.getSession().getAttribute("_originalPackageName");
        String invoiceId = (String) request.getSession().getAttribute("_invoiceId");
        String paidAmount = (String) request.getSession().getAttribute("_grossAmount");                    
        logger.info("Brand owner " + ownerObj.getBrandName());
        logger.info("PackageName " + packageName);
        logger.info("InvoiceId " + invoiceId);
        logger.info("PaidAmount " + paidAmount);
        DecimalFormat df = new DecimalFormat("#0.00");
        int retValue = -1;
        Date d = new Date();        
        RmCreditinfo info = null;
        float pAmount = 0;        
        Double packageAmount = null;
        String billingPeriodMonthly = LoadSettings.g_sSettings.getProperty("billing.period.month");
        String billingPeriodQuaterly = LoadSettings.g_sSettings.getProperty("billing.period.quarterly");
        String billingPeriodHalfYearly = LoadSettings.g_sSettings.getProperty("billing.period.halfYearly");
        String billingPeriodYearly = LoadSettings.g_sSettings.getProperty("billing.period.yearly");       
        Subscription charge = (Subscription) request.getSession().getAttribute("chargeObj");
        if (charge != null) {
            try {
                if (paidAmount != null) {
                    pAmount = Float.parseFloat(paidAmount);
                }
                RmSubscriptionpackagedetails subscriptionObject = new RmSubscriptionpackagedetails();                               
                int multiple=1;
                // add the subscription package details of a advertiser
                RmApprovedpackagedetails packageObject = new ApprovedPackageManagement().getReqPackageByName(SessionId,packageName);               
                subscriptionObject.setCreationDate(new Date());                
                subscriptionObject.setOwnerId(ownerObj.getOwnerId());
                subscriptionObject.setPaymentMode(packageObject.getPaymentMode());
                subscriptionObject.setPlanAmount(packageObject.getPlanAmount());
                subscriptionObject.setStatus(GlobalStatus.PAID);
                subscriptionObject.setTax(packageObject.getTax());
                subscriptionObject.setMainCredits(packageObject.getMainCredits());                
                subscriptionObject.setPackageDescription(packageObject.getPackageDescription());
                subscriptionObject.setCreditDeductionConfiguration(packageObject.getCreditDeductionConfiguration());                
                subscriptionObject.setPackageDuration(packageObject.getPackageDuration());
                subscriptionObject.setRecurrenceBillingPlanId(packageObject.getRecurrenceBillingPlanId());
                subscriptionObject.setPackageName(packageObject.getPackageName());
                int days = 0; int billingDays = 0;
                if (packageObject.getPackageDuration().equalsIgnoreCase("daily")) {
                    days = 1;
                } else if (packageObject.getPackageDuration().equalsIgnoreCase("weekly")) {
                    days = 7;
                } else if (packageObject.getPackageDuration().equalsIgnoreCase("biMonthly")) {
                    days = 15;
                } else if (packageObject.getPackageDuration().equalsIgnoreCase("monthly")) {
                    if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                        billingDays = Integer.parseInt(billingPeriodMonthly);
                        //multiple=2;
                        multiple = Integer.parseInt(LoadSettings.g_sSettings.getProperty("first.time.discount.mothly"));
                    }
                    days = billingDays;
                } else if (packageObject.getPackageDuration().equalsIgnoreCase("quarterly")) {
                    if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                        billingDays = Integer.parseInt(billingPeriodQuaterly);
                    }
                    days = billingDays;
                } else if (packageObject.getPackageDuration().equalsIgnoreCase("halfYearly")) {
                    if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                        billingDays = Integer.parseInt(billingPeriodHalfYearly);
                    }
                    days = billingDays;
                } else if (packageObject.getPackageDuration().equalsIgnoreCase("yearly")) {
                    if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                        billingDays = Integer.parseInt(billingPeriodYearly);                        
                        multiple = Integer.parseInt(LoadSettings.g_sSettings.getProperty("first.time.discount.yearly"));
                    }
                    days = billingDays;
                }
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.DATE, days);
                subscriptionObject.setExpiryDate(calendar.getTime());
                retValue = new SubscriptionManagement().CreateSubscriptionDetails(SessionId, subscriptionObject);
                logger.info("Subscription Details created");
                // addtion of subscription package details end
                if (retValue > 0) {
                    CreditManagement crtMangt = new CreditManagement();
                    // assign a credit to Owner
                    info = crtMangt.getDetailsByOwnerId(subscriptionObject.getOwnerId());
                    if (info != null) {
                        info.setStatus(GlobalStatus.UPDATED);
                        info.setMainCredit(subscriptionObject.getMainCredits() + info.getMainCredit());
                        info.setCreditDeductionConfiguration(subscriptionObject.getCreditDeductionConfiguration());
                        info.setOwnerId(subscriptionObject.getOwnerId());
                        info.setExpectedCreditDeduction(info.getExpectedCreditDeduction()+subscriptionObject.getMainCredits());
                        crtMangt.updateDetails(info);
                    } else {
                        info = new RmCreditinfo();
                        info.setStatus(GlobalStatus.SUCCESS);                       
                        info.setMainCredit(subscriptionObject.getMainCredits());
                        info.setCreditDeductionConfiguration(subscriptionObject.getCreditDeductionConfiguration());
                        info.setOwnerId(subscriptionObject.getOwnerId());
                        info.setExpectedCreditDeduction(subscriptionObject.getMainCredits());
                        crtMangt.addDetails(info);
                    }
                    logger.info("Credit assign to brandowner.");
                    // credit assignment end                                        
                    packageAmount = Float.valueOf(packageObject.getPlanAmount()).doubleValue();                    
                    String tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.brandowner.payment");
                    String enquiryId = (String) LoadSettings.g_sSettings.getProperty("email.enquiry");
                    String supportId = (String) LoadSettings.g_sSettings.getProperty("email.question");
                    String ideaId = (String) LoadSettings.g_sSettings.getProperty("email.idea");
                    String[] enquiryEmailDetails = enquiryId.split(":");
                    String[] supportEmailDetails = supportId.split(":");
                    String[] ideaEmailDetails = ideaId.split(":");                                                            
                    String appurl = (request.getRequestURL().toString());
                    URL myAppUrl = new URL(appurl);
                    int port = myAppUrl.getPort();
                    if (myAppUrl.getProtocol().equals("https") && port == -1) {
                        port = 443;
                    } else if (myAppUrl.getProtocol().equals("http") && port == -1) {
                        port = 80;
                    }            
                    String path = request.getContextPath();
                    String dashboardURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path;                    
                    String greenbackGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/greenback.gif";
                    String spadeGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/spade.gif";
                    String addressbookGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/addressbook.gif";
                    String penpaperGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/penpaper.gif";
                    String lightbulbGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/lightbulb.gif";
                    logger.info("greenbackGIF : "+greenbackGIF);
                    String bucketName = packageName.toLowerCase();                
                    if (bucketName.contains("basic") && bucketName.contains("month")) {
                        packageName = "Basic";
                    } else if (bucketName.contains("basic") && bucketName.contains("year")) {
                        packageName = "Basic";
                    } else if (bucketName.contains("student") && bucketName.contains("month")) {
                        packageName = "Student";
                    } else if (bucketName.contains("student") && bucketName.contains("year")) {
                        packageName = "Student";
                    } else if (bucketName.contains("standard") && bucketName.contains("month")) {
                        packageName = "Standard";
                    } else if (bucketName.contains("standard") && bucketName.contains("year")) {
                        packageName = "Standard";
                    } else if (bucketName.contains("enterprise") && bucketName.contains("month")) {
                        packageName = "Enterprise";
                    } else if (bucketName.contains("enterprise") && bucketName.contains("year")) {
                        packageName = "Enterprise";
                    }
                    
                    if (tmessage != null) {
                        tmessage = tmessage.replaceAll("#partnerName#", ownerObj.getBrandName());                        
                        tmessage = tmessage.replaceAll("#datetime#", UtilityFunctions.getTMReqDate(d));
                        tmessage = tmessage.replaceAll("#package#", packageName);
                        tmessage = tmessage.replaceAll("#invoiceid#", invoiceId);
                        tmessage = tmessage.replaceAll("#paidAmount#", String.valueOf(df.format(pAmount)));
                        tmessage = tmessage.replaceAll("#paymentDate#", UtilityFunctions.getTMReqDate(new Date()));
                        tmessage = tmessage.replaceAll("#email#", ownerObj.getEmail());                        
                        tmessage = tmessage.replaceAll("#dashboardhref#", dashboardURL);
                        tmessage = tmessage.replaceAll("#enquiryId#", enquiryId);
                        tmessage = tmessage.replaceAll("#supportId#", supportId);
                        tmessage = tmessage.replaceAll("#ideaId#", ideaId);
                        tmessage = tmessage.replaceAll("#enquiryEmailLabel#", enquiryEmailDetails[1]);
                        tmessage = tmessage.replaceAll("#supportEmailLabel#", supportEmailDetails[1]);
                        tmessage = tmessage.replaceAll("#ideaEmailLabel#", ideaEmailDetails[1]);
                        tmessage = tmessage.replace("#greenbackGIF#", greenbackGIF);
                        tmessage = tmessage.replace("#spadeGIF#", spadeGIF);
                        tmessage = tmessage.replace("#addressbookGIF#", addressbookGIF);
                        tmessage = tmessage.replace("#penpaperGIF#", penpaperGIF);
                        tmessage = tmessage.replace("#lightbulbGIF#", lightbulbGIF);
                    }
                    String mimeType[] = {"application/octet-stream"};
                    String invoiceFilePath = new PDFInvoiceManagement().createInvoicePDF(packageAmount, ownerObj, invoiceId, subscriptionObject);
                    
                    String[] arrInvoicePath = {invoiceFilePath};
                    Path saveInvoicepath = Paths.get(invoiceFilePath, new String[0]);
                    byte[] invoiceByteArray = Files.readAllBytes(saveInvoicepath);
                    
                    // add payment details
                    RmPaymentdetails paymentObj = new RmPaymentdetails();                                        
                    packageAmount = Float.valueOf(packageObject.getPlanAmount()).doubleValue();
                    paymentObj.setInvoiceNo(invoiceId);
                    paymentObj.setPaidOn(new Date());
                    paymentObj.setPaidAmount(pAmount);
                    paymentObj.setSubscriptionId(subscriptionObject.getSubscriptionId());
                    paymentObj.setOwnerId(ownerObj.getOwnerId());
                    paymentObj.setInvoiceData(invoiceByteArray);
                    retValue = new PaymentManagement().createPaymentDetails(SessionId,paymentObj);                                        
                    logger.info("Payment details added");
                    SendThreadEmailNotification signupNotification = new SendThreadEmailNotification(tmessage,"Package Payment has been made successfully", SessionId, ownerObj.getEmail(), null, arrInvoicePath, invoiceFilePath);
                    Thread signupNotificationThread = new Thread(signupNotification);
                    signupNotificationThread.start();

                    json.put("result", result);
                    json.put("message", message);
                    request.getSession().setAttribute("chargeObj", null);
                } else {
                    result = "error";
                    message = "Package subscription failed.";
                    json.put("result", result);
                    logger.info("Response of #SubscribePackage from Parameter result is " + result);
                    json.put("message", message);
                    logger.info("Response of #SubscribePackage from Parameter message is " + message);
                }
                out.print(json);
                out.flush();
                out.close();
                return;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                logger.info("Response of #SubscribePackage from " + json.toString());
                logger.info("Response of #SubscribePackage from Servlet at " + new Date());
                out.print(json);
                out.flush();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
