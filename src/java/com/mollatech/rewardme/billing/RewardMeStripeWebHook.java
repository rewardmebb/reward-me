/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.billing;

import com.mollatech.rewardme.common.GenerateInvoiceId;
import com.mollatech.rewardme.common.SendThreadEmailNotification;
import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.commons.UtilityFunctions;
import com.mollatech.rewardme.nucleus.db.RmApprovedpackagedetails;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.RmCreditinfo;
import com.mollatech.rewardme.nucleus.db.RmOperators;
import com.mollatech.rewardme.nucleus.db.RmPaymentdetails;
import com.mollatech.rewardme.nucleus.db.RmSubscriptionpackagedetails;
import com.mollatech.rewardme.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.rewardme.nucleus.db.connector.management.ApprovedPackageManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.BrandOwnerManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.CreditManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.PDFInvoiceManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.PaymentManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.SessionManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.SubscriptionManagement;
import com.mollatech.service.nucleus.crypto.AES;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.stripe.Stripe;
import com.stripe.model.Event;
import com.stripe.model.EventData;
import com.stripe.model.Invoice;
import com.stripe.model.InvoiceLineItem;
import com.stripe.model.InvoiceLineItemCollection;
import com.stripe.model.Plan;
import com.stripe.model.StripeObject;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "RewardMeStripeWebHook", urlPatterns = {"/RewardMeStripeWebHook"})
public class RewardMeStripeWebHook extends HttpServlet {

    static final Logger logger = Logger.getLogger(RewardMeStripeWebHook.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(logger.getLevel() == null){
            logger.setLevel(Level.ALL);
        }
        AES aesObj = new AES();
        response.addHeader("Access-Control-Allow-Origin", "*");
        int retValue = -1;
        SessionManagement sManagement = new SessionManagement();        
        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);        
        String[] credentialInfo = rUtil.GetRemoteAccessCredentials();
        String SessionId = sManagement.OpenSession(credentialInfo[0], credentialInfo[1], request.getSession().getId());
        String stripeapikey = LoadSettings.g_sSettings.getProperty("stripe.apiKey");
        String billingPeriodMonthly = LoadSettings.g_sSettings.getProperty("billing.period.month");
        String billingPeriodQuaterly = LoadSettings.g_sSettings.getProperty("billing.period.quarterly");
        String billingPeriodHalfYearly = LoadSettings.g_sSettings.getProperty("billing.period.halfYearly");
        String billingPeriodYearly = LoadSettings.g_sSettings.getProperty("billing.period.yearly");
        stripeapikey = aesObj.PINDecrypt(stripeapikey, AES.getSignature());
        Stripe.apiKey = stripeapikey;
        RmCreditinfo info = null;
        logger.info("Web Hook StripeAPIKey " + stripeapikey);
        //System.out.println("Webhook called >>> " + stripeapikey);        
        DecimalFormat df = new DecimalFormat("#0.00");
        //Event event = null;
        //public Object handle(Request request, Response response) {
        // Retrieve the request's body and parse it as JSON
        try {
            String rawJson = IOUtils.toString(request.getInputStream());
            logger.info("rawJson " + rawJson);
            Event event = Event.GSON.fromJson(rawJson, Event.class);
            // Do something with event
            
            
            String enquiryId = (String) LoadSettings.g_sSettings.getProperty("email.enquiry");
            String supportId = (String) LoadSettings.g_sSettings.getProperty("email.question");
            String ideaId = (String) LoadSettings.g_sSettings.getProperty("email.idea");
            String[] enquiryEmailDetails = enquiryId.split(":");
            String[] supportEmailDetails = supportId.split(":");
            String[] ideaEmailDetails = ideaId.split(":");
            String appurl = (request.getRequestURL().toString());
            URL myAppUrl = new URL(appurl);
            int port = myAppUrl.getPort();
            if (myAppUrl.getProtocol().equals("https") && port == -1) {
                port = 443;
            } else if (myAppUrl.getProtocol().equals("http") && port == -1) {
                port = 80;
            }
            String path = request.getContextPath();
            
            String dashboardURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path;
            String greenbackGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/greenback.gif";
            String spadeGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/spade.gif";
            String addressbookGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/addressbook.gif";
            String penpaperGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/penpaper.gif";
            String lightbulbGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/lightbulb.gif";
            logger.info("greenbackGIF : " + greenbackGIF);
            if (event != null) {                                                
                String eventType = event.getType();
                logger.info("eventType " + eventType);
                if (eventType != null && eventType.equalsIgnoreCase("invoice.payment_succeeded")) {
                    EventData eventData = event.getData();
                    StripeObject stripeObj = eventData.getObject();
                    Invoice invoiceevent = Invoice.GSON.fromJson(stripeObj.toJson(), Invoice.class);
                    logger.info("invoiceevent " + invoiceevent.toString());
                    if (invoiceevent != null) {
                        String custId = invoiceevent.getCustomer();
                        logger.info("stripeCustId " + custId);
                        String subscriptionDetails = invoiceevent.getSubscription();
                        logger.info("stripeSubscriptionDetails " + subscriptionDetails);
                        Long grossTotalAmt = invoiceevent.getAmountDue();
                        Date currentDate = new Date();
                        RmSubscriptionpackagedetails subscriObject1 = null;
                        RmBrandownerdetails partnerObj = new BrandOwnerManagement().getOwnerDetailsByStripeCustomerId(custId);
                        if (partnerObj != null) {
                            subscriObject1 = new SubscriptionManagement().getSubscriptionbyOwnerId(partnerObj.getOwnerId());
                        }
                        if (subscriObject1 != null) {
                            Date expDate = subscriObject1.getExpiryDate();

                            if (currentDate.after(expDate)) {
                                InvoiceLineItemCollection invoiceLineItemColl = invoiceevent.getLines();
                                String tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.brandowner.payment");
                                List invoiceList = invoiceLineItemColl.getData();
                                InvoiceLineItem invoiceLineItemObj = (InvoiceLineItem) invoiceList.get(0);
                                Plan planObject = invoiceLineItemObj.getPlan();                                
                                String strPlanAmount = String.valueOf(grossTotalAmt);
                                String newAmount = strPlanAmount.substring(0, strPlanAmount.length() - 2);
                                String lastTwo = strPlanAmount.substring(Math.max(strPlanAmount.length() - 2, 0));
                                newAmount += "." + lastTwo;
                                logger.info("planId " + planObject.getId());
                                RmApprovedpackagedetails packageObject = new ApprovedPackageManagement().getBucketRequestsbyStripePlanId(planObject.getId());
                                if (packageObject != null) {
                                    RmSubscriptionpackagedetails subscriptionObject = new RmSubscriptionpackagedetails();                                    
                                    subscriptionObject.setCreationDate(new Date());                
                                    subscriptionObject.setOwnerId(partnerObj.getOwnerId());
                                    subscriptionObject.setPaymentMode(packageObject.getPaymentMode());
                                    subscriptionObject.setPlanAmount(packageObject.getPlanAmount());
                                    subscriptionObject.setStatus(GlobalStatus.PAID);
                                    subscriptionObject.setTax(packageObject.getTax());
                                    subscriptionObject.setMainCredits(packageObject.getMainCredits());                
                                    subscriptionObject.setPackageDescription(packageObject.getPackageDescription());
                                    subscriptionObject.setCreditDeductionConfiguration(packageObject.getCreditDeductionConfiguration());                
                                    subscriptionObject.setPackageDuration(packageObject.getPackageDuration());
                                    subscriptionObject.setRecurrenceBillingPlanId(packageObject.getRecurrenceBillingPlanId());
                                    subscriptionObject.setPackageName(packageObject.getPackageName());
                                    int days = 0; int billingDays = 0;
                                    if (packageObject.getPackageDuration().equalsIgnoreCase("daily")) {
                                        days = 1;
                                    } else if (packageObject.getPackageDuration().equalsIgnoreCase("weekly")) {
                                        days = 7;
                                    } else if (packageObject.getPackageDuration().equalsIgnoreCase("biMonthly")) {
                                        days = 15;
                                    } else if (packageObject.getPackageDuration().equalsIgnoreCase("monthly")) {
                                        if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                                            billingDays = Integer.parseInt(billingPeriodMonthly);                        
                                        }
                                        days = billingDays;
                                    } else if (packageObject.getPackageDuration().equalsIgnoreCase("quarterly")) {
                                        if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                                            billingDays = Integer.parseInt(billingPeriodQuaterly);
                                        }
                                        days = billingDays;
                                    } else if (packageObject.getPackageDuration().equalsIgnoreCase("halfYearly")) {
                                        if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                                            billingDays = Integer.parseInt(billingPeriodHalfYearly);
                                        }
                                        days = billingDays;
                                    } else if (packageObject.getPackageDuration().equalsIgnoreCase("yearly")) {
                                        if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                                            billingDays = Integer.parseInt(billingPeriodYearly);                                                
                                        }
                                        days = billingDays;
                                    }
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(new Date());
                                    calendar.add(Calendar.DATE, days);
                                    subscriptionObject.setExpiryDate(calendar.getTime());
                                    retValue = new SubscriptionManagement().CreateSubscriptionDetails(SessionId, subscriptionObject);
                                    // addtion of subscription package details end
                                    // assign a credit to developer
                                    info = new CreditManagement().getDetailsByOwnerId(subscriptionObject.getOwnerId());
                                    if (info != null) {
                                        info.setStatus(GlobalStatus.UPDATED);
                                        info.setMainCredit(subscriptionObject.getMainCredits() + info.getMainCredit());
                                        info.setCreditDeductionConfiguration(subscriptionObject.getCreditDeductionConfiguration());
                                        info.setOwnerId(subscriptionObject.getOwnerId());
                                        info.setExpectedCreditDeduction(info.getExpectedCreditDeduction()+subscriptionObject.getMainCredits());                        
                                        new CreditManagement().updateDetails(info);
                                    } else {
                                        info = new RmCreditinfo();
                                        info.setStatus(GlobalStatus.SUCCESS);                       
                                        info.setMainCredit(subscriptionObject.getMainCredits());
                                        info.setCreditDeductionConfiguration(subscriptionObject.getCreditDeductionConfiguration());
                                        info.setOwnerId(subscriptionObject.getOwnerId());
                                        info.setExpectedCreditDeduction(subscriptionObject.getMainCredits());
                                        new CreditManagement().addDetails(info);
                                    }
                                    // credit assignment end
                                    String invoiceid = GenerateInvoiceId.getDate() + partnerObj.getOwnerId()+ GenerateInvoiceId.getRandom();
                                    Double packageAmount = Float.valueOf(packageObject.getPlanAmount()).doubleValue();  
                                    
                                    String invoiceFilePath = new PDFInvoiceManagement().createInvoicePDF(packageAmount, partnerObj, invoiceid, subscriptionObject);                    
                                    String[] arrInvoicePath = {invoiceFilePath};
                                    Path saveInvoicepath = Paths.get(invoiceFilePath, new String[0]);
                                    byte[] invoiceByteArray = Files.readAllBytes(saveInvoicepath);
                                    // add payment details
                                    RmPaymentdetails paymentObj = new RmPaymentdetails();                                                                                                                                                                                 
                                    paymentObj.setInvoiceNo(invoiceid);
                                    paymentObj.setPaidOn(new Date());
                                    paymentObj.setPaidAmount(Float.parseFloat(newAmount));
                                    paymentObj.setSubscriptionId(subscriptionObject.getSubscriptionId());
                                    paymentObj.setOwnerId(partnerObj.getOwnerId());
                                    paymentObj.setInvoiceData(invoiceByteArray);                    
                                    retValue = new PaymentManagement().createPaymentDetails(SessionId, paymentObj);
                                    if (retValue > 0) {
                                        subscriptionObject.setStatus(GlobalStatus.PAID);
                                        new SubscriptionManagement().updateDetails(subscriptionObject);
                                    }                                                                                                                                                                                                                                                                                                                           
                                    String packageName = packageObject.getPackageName();
                                    String bucketName = packageName.toLowerCase();
                                    if (bucketName.contains("basic") && bucketName.contains("month")) {
                                        packageName = "Basic";
                                    } else if (bucketName.contains("basic") && bucketName.contains("year")) {
                                        packageName = "Basic";
                                    } else if (bucketName.contains("student") && bucketName.contains("month")) {
                                        packageName = "Student";
                                    } else if (bucketName.contains("student") && bucketName.contains("year")) {
                                        packageName = "Student";
                                    } else if (bucketName.contains("standard") && bucketName.contains("month")) {
                                        packageName = "Standard";
                                    } else if (bucketName.contains("standard") && bucketName.contains("year")) {
                                        packageName = "Standard";
                                    } else if (bucketName.contains("enterprise") && bucketName.contains("month")) {
                                        packageName = "Enterprise";
                                    } else if (bucketName.contains("enterprise") && bucketName.contains("year")) {
                                        packageName = "Enterprise";
                                    }
                                    if (tmessage != null) {
                                        tmessage = tmessage.replaceAll("#partnerName#", partnerObj.getBrandName());                        
                                        tmessage = tmessage.replaceAll("#datetime#", UtilityFunctions.getTMReqDate(new Date()));
                                        tmessage = tmessage.replaceAll("#package#", packageName);
                                        tmessage = tmessage.replaceAll("#invoiceid#", invoiceid);
                                        tmessage = tmessage.replaceAll("#paidAmount#", String.valueOf(df.format(newAmount)));
                                        tmessage = tmessage.replaceAll("#paymentDate#", UtilityFunctions.getTMReqDate(new Date()));
                                        tmessage = tmessage.replaceAll("#email#", partnerObj.getEmail());                        
                                        tmessage = tmessage.replaceAll("#dashboardhref#", dashboardURL);
                                        tmessage = tmessage.replaceAll("#enquiryId#", enquiryId);
                                        tmessage = tmessage.replaceAll("#supportId#", supportId);
                                        tmessage = tmessage.replaceAll("#ideaId#", ideaId);
                                        tmessage = tmessage.replaceAll("#enquiryEmailLabel#", enquiryEmailDetails[1]);
                                        tmessage = tmessage.replaceAll("#supportEmailLabel#", supportEmailDetails[1]);
                                        tmessage = tmessage.replaceAll("#ideaEmailLabel#", ideaEmailDetails[1]);
                                        tmessage = tmessage.replace("#greenbackGIF#", greenbackGIF);
                                        tmessage = tmessage.replace("#spadeGIF#", spadeGIF);
                                        tmessage = tmessage.replace("#addressbookGIF#", addressbookGIF);
                                        tmessage = tmessage.replace("#penpaperGIF#", penpaperGIF);
                                        tmessage = tmessage.replace("#lightbulbGIF#", lightbulbGIF);
                                    }                                                                                                            
                                    String mimeType[] = {"application/octet-stream"};

                                   // String[] arrInvoicePath = {invoiceFilePath};
                                    SendThreadEmailNotification signupNotification = new SendThreadEmailNotification(tmessage,"Package Payment has been made successfully", SessionId, partnerObj.getEmail(), null, arrInvoicePath, invoiceFilePath);
                                    Thread signupNotificationThread = new Thread(signupNotification);
                                    signupNotificationThread.start();
                                    File deleteFile = new File(invoiceFilePath);
                                    deleteFile.delete();
                                }
                                response.sendError(200);
                            }
                        }
                    }
                } else if (eventType != null && eventType.equalsIgnoreCase("invoice.payment_failed")) {
                    EventData eventData = event.getData();
                    StripeObject stripeObj = eventData.getObject();
                    Invoice invoiceevent = Invoice.GSON.fromJson(stripeObj.toJson(), Invoice.class);
                    logger.info("webhook invoiceevent " + invoiceevent.toString());
                    if (invoiceevent != null) {
                        String custId = invoiceevent.getCustomer();
                        Long grossTotalAmt = invoiceevent.getAmountDue();
                        logger.info("webhook stripeCustId " + custId);
                        logger.info("webhook grossTotalAmt " + grossTotalAmt);
                        RmBrandownerdetails partnerObj = new BrandOwnerManagement().getOwnerDetailsByStripeCustomerId(custId);

                        InvoiceLineItemCollection invoiceLineItemColl = invoiceevent.getLines();
                        String tmessage = (String) LoadSettings.g_templateSettings.getProperty("package.payment.failed");
                        List invoiceList = invoiceLineItemColl.getData();
                        InvoiceLineItem invoiceLineItemObj = (InvoiceLineItem) invoiceList.get(0);
                        Plan planObject = invoiceLineItemObj.getPlan();

                        String strPlanAmount = String.valueOf(grossTotalAmt);
                        String newAmount = strPlanAmount.substring(0, strPlanAmount.length() - 2);
                        String lastTwo = strPlanAmount.substring(Math.max(strPlanAmount.length() - 2, 0));
                        String packageName = "";
                        newAmount += "." + lastTwo;
                        logger.info("planId " + planObject.getId());
                        RmApprovedpackagedetails packageObject = new ApprovedPackageManagement().getBucketRequestsbyStripePlanId(planObject.getId());
                        if (packageObject != null) {
                            packageName = packageObject.getPackageName();
                        }
                        RmOperators[] operatorObj = new OperatorsManagement().getAllOperators();
                        String[] operatorEmail = null;
                        if (operatorObj != null) {
                            operatorEmail = new String[operatorObj.length];
                            for (int i = 0; i < operatorObj.length; i++) {
                                operatorEmail[i] = (String) operatorObj[i].getEmailid();
                            }
                        }                                               
                        String bucketName = packageName.toLowerCase();
                        if (bucketName.contains("basic") && bucketName.contains("month")) {
                            packageName = "Basic";
                        } else if (bucketName.contains("basic") && bucketName.contains("year")) {
                            packageName = "Basic";
                        } else if (bucketName.contains("student") && bucketName.contains("month")) {
                            packageName = "Student";
                        } else if (bucketName.contains("student") && bucketName.contains("year")) {
                            packageName = "Student";
                        } else if (bucketName.contains("standard") && bucketName.contains("month")) {
                            packageName = "Standard";
                        } else if (bucketName.contains("standard") && bucketName.contains("year")) {
                            packageName = "Standard";
                        } else if (bucketName.contains("enterprise") && bucketName.contains("month")) {
                            packageName = "Enterprise";
                        } else if (bucketName.contains("enterprise") && bucketName.contains("year")) {
                            packageName = "Enterprise";
                        }
                        if (tmessage != null) {
                            tmessage = tmessage.replaceAll("#partnerName#", partnerObj.getBrandName());
                            tmessage = tmessage.replaceAll("#package#", packageName);
                            tmessage = tmessage.replaceAll("#paidAmount#", df.format(Double.parseDouble(newAmount)));
                            tmessage = tmessage.replaceAll("#enquiryId#", enquiryId);
                            tmessage = tmessage.replaceAll("#supportId#", supportId);
                            tmessage = tmessage.replaceAll("#ideaId#", ideaId);
                            tmessage = tmessage.replaceAll("#enquiryEmailLabel#", enquiryEmailDetails[1]);
                            tmessage = tmessage.replaceAll("#supportEmailLabel#", supportEmailDetails[1]);
                            tmessage = tmessage.replaceAll("#ideaEmailLabel#", ideaEmailDetails[1]);
                            tmessage = tmessage.replace("#greenbackGIF#", greenbackGIF);
                            tmessage = tmessage.replace("#spadeGIF#", spadeGIF);
                            tmessage = tmessage.replace("#addressbookGIF#", addressbookGIF);
                            tmessage = tmessage.replace("#penpaperGIF#", penpaperGIF);
                            tmessage = tmessage.replace("#lightbulbGIF#", lightbulbGIF);
                        }                        
                        SendThreadEmailNotification signupNotification = new SendThreadEmailNotification(tmessage, "Package payment failed", SessionId, partnerObj.getEmail(), null, null, null);
                        Thread signupNotificationThread = new Thread(signupNotification);
                        signupNotificationThread.start();
                        response.sendError(200);
                    }
                }
                //response.sendError(200);
            } else {
                response.sendError(500);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
