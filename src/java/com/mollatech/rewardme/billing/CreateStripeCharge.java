/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.billing;

import com.mollatech.rewardme.nucleus.db.RmApprovedpackagedetails;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.connector.management.BrandOwnerManagement;
import com.mollatech.service.nucleus.crypto.AES;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.stripe.Stripe;
import com.stripe.model.Customer;
import com.stripe.model.Subscription;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "CreateStripeCharge", urlPatterns = {"/CreateStripeCharge"})
public class CreateStripeCharge extends HttpServlet {

    static final Logger logger = Logger.getLogger(CreateStripeCharge.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(logger.getLevel() == null){
            logger.setLevel(Level.ALL);
        }
        logger.info("Requested Servlet is StripeCreateCharge at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());                           
        JSONObject json = new JSONObject();
        RmBrandownerdetails details = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
        RmApprovedpackagedetails packageObject = (RmApprovedpackagedetails) request.getSession().getAttribute("_packageObject");
        try {            
            AES aesObj = new AES();
            String taxDetails = packageObject.getTax();            
            Float gstTax = Float.parseFloat("0");
            if(taxDetails != null && !taxDetails.isEmpty()){
                gstTax = Float.parseFloat(taxDetails);
            }
            String stripeapikey = LoadSettings.g_sSettings.getProperty("stripe.apiKey");
            logger.info("StripeAPIKey " + stripeapikey);            
            stripeapikey = aesObj.PINDecrypt(stripeapikey, AES.getSignature());
            Stripe.apiKey = stripeapikey;            
            Subscription subscription = null;                
            String stripeToken = request.getParameter("stripeToken");
            logger.info("stripeToken  = " + stripeToken);
            //check with stripe 
            String stripeCustId = details.getStripeCustomerId();
            if(stripeCustId != null){                    
                    // create subscription
                    logger.info("Brand owner subscribe for plan for recurrence billing "+details.getEmail());
                    Map<String, Object> item = new HashMap<String, Object>();
                    item.put("plan", packageObject.getRecurrenceBillingPlanId());

                    Map<String, Object> items = new HashMap<String, Object>();
                    items.put("0", item);

                    Map<String, Object> recurrencePlan = new HashMap<String, Object>();
                    recurrencePlan.put("customer", stripeCustId);
                    recurrencePlan.put("items", items);
                    recurrencePlan.put("tax_percent", gstTax);
                    subscription = Subscription.create(recurrencePlan);
                    String planSubscriptionStatus = subscription.getStatus();                    
                    RmBrandownerdetails partObj = new BrandOwnerManagement().getBrandOwnerDetails(details.getOwnerId());
                    partObj.setStripeCustomerId(stripeCustId);
                    partObj.setStripeSubscriptionId(subscription.getId());
                    new BrandOwnerManagement().updateDetails(partObj); 
                    request.getSession().setAttribute("_brandownerDetails", partObj);
                    logger.info("Brand owner successfully subscribe for plan "+packageObject.getRecurrenceBillingPlanId()+" with details "+planSubscriptionStatus);                                                
            }else{                
                //create customer with stripe
                logger.info("Creating stripe customer for "+details.getEmail());
                Map<String, Object> customerParams = new HashMap<String, Object>();
                customerParams.put("email", details.getEmail());
                customerParams.put("source", stripeToken);                                
                Customer customer = Customer.create(customerParams);
                String custId = customer.getId();
                logger.info("Stripe customer created  for "+details.getEmail() +" with custId "+custId);                                
                // subscribe customer to a plan for recureence
                logger.info("customer subscribe for plan for recurrence billing "+details.getEmail());
                Map<String, Object> item = new HashMap<String, Object>();
                item.put("plan", packageObject.getRecurrenceBillingPlanId());

                Map<String, Object> items = new HashMap<String, Object>();
                items.put("0", item);

                Map<String, Object> recurrencePlan = new HashMap<String, Object>();
                recurrencePlan.put("customer", custId);
                recurrencePlan.put("items", items);
                recurrencePlan.put("tax_percent", gstTax);
                subscription = Subscription.create(recurrencePlan);
                String planSubscriptionStatus = subscription.getStatus();                
                RmBrandownerdetails partObj = new BrandOwnerManagement().getBrandOwnerDetails(details.getOwnerId());
                partObj.setStripeCustomerId(custId);
                partObj.setStripeSubscriptionId(subscription.getId());
                new BrandOwnerManagement().updateDetails(partObj);
                request.getSession().setAttribute("_advertisorDetails", partObj);
                logger.info("Brand owner successfully subscribe for plan "+packageObject.getRecurrenceBillingPlanId()+" with details "+planSubscriptionStatus);                                                
            }
            request.getSession().setAttribute("chargeObj", subscription);                             
            response.sendRedirect("./paymentResponse.jsp");           
        }catch(Exception ex){
            ex.printStackTrace();
            json.put("_result", "error");
            json.put("_message", "Transaction failed");
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
