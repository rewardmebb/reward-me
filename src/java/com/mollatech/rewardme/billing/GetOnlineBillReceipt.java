/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.billing;

import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.RmPaymentdetails;
import com.mollatech.rewardme.nucleus.db.RmSubscriptionpackagedetails;
import com.mollatech.rewardme.nucleus.db.connector.management.PDFInvoiceManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.PaymentManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.SubscriptionManagement;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "GetOnlineBillReceipt", urlPatterns = {"/GetOnlineBillReceipt"})
public class GetOnlineBillReceipt extends HttpServlet {

    static final Logger logger = Logger.getLogger(GetOnlineBillReceipt.class);
    private static final int BUFSIZE = 4096;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Request servlet is #GetOnlineBillReceipt at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
                
        String SessionId = (String) request.getSession().getAttribute("_brandownerSessionId");
        RmBrandownerdetails partnerObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
        String invoiceId = request.getParameter("invoiceId");
        String paymentId = request.getParameter("paymentId");
        String subscriptionId = request.getParameter("subscriptionId");
        
        logger.info("invoiceId " + invoiceId);
        logger.info("paymentId " + paymentId);
        logger.info("subscriptionId " + subscriptionId);
        int ipayId = 0;
        int isubscribeId = 0;
        try {
            if (paymentId != null) {
                ipayId = Integer.parseInt(paymentId);
            }
            if (subscriptionId != null) {
                isubscribeId = Integer.parseInt(subscriptionId);
            }
            Double packageAmount = null;
            String invoiceFilePath = null;
            RmSubscriptionpackagedetails packageSubscribed = new SubscriptionManagement().getSubscriptionbySubscriptionId(isubscribeId);
            RmPaymentdetails receiptFile = new PaymentManagement().getPaymentDetailsbyBrandOwnerAndSubscriptionID(packageSubscribed.getSubscriptionId(),partnerObj.getOwnerId());
            String appurl = (request.getRequestURL().toString());
            URL myAppUrl = new URL(appurl);
            int port = myAppUrl.getPort();
            if (myAppUrl.getProtocol().equals("https") && port == -1) {
                port = 443;
            } else if (myAppUrl.getProtocol().equals("http") && port == -1) {
                port = 80;
            }                        
            String pdfSigImg = LoadSettings.pdfSavePath;
            if(receiptFile != null && receiptFile.getInvoiceData() != null){                
                invoiceFilePath = pdfSigImg +packageSubscribed.getPackageName() + invoiceId + ".pdf";
                File file = new File(invoiceFilePath);
                FileOutputStream fop = new FileOutputStream(file);
                fop.write(receiptFile.getInvoiceData());
                fop.flush();
                fop.close();
            }else{
            //RmPaymentdetails paymentObj = new PaymentManagement().getPaymentDetailsbyPaymentID(ipayId);                                  
            packageAmount = new Float(packageSubscribed.getPlanAmount()).doubleValue();
            invoiceFilePath = new PDFInvoiceManagement().createInvoicePDF(packageAmount, partnerObj, invoiceId, packageSubscribed);
                                                  
            Path saveInvoicepath = Paths.get(invoiceFilePath, new String[0]);
            byte[] invoiceByteArray = Files.readAllBytes(saveInvoicepath);    
            receiptFile = new RmPaymentdetails();
                receiptFile.setInvoiceData(invoiceByteArray);
                receiptFile.setOwnerId(partnerObj.getOwnerId());
                receiptFile.setSubscriptionId(packageSubscribed.getSubscriptionId());
                receiptFile.setPaidOn(new Date());
                new PaymentManagement().createPaymentDetails(SessionId,receiptFile);
            }    
            File file = new File(invoiceFilePath);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(invoiceFilePath);
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) file.length());
            String fileName = (new File(invoiceFilePath)).getName();
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(file));
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }
            in.close();
            outStream.close();
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
