/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.billing;

import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.connector.management.BrandOwnerManagement;
import com.mollatech.service.nucleus.crypto.AES;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.stripe.Stripe;
import com.stripe.model.Customer;
import com.stripe.model.Subscription;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "UnSubscribePackage", urlPatterns = {"/UnSubscribePackage"})
public class UnSubscribePackage extends HttpServlet {

    static final Logger logger = Logger.getLogger(UnSubscribePackage.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        logger.info("Requested Servlet is UnSubscribePackage at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();        
        String result = "success";
        String message = "Package have been subscriped successfully";                
        String subId = request.getParameter("packageId");
        logger.info("subscriptionId " + subId);        
        RmBrandownerdetails partnerObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
        try {
            AES aesObj = new AES();
            String stripeapikey = LoadSettings.g_sSettings.getProperty("stripe.apiKey");
            stripeapikey = aesObj.PINDecrypt(stripeapikey, AES.getSignature());
            Stripe.apiKey = stripeapikey;
            logger.info("StripeAPIKey " + stripeapikey);

            String subscriptionId = partnerObj.getStripeSubscriptionId();
            logger.info("stripescriptionId " + subscriptionId);
            String stripeCustId = partnerObj.getStripeCustomerId();
            logger.info("stripeCustId " + stripeCustId);
            try {
                Customer customer = Customer.retrieve(stripeCustId);
                if (customer == null) {
                    result = "error";
                    message = "Customer details failed to find";
                    json.put("_result", result);
                    logger.debug("Response of #UnSubscribePackage from Servlet's Parameter result is " + result);
                    json.put("_message", message);
                    logger.debug("Response of #UnSubscribePackage from Servlet's Parameter message is " + message);
                    out.print(json);
                    out.flush();
                    out.close();
                    return;
                }
                Subscription subscription = customer.getSubscriptions().retrieve(subscriptionId);
                if (subscription == null) {
                    result = "error";
                    message = "Subscription details failed to find";
                    json.put("_result", result);
                    logger.debug("Response of #UnSubscribePackage from Servlet's Parameter  result is " + result);
                    json.put("_message", message);
                    logger.debug("Response of #UnSubscribePackage from Servlet's Parameter  message is " + message);
                    out.print(json);
                    out.flush();
                    out.close();
                    return;
                }
                subscription.cancel(null);
            } catch (Exception ex) {
                ex.printStackTrace();
                result = "error";
                message = "failed to unsubscibe the package";
                json.put("_result", result);
                logger.debug("Response of #UnSubscribePackage from Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of #UnSubscribePackage from Servlet's Parameter  message is " + message);
                out.print(json);
                out.flush();
                out.close();
                return;
            }
            RmBrandownerdetails partObj = new BrandOwnerManagement().getBrandOwnerDetails(partnerObj.getOwnerId());
            partObj.setStripeSubscriptionId(null);
            int retValue = new BrandOwnerManagement().updateDetails(partObj);
            if (retValue == 0) {
                result = "success";
                message = "You successfully unsubscribe the package." ;
                json.put("_result", result);
                logger.debug("Response of #UnSubscribePackage from Servlet's Parameter result is " + result);
                json.put("_message", message);
                logger.debug("Response of #UnSubscribePackage from Servlet's Parameter message is " + message);
                request.getSession().setAttribute("_brandownerDetails", partObj);
            } else {
                result = "error";
                message = "failed to unsubscibe the package.";
                json.put("_result", result);
                logger.debug("Response of #UnSubscribePackage from #PPortal Servlet's Parameter result is " + result);
                json.put("_message", message);
                logger.debug("Response of #UnSubscribePackage from #PPortal Servlet's Parameter message is " + message);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
            result = "error";
            message = "failed to unsubscribe the package";
            json.put("_result", result);
            logger.debug("Response of #UnSubscribePackage from Servlet's Parameter result is " + result);
            json.put("_message", message);
            logger.debug("Response of #UnSubscribePackage from Servlet's Parameter message is " + message);
            out.print(json);
            out.flush();
            out.close();
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
