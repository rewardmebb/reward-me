/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.brandowner;

import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.rewardme.nucleus.db.connector.management.BrandOwnerManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.SessionManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "ForgetPassword", urlPatterns = {"/ForgetPassword"})
public class ForgetPassword extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json     = new JSONObject();
        String result       = "success";
        String message      = "Password updated successfully";
        PrintWriter out     = response.getWriter();
        String _channelName = "ServiceGuard";
        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
        String[] credentialInfo = rUtil.GetRemoteAccessCredentials();
        SessionManagement sManagement = new SessionManagement();
        String sessionId        = sManagement.OpenSession(credentialInfo[0], credentialInfo[1], request.getSession().getId());
        String userName         = request.getParameter("fpUsername");
        String emailID          = request.getParameter("fpEmailID");
        String new_password     = request.getParameter("newPassword");        
        String confirm_password = request.getParameter("confirmPassword");
        String userId           = request.getParameter("userId");
        int ownerid = Integer.parseInt(userId);
        int retValue = -1;
        if (new_password.equals(confirm_password)) {
            BrandOwnerManagement um  = new BrandOwnerManagement();
            RmBrandownerdetails userDetails = um.getBrandOwnerDetails(ownerid);
            if(userDetails != null){
                userDetails.setPassword(new_password);
                retValue = um.updateDetails(userDetails);
                if(retValue == 0){
                    result = "success";
                    message = "Password updated successfully";
                }
            }else{
                result = "error";
                message = "Password updation failed";
            }
        }else {
            result = "error";
            message = "Password Not Matched";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
