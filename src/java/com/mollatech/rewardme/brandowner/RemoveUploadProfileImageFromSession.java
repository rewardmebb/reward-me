/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.brandowner;

import static com.mollatech.rewardme.brandowner.UploadProfilePic.logger;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "RemoveUploadProfileImageFromSession", urlPatterns = {"/RemoveUploadProfileImageFromSession"})
public class RemoveUploadProfileImageFromSession extends HttpServlet {

    static final Logger logger = Logger.getLogger(RemoveUploadProfileImageFromSession.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("applicaion/json");
        if(logger.getLevel() == null){
            logger.setLevel(Level.ALL);
        }
        logger.info("Request servlet is #RemoveUploadProfileImageFromSession at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String fileName = request.getParameter("file");        
        String uploadedFileName = (String) request.getSession().getAttribute("_uploadedImageFileName");
        logger.info("uploadedFileName " + uploadedFileName);
        if(uploadedFileName != null && uploadedFileName.equalsIgnoreCase(fileName)){
            request.getSession().setAttribute("_profilePicUploaded", null);            
            json.put("_result", "success");
            json.put("_message", "Removed uploaded file from Session.");  
        }else{
            json.put("_result", "error");
            json.put("_message", "No file to remove from Session."); 
        }
        logger.info("Response of #RemoveUploadProfileImageFromSession  " + json.toString());
        logger.info("Response of #RemoveUploadProfileImageFromSession at " + new Date());
        out.print(json);
        out.close();
        return;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
