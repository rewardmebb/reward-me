/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.brandowner;

import com.mollatech.rewardme.common.SendThreadEmailNotification;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.rewardme.nucleus.db.connector.management.BrandOwnerManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.SessionManagement;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.bouncycastle.util.encoders.Base64;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "ResetPassword", urlPatterns = {"/ResetPassword"})
public class ResetPassword extends HttpServlet {

     String channelId = null;

    RmBrandownerdetails developer = null;

    String msg = "";
    
    String adImagePath = null;

    static final Logger logger = Logger.getLogger(ResetPassword.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Requested Servlet is ResetOperatorPassword at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
        String[] credentialInfo = rUtil.GetRemoteAccessCredentials();
        SessionManagement sManagement = new SessionManagement();
        String sessionId = sManagement.OpenSession(credentialInfo[0], credentialInfo[1], request.getSession().getId());
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        try {
//            String _oUsername = request.getParameter("_oUsername");
//            logger.debug("Value of _oUsername  = " + _oUsername);
            String _oEmailId = request.getParameter("_oEmailId");
            logger.debug("Value of _oEmailId  = " + _oEmailId);
//            String _oRandomString = request.getParameter("_oRandomString");
//            logger.debug("Value of _oRandomString  = " + _oRandomString);
            int hour = Integer.parseInt(LoadSettings.g_sSettings.getProperty("password.expiry.minutes"));
            Calendar c = Calendar.getInstance();
            c.add(Calendar.MINUTE, hour);
            Date expiry = c.getTime();
//            if (_oUsername.equals("")) {
//                json.put("result", "error");
//                json.put("message", "Please Enter Your Name");
//                return;
//            }
            if (_oEmailId.equals("")) {
                json.put("result", "error");
                json.put("message", "Please Enter Your Email");
                return;
            }

            RmBrandownerdetails operators = new BrandOwnerManagement().initializeResetPassword(sessionId, null, _oEmailId, expiry);
            if (operators == null) {
                json.put("result", "error");
                json.put("message", "No such Developer is present");
                return;
            }
            developer = operators;
            String appurl = (request.getRequestURL().toString());
                URL myAppUrl = new URL(appurl);
                String path = request.getContextPath();
                SimpleDateFormat formatter = new SimpleDateFormat("EEEE, MMM dd, yyyy HH:mm:ss a");
                String username = URLEncoder.encode(new String(Base64.encode(operators.getBrandName().getBytes())), "UTF-8");
                String email = URLEncoder.encode(operators.getEmail(), "UTF-8");
                String userId = URLEncoder.encode(new String(Base64.encode(String.valueOf(operators.getOwnerId()).getBytes())), "UTF-8");
//                //String randomString = URLEncoder.encode(new String(Base64.encode(operators.getRandomString().getBytes())), "UTF-8");
                String expiryTime = URLEncoder.encode(new String(Base64.encode(formatter.format(operators.getLinkExpiry()).getBytes())), "UTF-8");
//                String url = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/verifyDeveloperDetails.jsp?username=" + username + "&email=" + email + "&expiryTime=" + expiryTime + "&ref=" + userId;
//                //randomString = operators.getRandomString().split(",")[1];
//                //String message = LoadSettings.g_templateSettings.getProperty("mobile.operator.password.reset");
//                //message = message.replace("#rs#", randomString);
//                String greenbackGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/greenback.gif";
//                String spadeGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/spade.gif";
//                String addressbookGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/addressbook.gif";
//                String penpaperGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/penpaper.gif";
//                String lightbulbGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/lightbulb.gif";
                
            int port = myAppUrl.getPort();
            if (myAppUrl.getProtocol().equals("https") && port == -1) {
                port = 443;
            } else if (myAppUrl.getProtocol().equals("http") && port == -1) {
                port = 80;
            }              
            String greenbackGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/greenback.gif";
            String spadeGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/spade.gif";
            String addressbookGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/addressbook.gif";
            String penpaperGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/penpaper.gif";
            String lightbulbGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/lightbulb.gif";
            String webSiteURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path;
            
                
                
                
                logger.info("Reset Password lightbulbGIF >> " + lightbulbGIF);
                String enquiryId = (String) LoadSettings.g_sSettings.getProperty("email.enquiry");
                String supportId = (String) LoadSettings.g_sSettings.getProperty("email.question");
                String ideaId = (String) LoadSettings.g_sSettings.getProperty("email.idea");
                logger.info("Reset Password enquiryId >> " + enquiryId);
                String[] enquiryEmailDetails = enquiryId.split(":");
                String[] supportEmailDetails = supportId.split(":");
                String[] ideaEmailDetails = ideaId.split(":");
                if (myAppUrl.getProtocol().equals("https") && port == -1) {
                    port = 443;
                } else if (myAppUrl.getProtocol().equals("http") && port == -1) {
                    port = 80;
                }
                String contextPath = request.getContextPath();
                 String url = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/verifyDetails.jsp?username=" + username + "&email=" + email + "&expiryTime=" + expiryTime + "&ref=" + userId;
                String emailSaveURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + contextPath + "/images/email/";
                msg = LoadSettings.g_templateSettings.getProperty("email.operator.password.reset");
                msg = msg.replace("#href#", url);
                msg = msg.replace("#name#", operators.getBrandName());
                msg = msg.replace("#hour#", "" + hour);
                msg = msg.replace("#greenbackGIF#", greenbackGIF);
                msg = msg.replace("#spadeGIF#", spadeGIF);
                msg = msg.replace("#addressbookGIF#", addressbookGIF);
                msg = msg.replace("#penpaperGIF#", penpaperGIF);
                msg = msg.replace("#lightbulbGIF#", lightbulbGIF);
                msg = msg.replaceAll("#enquiryId#", enquiryId);
                msg = msg.replaceAll("#supportId#", supportId);
                msg = msg.replaceAll("#ideaId#", ideaId);
                msg = msg.replaceAll("#emailAd#", "");

                msg = msg.replaceAll("#enquiryEmailLabel#", enquiryEmailDetails[1]);
                msg = msg.replaceAll("#supportEmailLabel#", supportEmailDetails[1]);
                msg = msg.replaceAll("#ideaEmailLabel#", ideaEmailDetails[1]);
                
                logger.info("Reset Password Email \n " + msg);
                String emailAddSavePath = LoadSettings.emailAddSavePath;
                String emailAdLogoPath = emailAddSavePath+ "images/email";
                // New changes for advertisement
                if (operators.getEmail() != null) {
                    SendThreadEmailNotification signupNotification = new SendThreadEmailNotification(msg,"Verification link for reset password", sessionId, operators.getEmail(),null,null,null);
                    Thread signupNotificationThread = new Thread(signupNotification);
                    signupNotificationThread.start();
                    //new SendNotification().SendOnMobileByPreference(channelId, operators.getPhone(), message, 1, 1, hour);
                    json.put("result", "success");
                    json.put("message", "Password reset link send on your email address");
                    return;
                } else {
                    json.put("result", "error");
                    json.put("message", "Developer email details not found.");
                    return;
                }

        } catch (Exception ex) {
            try {
                json.put("result", "error");
                json.put("message", "Error in reseting password");
                logger.error("Exception at ResetDeveloperPassword ", ex);
                return;
            } catch (Exception e) {
                logger.error("Exception at ResetDeveloperPassword ", ex);
            }
        } finally {
            logger.info("Response of ResetDeveloperPassword " + json.toString());
            logger.info("Response of ResetDeveloperPassword Servlet at " + new Date());
           
            sRemoteAcess.close();
            suRemoteAcess.close();
            out.print(json);
            out.flush();
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
