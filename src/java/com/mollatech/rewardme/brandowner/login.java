/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.brandowner;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.RmSubscriptionpackagedetails;
import com.mollatech.rewardme.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.rewardme.nucleus.db.connector.management.BrandOwnerManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.SessionManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.SubscriptionManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "login", urlPatterns = {"/login"})
public class login extends HttpServlet {

    final String itemType = "LOGIN";

    int SUCCESS = 0;

    int FAILED = -1;

    final int ADMIN = 0;

    final int OPERATOR = 1;

    final int REPORTER = 2;

    final int ACTIVATED = 1;

    String BLOCKED = "BLOCKED_IP";
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = null;
        try {
            request.getSession().invalidate();
        } catch (Exception ex) {
        }
        response.setContentType("application/json");
        Object Obj = getServletContext().getAttribute("LoginDetails");
        Map<String, HttpSession> sessionMap = (Map) getServletContext().getAttribute("sessionDetails");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        try {
            String _userName = request.getParameter("_name");
            String _userpassword = request.getParameter("_passwd");
            String otp = request.getParameter("_otp");
            String result = "success";
            String message = "successful credential verification.";
            //SgAdvertiserDetails pdetails = null;
            //SgPartnerrequest partnerReqObj = null;
            SessionManagement sManagement = new SessionManagement();
           // String _channelName = "ServiceGuard";           
            SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
            Session sRemoteAcess = suRemoteAcess.openSession();
            RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
           // Channels channel = cUtil.getChannel(_channelName);
            String[] credentialInfo = rUtil.GetRemoteAccessCredentials();
            String SessionId = sManagement.OpenSession(credentialInfo[0], credentialInfo[1], request.getSession().getId());
            if (_userName == null || (_userpassword != null && _userpassword.equals("") && otp.equals(""))) {
                try {
                    request.getSession().invalidate();
                } catch (Exception ex) {
                }
                result = "error";
                message = "Enter Valid Username and Password";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }            
            BrandOwnerManagement um = new BrandOwnerManagement();
            RmBrandownerdetails user = null;
            RmSubscriptionpackagedetails subscriObject1 = null;
            if (_userpassword != null && !_userpassword.equals("")) {
                user = um.VerifyCredential(SessionId,_userName, _userpassword);
                //passwordFailed = true;
            }                                           
                if (user != null) {
                    subscriObject1 = new SubscriptionManagement().getSubscriptionbyOwnerId(user.getOwnerId());
                    if (user.getStatus() == GlobalStatus.SUSPEND) {
                        result = "error";
                        message = "Your account is Suspended, Please contact to Admin";
                        json.put("_result", result);
                        json.put("_message", message);
                        out.print(json);
                        out.flush();
                        return;
                    }
                    if (user.getStatus() == GlobalStatus.DELETED) {
                        result = "error";
                        message = "Your account is removed, Please contact to Admin";
                        json.put("_result", result);
                        json.put("_message", message);
                        out.print(json);
                        out.flush();
                        return;
                    }                    
                    request.getSession(true);
                    request.getSession().setAttribute("_brandownerSessionId", SessionId);
                    request.getSession().setAttribute("_brandownerDetails", user);                                                                                                                                    
                } else {
                    try {
                        request.getSession().invalidate();
                    } catch (Exception ex) {
                    }
                    result = "error";
                    message = "Not Valid User";
                    json.put("_result", result);
                    json.put("_message", message);
                    out.print(json);
                    out.flush();
                    return;
                }
            
            if (Obj == null) {
                Map map = new HashMap();
                sessionMap = new HashMap();
                map.put(user.getOwnerId(), request.getSession().getId() + ":Logged" + ":" + (new Date().getTime()));
                sessionMap.put(user.getOwnerId().toString(), request.getSession());
                getServletContext().setAttribute("LoginDetails", map);
                getServletContext().setAttribute("sessionDetails", sessionMap);
            } else {
                Map map = (Map) Obj;
                Object value = map.get(user.getOwnerId());
                if (value == null) {
                    map.put(user.getOwnerId(), request.getSession().getId() + ":Logged" + ":" + (new Date().getTime()));
                    getServletContext().setAttribute("LoginDetails", map);
                    sessionMap.put(user.getOwnerId().toString(), request.getSession());
                    getServletContext().setAttribute("sessionDetails", sessionMap);
                } else {
                    String val = value.toString();
                    String sessionIdstr = val.split(":")[0];
                    String flag = val.split(":")[1];
                    if (!request.getSession().getId().equals(sessionIdstr) && !flag.equalsIgnoreCase("Not Logged")) {
                        map.remove(user.getOwnerId());
                        HttpSession session = sessionMap.get(user.getOwnerId());
                        try {
                            session.invalidate();
                        } catch (Exception ex) {
                        }
                        sessionMap.remove(user.getOwnerId());
                        sessionMap.put(user.getOwnerId().toString(), request.getSession());
                        getServletContext().setAttribute("LoginDetails", map);
                        getServletContext().setAttribute("sessionDetails", sessionMap);
                        json.put("_result", "success");
                        json.put("_message", "Login Successful.");
                        // json.put("_url", "./homePage.jsp");
//                        json.put("_url", "./header.jsp");
                        // check for new resource added and assign auto APIToken
                        if (subscriObject1 != null) {
                            json.put("_url", "./header.jsp");
                        } else {
                            json.put("_url", "./packages.jsp");
                            json.put("_message", "Login Successful, Please subscribe a package first");
                        }
                        out.print(json);
                        out.flush();
                        return;
                    } else {
                        map.put(user.getOwnerId(), request.getSession().getId() + ":Logged" + ":" + (new Date().getTime()));
                        getServletContext().setAttribute("LoginDetails", map);
                        sessionMap.put(user.getOwnerId().toString(), request.getSession());
                        getServletContext().setAttribute("sessionDetails", sessionMap);
                    }
                }
            }
            json.put("_result", result);
            
            if (subscriObject1 != null) {
                json.put("_url", "./header.jsp");
            } else {
                json.put("_url", "./packages.jsp");
                json.put("_message", "Login Successful, Please subscribe a package first");
            }
            json.put("_message", message);
            out.print(json);
            out.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
            json.put("_url", url);
            json.put("_result", "error");
            json.put("_message", ex.getMessage());
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
