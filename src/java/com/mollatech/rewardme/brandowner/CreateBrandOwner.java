/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.brandowner;

import com.mollatech.rewardme.common.SendThreadEmailNotification;
import com.mollatech.rewardme.nucleus.commons.CommonUtility;
import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.rewardme.nucleus.db.connector.management.BrandOwnerManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.SessionManagement;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "CreateBrandOwner", urlPatterns = {"/CreateBrandOwner"})
public class CreateBrandOwner extends HttpServlet {

    static final Logger logger = Logger.getLogger(CreateBrandOwner.class);
    public static final int PENDING = 2;
    public static final int SEND = 0;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(logger.getLevel() == null){
            logger.setLevel(Level.ALL);
        }
        logger.info("Request servlet is #CreateBrandOwner called at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Your account created successfully, Please check your email for password.";
        PrintWriter out = response.getWriter();
        
        String ownerName = request.getParameter("partnerName");
        String mobileNo = request.getParameter("mobileNo");
        String emailId = request.getParameter("emailId");
        String comapnyName = request.getParameter("comapnyName");
        String regNo = request.getParameter("regNo");
        String landlineNo = request.getParameter("landlineNo");
        String companyAddressOne = request.getParameter("companyAddressOne");
        String companyAddressTwo = request.getParameter("companyAddressTwo");
        String companyAddressThree = request.getParameter("companyAddressThree");
        String cityName = request.getParameter("cityName");
        String state = request.getParameter("state");
        String country = request.getParameter("country");
        String pinCode = request.getParameter("pinCode");                
        String zippath = (String) request.getSession().getAttribute("_KYCUploaded");        

        logger.info("ownerName : " + ownerName);
        logger.info("mobileNo : " + mobileNo);
        logger.info("emailId : " + emailId);
        logger.info("comapnyName : " + comapnyName);
        logger.info("regNo : " + regNo);
        logger.info("landlineNo : " + landlineNo);
        logger.info("companyAddressOne : " + companyAddressOne);
        logger.info("companyAddressTwo : " + companyAddressTwo);
        logger.info("companyAddressThree : " + companyAddressThree);
        logger.info("cityName : " + cityName);
        logger.info("state : " + state);
        logger.info("country : " + country);
        logger.info("pinCode : " + pinCode);
        logger.info("kyc document path : " + zippath);
        
        if (zippath == null) {
            try {
                message = "Please attach document in zip file format";
                json.put("_result", "error");
                logger.info("Response of CreateBrandOwner Servlet's Parameter result is error");
                json.put("_message", message);
                logger.info("Response of CreateBrandOwner Servlet's Parameter message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        
        SessionManagement sManagement = new SessionManagement();
        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);       
        String[] credentialInfo = rUtil.GetRemoteAccessCredentials();
        String SessionId = sManagement.OpenSession(credentialInfo[0], credentialInfo[1], request.getSession().getId());
        
        byte[] compressed_document = null;
        String mimeType[] = {"application/octet-stream"};
        if (zippath != null) {
            Path path = Paths.get(zippath);
            compressed_document = Files.readAllBytes(path);
        }
        
        int retValue = -1;

        String tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.brandowner.signUp");
        retValue = new BrandOwnerManagement().checkIsUniqueInBrandOwner(SessionId, ownerName, emailId, mobileNo);

        if (retValue != 0) {
            result = "error";
            message = "Email id or Phone no is already in use, Please try with other";
            try {
                json.put("_result", result);
                json.put("_message", message);
                logger.info("Response of #CreateBrandOwner from Servlet's Parameter  result is " + result);                
                logger.info("Response of #CreateBrandOwner from Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        String token = ownerName + emailId + new Date() + mobileNo;
        byte[] SHA1hash = CommonUtility.SHA1(token);
        token = new String(Base64.encode(SHA1hash));
        String advertiserpassword = token.substring(0, 9);
        RmBrandownerdetails ownerDetails = new RmBrandownerdetails();
        ownerDetails.setBrandName(ownerName);        
        ownerDetails.setCity(country);
        ownerDetails.setComapanyAddrLine1(companyAddressOne);
        ownerDetails.setComapanyAddrLine2(companyAddressTwo);
        ownerDetails.setComapanyAddrLine3(companyAddressThree);
        ownerDetails.setCompanyName(comapnyName);
        ownerDetails.setCompanyState(state);
        ownerDetails.setCountry(country);
        ownerDetails.setCreationDate(new Date());
        ownerDetails.setEmail(emailId);
        ownerDetails.setKycDocument(compressed_document);
        ownerDetails.setPhone(mobileNo);
        ownerDetails.setPincode(pinCode);
        ownerDetails.setRegistrationNumber(regNo);
        ownerDetails.setLandlineNumber(landlineNo);
        ownerDetails.setPassword(advertiserpassword);
        ownerDetails.setStatus(GlobalStatus.ACTIVE);
        retValue = new BrandOwnerManagement().CreateBrandOwner(SessionId, ownerDetails);         
        if (retValue >= 0) {                        
            tmessage = tmessage.replaceAll("#password#", advertiserpassword);
            tmessage = tmessage.replaceAll("#email#", emailId);
            tmessage = tmessage.replaceAll("#datetime#", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
           
            String enquiryId = (String) LoadSettings.g_sSettings.getProperty("email.enquiry");
            String supportId = (String) LoadSettings.g_sSettings.getProperty("email.question");
            String ideaId = (String) LoadSettings.g_sSettings.getProperty("email.idea");
            String[] enquiryEmailDetails = enquiryId.split(":");
            String[] supportEmailDetails = supportId.split(":");
            String[] ideaEmailDetails = ideaId.split(":");
            
            String appurl = (request.getRequestURL().toString());
            URL myAppUrl = new URL(appurl);
            int port = myAppUrl.getPort();
            if (myAppUrl.getProtocol().equals("https") && port == -1) {
                port = 443;
            } else if (myAppUrl.getProtocol().equals("http") && port == -1) {
                port = 80;
            }            
            String path = request.getContextPath();    
            String greenbackGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/greenback.gif";
            String spadeGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/spade.gif";
            String addressbookGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/addressbook.gif";
            String penpaperGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/penpaper.gif";
            String lightbulbGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/lightbulb.gif";
            String webSiteURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path;
            logger.info("greenbackGIF : " + greenbackGIF);
            tmessage = tmessage.replace("#greenbackGIF#", greenbackGIF);
            tmessage = tmessage.replace("#spadeGIF#", spadeGIF);
            tmessage = tmessage.replace("#addressbookGIF#", addressbookGIF);
            tmessage = tmessage.replace("#penpaperGIF#", penpaperGIF);
            tmessage = tmessage.replace("#lightbulbGIF#", lightbulbGIF);

            tmessage = tmessage.replaceAll("#enquiryId#", enquiryId);
            tmessage = tmessage.replaceAll("#supportId#", supportId);
            tmessage = tmessage.replaceAll("#ideaId#", ideaId);

            tmessage = tmessage.replaceAll("#enquiryEmailLabel#", enquiryEmailDetails[1]);
            tmessage = tmessage.replaceAll("#supportEmailLabel#", supportEmailDetails[1]);
            tmessage = tmessage.replaceAll("#ideaEmailLabel#", ideaEmailDetails[1]);    
            
            tmessage = tmessage.replace("#webSiteURL#", webSiteURL);
            
            
            SendThreadEmailNotification signupNotification = new SendThreadEmailNotification(tmessage,"You successfully created an account with us", SessionId, emailId,null,null,null);
            Thread signupNotificationThread = new Thread(signupNotification);
            signupNotificationThread.start();

            request.getSession().setAttribute("_brandownerSessionId", SessionId);            
            request.getSession().setAttribute("_brandownerDetails", ownerDetails);
            request.getSession().setAttribute("showTourFirstSignUp", "yes");
            request.getSession().setAttribute("_KYCUploaded",null);
            File f = new File(zippath);
            f.delete();
        } else {
            result = "error";
            message = "Error while creating your account.";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            logger.info("Response of #CreateBrandOwner from Servlet's Parameter  result is " + result);                
            logger.info("Response of #CreateBrandOwner from Servlet's Parameter  message is " + message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
