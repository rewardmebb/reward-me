/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.brandowner;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.connector.management.BrandOwnerManagement;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.apache.log4j.Level;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "UploadBrandDetails", urlPatterns = {"/UploadBrandDetails"})
public class UploadBrandDetails extends HttpServlet {

    static final Logger logger = Logger.getLogger(UploadBrandDetails.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(logger.getLevel() == null){
            logger.setLevel(Level.ALL);
        }
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result;
        String message;
        PrintWriter out = response.getWriter();
        RmBrandownerdetails usrObj = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
        String SessionId = (String) request.getSession().getAttribute("_brandownerSessionId");       
        String cmpname = request.getParameter("cmpname");
        String cmpreg = request.getParameter("cmpreg");
        String addr1 = request.getParameter("addr1");
        String addr2 = request.getParameter("addr2");
        String addr3 = request.getParameter("addr3");
        String cmpstate = request.getParameter("cmpstate");
        String city = request.getParameter("city");
        String country = request.getParameter("country");
        String landline_number = request.getParameter("landline_number");
        String pinCode = request.getParameter("pinCode");
        String profilePic = (String) request.getSession().getAttribute("_profilePicUploaded");
        logger.info("cmpname " + cmpname);
        logger.info("cmpreg " + cmpreg);
        logger.info("addr1 " + addr1);
        logger.info("addr2 " + addr2);
        logger.info("addr3 " + addr3);
        logger.info("cmpstate " + cmpstate);
        logger.info("city " + city);
        logger.info("country " + country);
        logger.info("landline_number " + landline_number);
        logger.info("pinCode " + pinCode);
        logger.info("profilePic " + profilePic);
        try {            
            String imgBase64 = null;
            int res = -1;
            RmBrandownerdetails ownerDetails = new BrandOwnerManagement().getBrandOwnerDetails(usrObj.getOwnerId());                        
            if (profilePic != null) {
                File file = new File(profilePic);
                byte[] imgFileData = Files.readAllBytes(file.toPath());
                imgBase64 = com.sun.org.apache.xml.internal.security.utils.Base64.encode(imgFileData);
                ownerDetails.setProfilePic(imgBase64);
            } 
            
            ownerDetails.setBrandName(usrObj.getBrandName());
            ownerDetails.setCity(city);
            ownerDetails.setComapanyAddrLine1(addr1);
            ownerDetails.setComapanyAddrLine2(addr2);
            ownerDetails.setComapanyAddrLine3(addr3);
            ownerDetails.setCompanyName(cmpname);
            ownerDetails.setCompanyState(cmpstate);
            ownerDetails.setCountry(country);
            ownerDetails.setCreationDate(usrObj.getCreationDate());
            ownerDetails.setEmail(usrObj.getEmail());
            ownerDetails.setKycDocument(usrObj.getKycDocument());
            ownerDetails.setLandlineNumber(landline_number);
            ownerDetails.setLinkExpiry(usrObj.getLinkExpiry());
            ownerDetails.setPassword(usrObj.getPassword());
            ownerDetails.setPhone(usrObj.getPhone());
            ownerDetails.setPincode(pinCode);            
            ownerDetails.setRegistrationNumber(cmpreg);
            ownerDetails.setStatus(GlobalStatus.ACTIVE);
            ownerDetails.setStripeCustomerId(usrObj.getStripeCustomerId());
            ownerDetails.setStripeSubscriptionId(usrObj.getStripeSubscriptionId());            
            ownerDetails.setUpdationDate(new Date());
            res = new BrandOwnerManagement().updateDetails(ownerDetails);
            if (res == 0) {
                result = "success";
                message = "Your details saved successfully.";
                json.put("_result", result);
                json.put("_message", message);
                request.getSession().setAttribute("_brandownerDetails", ownerDetails);
                logger.info("Response of #UploadBrandDetails  " + json.toString());
                logger.info("Response of #UploadBrandDetails at " + new Date());
            } else {
                result = "error";
                message = "Failed to update detail";
                json.put("_result", result);
                json.put("_message", message);
                logger.info("Response of #UploadBrandDetails  " + json.toString());
                logger.info("Response of #UploadBrandDetails at " + new Date());
            }
        } catch (Exception e) {
            result = "Error";
            message = "Failed to update detail";
            json.put("_result", result);
            json.put("_message", message);
            logger.info("Response of #UploadBrandDetails  " + json.toString());
            logger.info("Response of #UploadBrandDetails at " + new Date());
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
            processRequest(request, response);
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
            processRequest(request, response);
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
