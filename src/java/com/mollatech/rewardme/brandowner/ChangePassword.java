package com.mollatech.rewardme.brandowner;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.connector.BrandOwnerUtils;
import com.mollatech.rewardme.nucleus.db.connector.management.BrandOwnerManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Bluebricks
 */
@WebServlet(name = "ChangePassword", urlPatterns = {"/ChangePassword"})
public class ChangePassword extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Password updated successfully";
        PrintWriter out = response.getWriter();

        String current_password = request.getParameter("current_password");
        String new_password = request.getParameter("new_password");
        String confirm_password = request.getParameter("confirm_password");
        String SessionId = (String) request.getSession().getAttribute("_brandownerSessionId");

        if (new_password.equals("") || confirm_password.equals("")) {
            result = "error";
            message = "Please Enter Password";
        } else if (new_password.equals(confirm_password)) {
            RmBrandownerdetails owner = (RmBrandownerdetails) request.getSession().getAttribute("_brandownerDetails");
            
            if (owner != null) {
                if (SessionId != null) {
                    BrandOwnerManagement um = new BrandOwnerManagement();
                    if (owner.getPassword() == current_password || owner.getPassword().equals(current_password)) {
                        owner.setPassword(new_password);
                        owner.setUpdationDate(new Date());
                       int res =  um.updateDetails(owner);
                        if (res == 0) {
                            request.getSession().setAttribute("_brandownerDetails",owner);
                            result = "success";
                            message = "Password updated successfully";
                        } else {
                            result = "error";
                            message = "Password updation failed";
                        }
                    } else {
                        result = "error";
                        message = "Current Password is not Valid";
                    }
                } else {
                    result = "error";
                    message = "Password updation failed";
                }
            } else {
                result = "error";
                message = "Password updation failed";
            }
        } else {
            result = "error";
            message = "Password Not Matched";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
