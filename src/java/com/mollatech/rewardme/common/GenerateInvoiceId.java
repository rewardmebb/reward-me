/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.common;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 *
 * @author abhishekingle
 */
public class GenerateInvoiceId {
    
    private final Random random = new Random();

    private final int range;

    private int previous;

    GenerateInvoiceId(int range) {
        this.range = range;
    }

    int nextRnd() {
        if (previous == 0) {
            return previous = random.nextInt(range) + 1;
        }
        final int rnd = random.nextInt(range - 1) + 1;
        return previous = (rnd < previous ? rnd : rnd + 1);
    }

    public static String getDate() {
        String pattern = "yyddMMHHmmss";
        SimpleDateFormat date = new SimpleDateFormat(pattern);
        return date.format(new Date());
    }

    public static String getRandom() {
        final GenerateInvoiceId t = new GenerateInvoiceId(4);
        String num = "";
        for (int i = 0; i < 4; i++) {
            num += t.nextRnd();
        }
        return num;
    }
    
}
