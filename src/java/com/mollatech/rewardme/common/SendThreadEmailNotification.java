/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.common;

import com.mollatech.rewardme.connector.communication.RMStatus;
import com.mollatech.rewardme.nucleus.settings.SendNotification;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author abhishekingle
 */
public class SendThreadEmailNotification implements Runnable{
    
    static final Logger logger = Logger.getLogger(SendThreadEmailNotification.class);
    String email;
    String tmessage;
    String subject;    
    String sessionId;
    String[] operatorEmail;
    String[] arrInvoicePath;
    String filePath;
    public static final int PENDING = 2;
    public static final int SEND = 0;
    String mimeType[] = {"application/octet-stream"};
    int productType = 3;
    
    public SendThreadEmailNotification(String message, String subject, String sessionId, String email, String[] operatorEmail, String[] arrInvoicePath, String filePath) {
        this.email = email;
        this.tmessage = message;
        this.subject = subject;
        this.sessionId = sessionId;
        this.operatorEmail = operatorEmail;
        this.arrInvoicePath = arrInvoicePath;
        this.filePath = filePath;
    }
    
    public void run() {
        RMStatus status = new SendNotification().SendEmail(email, subject, tmessage, operatorEmail, null, arrInvoicePath, mimeType, productType);
        if(logger.getLevel() == null){
            logger.setLevel(Level.ALL);
        }
        if (status.iStatus == PENDING || status.iStatus == SEND) {
            //System.out.println(subject + "with retCode " + status.iStatus + " to " + email + " at " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
            
            logger.info("Email Status "+subject + "with retCode " + status.iStatus + " to " + email + " at " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
            
        } else {
            //System.out.println("Email Status "+subject + "with retCode " + status.iStatus + " to " + email + " at " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
            logger.info("Email Status "+subject + "with retCode " + status.iStatus + " to " + email + " at " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
        }
        if(filePath != null){
            File deleteFile = new File(filePath);
            deleteFile.delete();
        }
    }
    
}
