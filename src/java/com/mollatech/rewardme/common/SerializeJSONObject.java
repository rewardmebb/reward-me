/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import org.json.JSONObject;
/**
 *
 * @author abhishekingle
 */
public class SerializeJSONObject {
    
    public byte[] SerializeObject(JSONObject json){
        //JSONObject json = new JSONObject();
        json.put("logo1","image");
        json.put("logo2","image");
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        OutputStreamWriter writer = new OutputStreamWriter(out);
        try {
            writer.write(json.toString());
//            writer.close();
//            writer.flush();
            return out.toByteArray();
        } catch (IOException ex) {
            Logger.getLogger(SerializeJSONObject.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public String DeSerializeObject(byte[] json){
        
        ByteArrayInputStream bais = new ByteArrayInputStream(json);
        //String reconstitutedJSONString = new String(bais);
        JsonReader reader = Json.createReader(bais);
        JsonObject json1 = reader.readObject();
        String strjson = json1.toString();
        System.out.println("strjson");
        return null;
    }

    public static void main(String[] args) throws IOException {
        JSONObject json = new JSONObject();
        byte[] jsonArr = new SerializeJSONObject().SerializeObject(json);
        String str = new SerializeJSONObject().DeSerializeObject(jsonArr);
    }
    
    
}
